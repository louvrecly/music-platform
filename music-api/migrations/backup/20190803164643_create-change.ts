import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable('ads_response');
    if(hasTable){
        return knex.schema.alterTable('ads_response',(table)=>{
            table.dropColumn('band_id');
        });  
    }else{
        return Promise.resolve();
    }

}

export async function down(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("ads_response");
    if(hasTable){
        return knex.schema.alterTable('ads_response',(table)=>{
            table.integer("band_id").unsigned();
            table.foreign('band_id').references('bands.id');
        });
    }else{
        return Promise.resolve();
    }
}


