import * as Knex from "knex";


// Data Definition (DD) - Create Tables
export async function up(knex: Knex): Promise<any> {

    return await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // users
        hasTable = await trx.schema.hasTable("users");
        if (!hasTable) {
            await trx.schema.createTable("users", (table) => {
                table.increments();
                table.string('username');
                table.string('password');
                table.timestamps(false, true);
            });
            console.log(`Table "users" has been created!`);
        } else {
            console.log(`Table "users" already exists!`);
        }

        // players
        hasTable = await trx.schema.hasTable("players");
        if (!hasTable) {
            await trx.schema.createTable("players", (table) => {
                table.increments();
                table.string('name');
                table.dateTime('date_of_birth');
                table.string('gender');
                table.string('profile_img');
                table.integer('user_id').unsigned();
                table.foreign('user_id').references('users.id');
                table.timestamps(false, true);
            });
            console.log(`Table "players" has been created!`);
        } else {
            console.log(`Table "players" already exists!`);
        }

        // bands
        hasTable = await trx.schema.hasTable("bands");
        if (!hasTable) {
            await trx.schema.createTable("bands", (table) => {
                table.increments();
                table.string('name');
                table.string('band_img');
                table.integer('founder_id').unsigned();
                table.foreign('founder_id').references('players.id');
                table.timestamps(false, true);
            });
            console.log(`Table "bands" has been created!`);
        } else {
            console.log(`Table "bands" already exists!`);
        }

        // band_members
        hasTable = await trx.schema.hasTable("band_members");
        if (!hasTable) {
            await trx.schema.createTable("band_members", (table) => {
                table.increments();
                table.integer('band_id').unsigned();
                table.foreign('band_id').references('bands.id');
                table.integer('member_id').unsigned();
                table.foreign('member_id').references('players.id');
                table.timestamps(false, true);
            });
            console.log(`Table "band_members" has been created!`);
        } else {
            console.log(`Table "band_members" already exists!`);
        }

        // midis
        hasTable = await trx.schema.hasTable("midis");
        if (!hasTable) {
            await trx.schema.createTable("midis", (table) => {
                table.increments();
                table.string('name');
                table.string('type');
                table.timestamps(false, true);
            });
            console.log(`Table "midis" has been created!`);
        } else {
            console.log(`Table "midis" already exists!`);
        }

        // band_ads
        hasTable = await trx.schema.hasTable("band_ads");
        if (!hasTable) {
            await trx.schema.createTable("band_ads", (table) => {
                table.increments();
                table.string('name');
                table.string('description');
                table.string('instruments');
                table.string('message');
                table.boolean('valid');
                table.integer('band_id').unsigned();
                table.foreign('band_id').references('bands.id');
                table.timestamps(false, true);
            });
            console.log(`Table "band_ads" has been created!`);
        } else {
            console.log(`Table "band_ads" already exists!`);
        }

        // ads_response
        hasTable = await trx.schema.hasTable("ads_response");
        if (!hasTable) {
            await trx.schema.createTable("ads_response", (table) => {
                table.increments();
                table.string('message');
                table.string('status');
                table.integer('band_ads_id').unsigned();
                table.foreign('band_ads_id').references('band_ads.id');
                table.integer('player_id').unsigned();
                table.foreign('player_id').references('players.id');
                table.timestamps(false, true);
            });
            console.log(`Table "ads_response" has been created!`);
        } else {
            console.log(`Table "ads_response" already exists!`);
        }

        // instruments
        hasTable = await trx.schema.hasTable("instruments");
        if (!hasTable) {
            await trx.schema.createTable("instruments", (table) => {
                table.increments();
                table.string('name');
                table.timestamps(false, true);
            });
            console.log(`Table "instruments" has been created!`);
        } else {
            console.log(`Table "instruments" already exists!`);
        }

        // midi_instruments
        hasTable = await trx.schema.hasTable("midi_instruments");
        if (!hasTable) {
            await trx.schema.createTable("midi_instruments", (table) => {
                table.increments();
                table.integer('number').unsigned();
                table.string('class');
                table.string('name');
                table.boolean('is_percussion');
                table.timestamps(false, true);
            });
            console.log(`Table "midi_instruments" has been created!`);
        } else {
            console.log(`Table "midi_instruments" already exists!`);
        }

    });

}


// Data Definition (DD) - Drop Tables
export async function down(knex: Knex): Promise<any> {

    return await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // midi_instruments
        hasTable = await trx.schema.hasTable("midi_instruments");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("midi_instruments");
            console.log(`Table "midi_instruments" has been dropped!`);
        } else {
            console.log(`Table "midi_instruments" does not exist!`);
        }

        // instruments
        hasTable = await trx.schema.hasTable("instruments");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("instruments");
            console.log(`Table "instruments" has been dropped!`);
        } else {
            console.log(`Table "instruments" does not exist!`);
        }

        // ads_response
        hasTable = await trx.schema.hasTable("ads_response");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("ads_response");
            console.log(`Table "ads_response" has been dropped!`);
        } else {
            console.log(`Table "ads_response" does not exist!`);
        }

        // band_ads
        hasTable = await trx.schema.hasTable("band_ads");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("band_ads");
            console.log(`Table "band_ads" has been dropped!`);
        } else {
            console.log(`Table "band_ads" does not exist!`);
        }
        
        // midis
        hasTable = await trx.schema.hasTable("midis");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("midis");
            console.log(`Table "midis" has been dropped!`);
        } else {
            console.log(`Table "midis" does not exist!`);
        }
        
        // band_members
        hasTable = await trx.schema.hasTable("band_members");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("band_members");
            console.log(`Table "band_members" has been dropped!`);
        } else {
            console.log(`Table "band_members" does not exist!`);
        }
        
        // bands
        hasTable = await trx.schema.hasTable("bands");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("bands");
            console.log(`Table "bands" has been dropped!`);
        } else {
            console.log(`Table "bands" does not exist!`);
        }

        // players
        hasTable = await trx.schema.hasTable("players");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("players");
            console.log(`Table "players" has been dropped!`);
        } else {
            console.log(`Table "players" does not exist!`);
        }

        // users
        hasTable = await trx.schema.hasTable("users");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await trx.schema.dropTable("users");
            console.log(`Table "users" has been dropped!`);
        } else {
            console.log(`Table "users" does not exist!`);
        }



    });
}