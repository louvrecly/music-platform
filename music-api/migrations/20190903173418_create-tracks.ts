import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("tracks");
    if (!hasTable) {
        return knex.schema.createTable("tracks", (table) => {
            table.increments();
            table.string('name');
            table.boolean('is_mono');
            table.jsonb('midi_array');
            table.integer('band_id').unsigned();
            table.foreign('band_id').references('bands.id');
            table.timestamps(false, true);
        });
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("tracks");
    // console.log("hasTable = ", hasTable);
    if (hasTable) {
        await knex.schema.dropTable("tracks");
        console.log(`Table "tracks" has been dropped!`);
    } else {
        console.log(`Table "tracks" does not exist!`);
    }
}

