import * as Knex from "knex";
import { Instrument } from "./models";


export class InstrumentService {

    constructor(private knex: Knex) { }

    public async getInstruments() {
        return await this.knex.select('*').from('instruments').orderBy('id');
    }

    public async addInstrument(newInstrument: Instrument) {
        const [instrument] = await this.knex.select('*').from('instrument').where('name', newInstrument.name).limit(1);
        if (instrument) {
            console.log(`Instrument ${newInstrument.name} already exists!`);
            throw new Error(`Instrument ${newInstrument.name} already exists!`);
        } else {
            return await this.knex.insert(newInstrument).into('instruments').returning('id');
        }
    }

    public async editInstrument(editedInstrument: Instrument) {
        const [instrument] = await this.knex.select('*').from('instruments').where('name', editedInstrument.name);
        console.log({ instrument, editedInstrument });
        if (instrument) {
            return await this.knex('instruments').update(editedInstrument).where('name', editedInstrument.name).returning('id');
        } else {
            console.log(`Invalid instrument: ${editedInstrument.name}`);
            throw new Error(`Invalid instrument: ${editedInstrument.name}`);
        }
    }

}