// import * as jsonfile from "jsonfile";
import * as Knex from "knex";
import { Player } from "./models";


export class PlayerService {

    constructor(private knex: Knex) { }

    public async loadPlayerProfiles() {
        return await this.knex.select('id', 'name', 'date_of_birth', 'gender', 'profile_img').from('players').orderBy('id', 'asc');
    }

    public async getPlayer(id: number) {
        return await this.knex.select('*').from('players').where('id', id).limit(1);
    }

    public async getPublicProfile(id: number) {
        const [publicProfile] = await this.knex.select('id', 'name', 'date_of_birth', 'gender', 'profile_img').from('players').where('id', id).limit(1);
        if (publicProfile) {
            return publicProfile;
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async addPlayer(newPlayer: Player, instrumentIds: number[]) {
        const [player] = await this.knex.select('*').from('players').where('id', newPlayer.id).limit(1);
        if (player) {
            console.log("player.id = ", player.id);
            throw new Error("id already exists");
        } else {
            console.log({ newPlayer });
            const newPlayerId = await this.knex.transaction(async (trx) => {
                const newPlayerId = await trx.insert([newPlayer]).into('players').returning('id');
                console.log({ newPlayerId });
                let playerInstruments = [];
                // console.log({instrumentIds});
                for (let instrumentId of instrumentIds) {
                    const playerInstrument = {
                        player: newPlayerId[0],
                        instrument: instrumentId
                    };
                    playerInstruments.push(playerInstrument);
                }
                console.log({ playerInstruments });
                await trx.insert(playerInstruments).into('player_instruments').returning('id');
                return newPlayerId;
            });
            return newPlayerId;
        }
    }

    public async editPlayer(editedPlayer: Player) {
        const [player] = await this.knex.select('*').from('players').where('id', editedPlayer.id).limit(1);
        // console.log({ player, editedPlayer });
        if (player) {
            return await this.knex('players').update(editedPlayer).where('id', editedPlayer.id).returning('id');
        } else {
            // console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async updateProfileImg(id: number, profile_img: string) {
        const [player] = await this.knex.select('*').from('players').where('id', id).limit(1);
        // console.log({ id, player, profile_img });
        if (player) {
            player.profile_img = profile_img;
            const editedPlayerId = await this.knex('players').update(player).where('id', id).returning('id');
            return editedPlayerId;
        } else {
            // console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async updatePlayerInfo(id: number, editedPlayerName: string) {
        const [player] = await this.knex.select('*').from('players').where('id', id).limit(1);
        // console.log({ player, editedPlayerName });
        if (player) {
            player.name = editedPlayerName;
            const editedPlayerId = await this.knex('players').update(player).where('id', id).returning('id');
            return editedPlayerId;
        } else {
            // console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

}




// // // Test Codes
// const playerService1 = new PlayerService();
// // // const player = playerService.getPlayer("u00001");
// // // console.log("player = ", player);
// // // // playerService.addPlayer("u00003", "player3", 15, "M", 320, {match: 35, win: 10, lose: 25}, [22.3204, 114.1698], {}, {phone: 45678901, email: "user4@tecky.io"});
// // // playerService.addPlayer("u00004", "player4", 15, "F", 320, {match: 35, win: 10, lose: 25}, [22.3204, 114.1698], {}, {phone: 45678901, email: "user4@tecky.io"});
// // // const players = playerService.getPlayers();
// // // console.log("players = ", players);
// playerService1.updatePlayerProficiencyAfterMatch("u00001", "u00002", "tennis", 1, 2, "Third");