// import * as Knex from "knex";
// import * as aws from "aws-sdk";
// import { NoteEvent, MidiArray } from "./models";
// let MidiWriter = require("midi-writer-js");


// export class MusicService {

//     constructor(private knex: Knex) {
//         console.log(this.knex);
//     }

//     // public async getInstruments() {
//     //     return await this.knex.select('*').from('instruments').orderBy('id');
//     //     // const instruments = await this.knex.transaction(async (trx) => {
//     //     //     return await trx.select('*').from('instruments');
//     //     // });
//     //     // return instruments;
//     // }

//     // public async addInstrument(newInstrument: Instrument) {
//     //     const [instrument] = await this.knex.select('*').from('instrument').where('name', newInstrument.name).limit(1);
//     //     // let instrument: Instrument = await this.knex.transaction(async (trx) => {
//     //     //     return (await trx.select('*').from('instrument').where('name', newInstrument.name))[0];
//     //     // });
//     //     if (instrument) {
//     //         console.log(`Instrument ${newInstrument.name} already exists!`);
//     //         throw new Error(`Instrument ${newInstrument.name} already exists!`);
//     //     } else {
//     //         return await this.knex.insert(newInstrument).into('instruments').returning('id');
//     //         // const instrumentId = await this.knex.transaction(async (trx) => {
//     //         //     await trx.insert(newInstrument).into('instruments').returning('id');
//     //         // });
//     //         // console.log("instrumentId = ", instrumentId);
//     //         // return instrumentId;
//     //     }
//     // }

//     // public async editInstrument(editedInstrument: Instrument) {
//     //     const [instrument] = await this.knex.select('*').from('instruments').where('name', editedInstrument.name);
//     //     // let instrument: Instrument = await this.knex.transaction(async (trx) => {
//     //     //     return (await trx.select('*').from('instruments').where('name', editedInstrument.name))[0];
//     //     // });
//     //     console.log({ instrument, editedInstrument });
//     //     if (instrument) {
//     //         return await this.knex('instruments').update(editedInstrument).where('name', editedInstrument.name).returning('id');
//     //         // const instrumentId = await this.knex.transaction(async (trx) => {
//     //         //     await trx('instruments').update(editedInstrument).where('name', editedInstrument.name).returning('id');
//     //         // });
//     //         // return instrumentId;
//     //     } else {
//     //         console.log(`Invalid instrument: ${editedInstrument.name}`);
//     //         throw new Error(`Invalid instrument: ${editedInstrument.name}`);
//     //     }
//     // }

//     public upload(data: Buffer, trackName: string) {
//         // asw.config.update({
//         //     accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//         //     secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
//         //     region: 'ap-southeast-1'
//         // });
//         var s3 = new aws.S3({
//             accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//             secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
//             region: 'ap-southeast-1'
//             // params: {
//             //     Bucket: 'cdn.letsjam.ga',
//             //     ACL: 'public-read' //檔案權限
//             // }
//         });
//         s3.upload({
//             Body: data,
//             Bucket: 'cdn.letsjam.ga',
//             Key: `${trackName}-${Date.now()}.mid`
//         }).on('httpUploadProgress', function (evt: any) {
//             //上傳進度
//             console.log(evt);
//         }).send(function (err: Error, res: any) {
//             if (err) {
//                 console.log(err)
//             }
//             console.log(res.Location.split('s://s3.ap-southeast-1.amazonaws.com').join(':/'))
//             //上傳完畢或是碰到錯誤
//         });
//     }

//     public writeMidiFile(trackName: string, format: number, midiArray: MidiArray) {
//         let track = new MidiWriter.Track();
//         const noteEvents: NoteEvent[] = midiArray.map(midiItem => {
//             const [pitch, duration] = midiItem;
//             const noteEvent: NoteEvent = { pitch, duration };
//             console.log({ noteEvent });
//             return new MidiWriter.NoteEvent(noteEvent);
//         });
//         console.log({ noteEvents });
//         let sequential: boolean;
//         switch (format) {
//             case 0:
//                 sequential = true;
//                 break;
//             case 1:
//                 sequential = false;
//                 break;
//             default:
//                 console.log(`Invalid format code: ${format}\nformat should only be either 0 or 1!`);
//                 throw new Error(`Invalid format code: ${format}\nformat should only be either 0 or 1!`);
//         }
//         track.addEvent(noteEvents, () => {
//             return { sequential: sequential };
//         });
//         console.log({ track });
//         let writer = new MidiWriter.Writer([track]);
//         console.log("writer.dataUri() = ", writer.dataUri());
//         // writer.saveMIDI(path.join(__dirname, trackName));
//         const buffer = new (Buffer as any).from(writer.buildFile());
//         this.upload(buffer, trackName)
//         return writer.dataUri();
//     }
// }




// // // // Test Codes
// // const playerService1 = new PlayerService();
// // // // const player = playerService.getPlayer("u00001");
// // // // console.log("player = ", player);
// // // // // playerService.addPlayer("u00003", "player3", 15, "M", 320, {match: 35, win: 10, lose: 25}, [22.3204, 114.1698], {}, {phone: 45678901, email: "user4@tecky.io"});
// // // // playerService.addPlayer("u00004", "player4", 15, "F", 320, {match: 35, win: 10, lose: 25}, [22.3204, 114.1698], {}, {phone: 45678901, email: "user4@tecky.io"});
// // // // const players = playerService.getPlayers();
// // // // console.log("players = ", players);
// // playerService1.updatePlayerProficiencyAfterMatch("u00001", "u00002", "tennis", 1, 2, "Third");