'use strict';
var myId;
var joinedClients = 0;
var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var localStream;
var remoteStream;
var pc = [];
var turnReady;
var hasConnection = [];
var connection = 0;
var newId;
var connectId;
// var position;

var pcConfig = {
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  }]
};

// Set up audio and video regardless of what devices are present.
var sdpConstraints = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

/////////////////////////////////////////////

var room = 1;
// Could prompt for room name:
// room = prompt('Enter room name:');

var socket = io.connect();

if (room !== '') {
  socket.emit('create or join', room);
  console.log('Attempted to create or  join room', room);
}

socket.on('created', function(room,Id) {
  myId = Id;
  console.log('Created room ' + room + ' ' + myId);
  console.log('This peer is the initiator of room ' + room + '!');
  isInitiator = true;
});

socket.on('join', function (room,Id){
  joinedClients += 1;
  newId = Id;
  console.log(`Now has' ${joinedClients} 'client`)
  console.log(`${newId} made a request to join room ${room}`);
  isChannelReady = true;
});

socket.on('joined', function(room , Id, joinedClients) {
  myId = Id;
  console.log('joined: ' + room + ' ' + myId);
  isChannelReady = true;
});

    // switch (command) {
    //     case 144: // noteOn
    //         if (velocity > 0) {
    //             pianoKeyDown = true;
    //             triggerMouseEvent(pianoKey, "mousedown");
    //         } else {
    //             pianoKeyDown = false;
    //             triggerMouseEvent(pianoKey, "mouseup");
    //         }
    //         break;
    //     case 128: // noteOff
    //         pianoKeyDown = false;
    //         triggerMouseEvent(pianoKey, "mouseup");
    //         break;
    //     default:
    //         console.log(`Something went terribly wrong! \nCommand value is neither 144 nor 128: ${command}`);
    // }

function sendMessage(message) {
  console.log('Client sending message: ', message);
  socket.emit('message', message);
}

// This client receives a message
socket.on('message', function(message, id) {
  console.log('Client received message:', message);
  if (message.getUserMedia === 'got user media') {
    maybeStart(id);
  } else if (message.id && hasConnection.indexOf(message.id) === -1){
    if (message.session.type === 'offer') {
      if (!isInitiator) {
      maybeStart(id);
    }
    pc[message.id].setRemoteDescription(new RTCSessionDescription(message.session));
    
    doAnswer(id); 
    }else if (message.session.type === 'answer' && isStarted) {
      if (message.target === myId){
    pc[message.id].setRemoteDescription(new RTCSessionDescription(message.session));
    }else{
      console.log('not for me')
    }
    }
   else if (message.session.type === 'candidate' && isStarted) {
     if (message.target === myId){
    var candidate = new RTCIceCandidate({
      sdpMLineIndex: message.session.label,
      candidate: message.session.candidate
    });
    pc[message.id].addIceCandidate(candidate);
    hasConnection.push(message.id)
    console.log(message.id + 'is connected')
    }
  }else{
    console.log('not for me')
  }
  }  
  else if (message.id && hasConnection.indexOf(message.id) !== -1){
    console.log('connection on, no action required')
  }
  else if (message === 'bye' && isStarted) {
    handleRemoteHangup(id);
    }
  }
);

////////////////////////////////////////////////////

var localVideo = document.querySelector('#localVideo');
// var remoteVideo = document.querySelector('#remoteVideo');
// var remoteVideo2 = document.querySelector('#remoteVideo2');

navigator.mediaDevices.getUserMedia({
  audio: true,
  video: false
})
.then(gotStream)
.catch(function(e) {
  alert('getUserMedia() error: ' + e.name);
});

function gotStream(stream) {
  console.log('Adding local stream.');
  localStream = stream;
  localVideo.srcObject = stream;
  let message = {
    getUserMedia:'got user media',
    id:myId
  }
  sendMessage(message);
  if (isInitiator) {
    maybeStart('null');
  }
}

var constraints = {
  video: true
};

console.log('Getting user media with constraints', constraints);

if (location.hostname !== 'localhost') {
  requestTurn(
    'https://computeengineondemand.appspot.com/turn?username=41784574&key=4080218913'
  );
}

function maybeStart(socketId) {
  console.log('>>>>>>> maybeStart() ', isStarted, localStream, isChannelReady);
  if ( typeof localStream !== 'undefined' && isChannelReady) {
    console.log('>>>>>> creating peer connection');
    createPeerConnection(socketId);
    pc[socketId].addStream(localStream);
    isStarted = true;
    console.log('isInitiator', isInitiator);
    if (joinedClients >= 1 ) {
      doCall(socketId);
    }
  }
}

window.onbeforeunload = function() {
  sendMessage('bye');
};

/////////////////////////////////////////////////////////

function createPeerConnection(socketId) {
  try {
    pc[socketId] = new RTCPeerConnection(null);
    pc[socketId].onicecandidate = function(event) {
      handleIceCandidate(event,socketId);
    }
    pc[socketId].onaddstream = function(event){
    handleRemoteStreamAdded(event, socketId);
    }
    pc[socketId].onremovestream = handleRemoteStreamRemoved;
    console.log('Created RTCPeerConnection');
  } catch (e) {
    console.log('Failed to create PeerConnection, exception: ' + e.message);
    alert('Cannot create RTCPeerConnection object.');
    return;
  }
}

function handleIceCandidate(event,socketId) {
  console.log('icecandidate event: ', event);
  if (event.candidate) {
    sendMessage(
      {id:myId,
        target:socketId,
        session:
        {
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
        }
      }
    );
  } else {
    console.log('End of candidates.');
  }
}

function handleCreateOfferError(event) {
  console.log('createOffer() error: ', event);
}

function doCall(socketId) {
  console.log('Sending offer to peer');
  // pc[socketId].createOffer(setLocalAndSendMessage, handleCreateOfferError);
  pc[socketId].createOffer().then(function(offer) {
    let offerMsg = {
      session:offer,
      id:myId,
      target:socketId
    }
    console.log('setLocalAndSendMessage sending message', offerMsg);
    sendMessage(offerMsg);
    return pc[socketId].setLocalDescription(offer);
  })
  .catch(function(event) {
    console.log('createOffer() error: ', event);
  });
  console.log('call you'+ ' '+ socketId)
}

function doAnswer(socketId) {
  console.log('Sending answer to peer.');
  pc[socketId].createAnswer().then(function(answer) {
    let offerMsg = {
      session:answer,
      id:myId,
      target:socketId
    }
    console.log('setLocalAndSendMessage sending message', offerMsg);
    sendMessage(offerMsg);
    return pc[socketId].setLocalDescription(answer);
  })
  .catch(function(event) {
    console.log('createAnswer() error: ', event);
  });
  console.log('answer you' + ' ' + socketId)
}

// function setLocalAndSendMessage(sessionDescription) {
//   pc[newId].setLocalDescription(sessionDescription);
//     let offerMsg = {
//     session:sessionDescription,
//     id:myId,
//     target:targetId
//   }
//   console.log('setLocalAndSendMessage sending message', offerMsg);
//   sendMessage(offerMsg);
// }

// function onCreateSessionDescriptionError(error) {
//   trace('Failed to create session description: ' + error.toString());
// }

function requestTurn(turnURL) {
  var turnExists = false;
  for (var i in pcConfig.iceServers) {
    if (pcConfig.iceServers[i].urls.substr(0, 5) === 'turn:') {
      turnExists = true;
      turnReady = true;
      break;
    }
  }
  if (!turnExists) {
    console.log('Getting TURN server from ', turnURL);
    // No TURN server. Get one from computeengineondemand.appspot.com:
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var turnServer = JSON.parse(xhr.responseText);
        console.log('Got TURN server: ', turnServer);
        pcConfig.iceServers.push({
          'urls': 'turn:' + turnServer.username + '@' + turnServer.turn,
          'credential': turnServer.password
        });
        turnReady = true;
      }
    };
    xhr.open('GET', turnURL, true);
    xhr.send();
  }
}

  function handleRemoteStreamAdded(event, socketId) {
    // if  (connection ===0){
    // remoteStream1 = event.stream;
    // remoteVideo1.srcObject = remoteStream1;
    // console.log('Remote stream 1 added.');
    // }else
    //   {
    //   remoteStream2 = event.stream;
    //   remoteVideo2.srcObject = remoteStream2;
    //   console.log('Remote stream 2 added.');
    //  }
    //  connection += 1
    remoteStream = event.stream;
    var videos = document.querySelectorAll('videos')
    var video  = document.createElement('video')
    var div    = document.createElement('div')
  
  video.setAttribute('id', `stream${socketId}`);
  video.srcObject   = remoteStream;
  video.autoplay    = true; 
  video.muted       = false;
  video.controls    = true;
  video.playsinline = true;
  
  div.appendChild(video);      
  document.querySelector('.videos').appendChild(div);  
  console.log('Remote stream added.');
  }

function handleRemoteStreamRemoved(event) {
  console.log('Remote stream removed. Event: ', event);
}

function hangup() {
  console.log('Hanging up.');
  stop();
  sendMessage('bye');
}

function handleRemoteHangup(id) {
  console.log('Session terminated.');
  stop(id);
  isInitiator = false;
}

function stop(id) {
  hasConnection.splice(hasConnection.indexOf(id),1)
  if (hasConnection.length === 0){
  isStarted = false;
  }
  pc[id].close();
  let video = document.querySelector(`#stream${id}`);
  let parentDiv = video.parentElement;
  video.parentElement.parentElement.removeChild(parentDiv);
  delete pc[id];
  console.log('remote stream removed')
  console.log(hasConnection.length + ' connection(s) remaining')
}


window.onload = () => {
    destroyContext();
    pianoKeyScroll.scrollLeft = findKeyLocation(60);
};

let midi;
let midiData;

// start talking to MIDI controller
if (navigator.requestMIDIAccess) {
    navigator.requestMIDIAccess({
        sysex: false
    }).then(onMidiSuccess, onMidiFailure);
} else {
    console.warn("No MIDI support in your browser")
}

function onMidiSuccess(midiData) {
    // this is all our MIDI data
    midi = midiData;
    var inputs = midi.inputs;
    var outputs = midi.outputs;
    // var allInputs = midi.inputs.values();
    var allInputs = inputs.values();
    // loop over all available inputs and listen for any MIDI input
    for (var input = allInputs.next(); input && !input.done; input = allInputs.next()) {
        // when a MIDI value is received call the onMIDIMessage function
        input.value.onmidimessage = gotMidiMessage;
    }
}

function gotMidiMessage(messageData) {

    const command = messageData.data[0];
    const midiNote = messageData.data[1];
    const velocity = messageData.data.length > 2 ? messageData.data[2] : 0;

    const keyId = `midiNote-${midiNote}`;
    const pianoKey = document.querySelector(`#${keyId}`);

    addNoteToMidiList(command, midiNote, velocity);

    switch (command) {
        case 144: // noteOn
            if (velocity > 0) {
                pianoKeyDown = true;
                triggerMouseEvent(pianoKey, "mousedown");
            } else {
                pianoKeyDown = false;
                triggerMouseEvent(pianoKey, "mouseup");
            }
            break;
        case 128: // noteOff
            pianoKeyDown = false;
            triggerMouseEvent(pianoKey, "mouseup");
            break;
        default:
            console.log(`Something went terribly wrong! \nCommand value is neither 144 nor 128: ${command}`);
    }

}

// on failure
function onMidiFailure() {
    console.warn("Not recognising MIDI controller")
}

// specify class Voice
class Voice {

    constructor(context) {
        this.context = context;
        this.oscillators = {};
        // this.synthesizers = {};
        this.amplifier = this.context.createGain();
    };

    // start a voice with vco & vca
    start(midiNote = -1, gainValue = volumeToGain(volume), startTime = 0, type = type) {
        // initialize vco & vca
        console.log(`starting a voice at midiNote ${midiNote}...`);
        let vco = this.context.createOscillator();
        let vca = this.context.createGain();

        // specify attributes of vco and vca
        vco.type = type;
        vco.frequency.value = this.midiNoteToFrequency(midiNote);
        console.log({ gainValue });
        vca.gain.value = gainValue;
        // this.amplifier.gain.linearRampToValueAtTime(gainValue, startTime);

        // setup connections
        // vco.connect(this.amplifier);
        // this.amplifier.connect(this.context.destination);
        vco.connect(vca);
        vca.connect(this.context.destination);
        vco.start(startTime);

        // store used vco and vca into synthesizers
        this.oscillators[midiNote] = vco;
        this.amplifier = vca;
        // this.synthesizers[midiNote] = { oscillator: vco, amplifier: vca };
    }

    // stop a voice
    stop(time = 0) {
        // console.log("stopping all synthesizers...");
        // const midiNotes = Object.keys(this.synthesizers);
        // console.log(this.synthesizers);
        console.log("stopping all oscillators...");
        const midiNotes = Object.keys(this.oscillators);
        console.log(this.oscillators);
        for (let midiNote of midiNotes) {
            // this.synthesizers[midiNote].oscillator.stop(time);
            this.oscillators[midiNote].stop(time);
        }
        // console.log("this.synthesizers = ", this.synthesizers);
        console.log("this.oscillators = ", this.oscillators);
    }

    // play midi note
    playNote(midiNote, duration, gainValue = volumeToGain(volume), startTime = 0, type = type) {
        let time = startTime + duration;
        this.start(midiNote, gainValue, startTime, type);
        activeVoices[midiNote] = this;
        this.stop(time);
    }

    // play a midi chord
    playChord(midiNotes, duration, gainValue = volumeToGain(volume), startTime = 0, type = type) {
        let time = startTime + duration;
        console.log({ time, startTime, duration });
        if (midiNotes.length > 0) {
            const gain = gainValue / midiNotes.length;
            for (let midiNote of midiNotes) {
                this.start(midiNote, gain, startTime, type);
                this.amplifier.gain.setTargetAtTime(0, time - eps, timeConstant);
                this.stop(time);
            }
        }
        //  else {
        //     // this.amplifier.gain.setTargetAtTime(0, startTime, time);

        //     // this.start(-1, 0, startTime, type);
        //     // this.stop(time);
        // }
    }

    // play monophonic midi array
    playMonophone(midiArray, length, eps = 0.01, gainValue = volumeToGain(volume), type = type, timeConstant = 0.001) {
        console.log({ midiArray });
        if (midiArray.length > 0) {
            activeVoices["-1"] = this;
            this.start(-1, gainValue, 0, type);
            let time = this.context.currentTime + eps;
            // console.log(`this.context.currentTime = ${this.context.currentTime}`);
            midiArray.forEach(midiItem => {
                const [midiNote, noteFraction] = midiItem;
                const freq = this.midiNoteToFrequency(midiNote);
                console.log({ midiNote, freq, noteFraction, time, eps });
                // this.synthesizers["-1"].oscillator.frequency.setTargetAtTime(0, time - eps, timeConstant);
                // this.synthesizers["-1"].oscillator.frequency.setTargetAtTime(freq, time, timeConstant);
                this.oscillators["-1"].frequency.setTargetAtTime(0, time - eps, timeConstant);
                this.oscillators["-1"].frequency.setTargetAtTime(freq, time, timeConstant);
                time += length / noteFraction;
            });
            this.stop(time);
            setTimeout(() => {
                delete activeVoices["-1"];
            }, time * 1000);
        }
    }

    // play polyphonic midi array
    playPolyphone(midiArray, length, gainValue = volumeToGain(volume), type = type) {
        console.log({ midiArray });
        if (midiArray.length > 0) {
            activeVoices["-1"] = this;
            let time = this.context.currentTime;
            let duration;
            // console.log(`this.context.currentTime = ${this.context.currentTime}`);
            midiArray.forEach(midiItem => {
                const [midiNotes, noteFraction] = midiItem;
                duration = length / noteFraction;
                this.playChord(midiNotes, duration, gainValue, time, type);
                time += duration;
                setTimeout(() => {
                    // delete this.synthesizers[midiNotes];
                    delete this.oscillators[midiNotes];
                }, time * 1000);
            });
            setTimeout(() => {
                delete activeVoices["-1"];
            }, time * 1000);
        }
    }

    // translate midi note to frequency
    midiNoteToFrequency(midiNote) {
        let frequency;
        if (midiNote >= 0 && midiNote < 128) {
            frequency = (2 ** ((midiNote - 69) / 12)) * 440;
        } else {
            frequency = 0;
        }
        return frequency;
    }

}

// initialize context, oscillator type, previous volume, volume and gain value
let context;
let type = "sine";
let previousVolume = 50;
let volume = 50;

// setup storage for active voices
let activeVoices = {};

// specify length and eps
const length = 2;
const eps = 0.001;
const timeConstant = 0.001;

// initialize accessible octave
let accessibleOctave = 4;

// initialize context
function getOrCreateContext() {
    if (!context) {
        context = new AudioContext();
    }
    return context;
}

// destroy context
function destroyContext() {
    context = null;
    activeVoices = {};
    return context;
}

// specify noteOn function
function noteOn(midiNote) {
    context = getOrCreateContext();
    let voice = new Voice(context);
    const gainValue = volumeToGain(volume);
    voice.start(midiNote, gainValue, 0, type);
    activeVoices[midiNote] = voice;
}

// specify noteOff function
function noteOff(midiNote) {
    activeVoices[midiNote].stop(0);
    delete activeVoices[midiNote];
}

// specify stop sound function
function stopSound() {
    if (context) {
        context.suspend();
    }
}

// add note to the midi list
function addNoteToMidiList(command, midiNote, velocity) {
    const timestamp = performance.now();
    const newItem = document.createElement('li');
    const midiData = [command, midiNote, velocity];
    const noteItem = { midiData: midiData, timestamp: timestamp };
    console.log({ noteItem });
    newItem.innerHTML = JSON.stringify(noteItem);
    dataList.appendChild(newItem);
    midiListScroll.scrollTop = midiListScroll.scrollHeight;
}

// play midi array
function playMidiArray(midiArray, phonic, length, eps = 0.01, gainValue = volumeToGain(volume), type = type, timeConstant = 0.001) {
    switch (phonic) {
        case "mono":
            playMonophonicMidi(midiArray, length, eps, gainValue, type, timeConstant);
            break;
        case "poly":
            playPolyphonicMidi(midiArray, length, gainValue, type);
            break;
        default:
            console.log(`Invalid phonic style: ${phonic}\nPhonic style can either be "mono" or "poly"!`);
    }
};

// play tetris midi
function playTetrisMidi(length, eps = 0.01, gainValue = volumeToGain(volume)) {
    playMonophonicMidi(tetris, length, eps, gainValue, type, timeConstant);
};

// play monophonic midi array
function playMonophonicMidi(midiArray, length, eps = 0.01, gainValue = volumeToGain(volume), type = type, timeConstant = 0.001) {
    context = getOrCreateContext();
    let voice = new Voice(context);
    voice.playMonophone(midiArray, length, eps, gainValue, type, timeConstant);
}

// play polyphonic midi array
function playPolyphonicMidi(midiArray, length, gainValue = volumeToGain(volume), type = type) {
    context = getOrCreateContext();
    let voice = new Voice(context);
    voice.playPolyphone(midiArray, length, gainValue, type);
}

// play midi list
function playMidiList(length, eps = 0.01, gainValue = volumeToGain(volume), type = type, timeConstant = 0.001) {
    // assign phonic style
    let phonic;
    if (monopoly.checked) {
        phonic = "poly";
    } else {
        phonic = "mono"
    }
    const midiArray = convertToMidiArray(phonic, length);
    playMidiArray(midiArray, phonic, length, eps, gainValue, type, timeConstant);
}

// clear midi list
function clearMidiList() {
    dataList.innerHTML = "";
}

// convert midi list to midi array
function convertToMidiArray(phonic, length) {
    let midiArray = [];
    let listItems = [...dataList.children];
    // console.log({ listItems });
    const noteItems = listItems.map(listItem => {
        let noteItem = JSON.parse(listItem.innerHTML);
        // console.log({ noteItem });
        return noteItem;
    });
    // console.log({ noteItems });
    switch (phonic) {
        case "mono":
            let onNote;
            let filteredNoteItems = noteItems
                .filter((noteItem) => {
                    const { midiData, timestamp } = noteItem;
                    const [command, midiNote, velocity] = midiData;
                    const isNoteOff = command == 128 || velocity == 0;
                    if (!isNoteOff) {
                        onNote = midiNote;
                    }
                    const mismatchOnNote = midiNote != onNote;
                    // console.log({ onNote, midiNote, isNoteOff, mismatchOnNote, timestamp });
                    return !(isNoteOff && mismatchOnNote);
                });
            midiArray = filteredNoteItems
                .map((noteItem, index) => {
                    const { midiData, timestamp } = noteItem;
                    const [command, midiNote, velocity] = midiData;
                    const nextTimestamp = filteredNoteItems[index + 1] ? filteredNoteItems[index + 1].timestamp : (timestamp + length * 50);
                    const elapsedTime = (nextTimestamp - timestamp) / 1000 > 0 ? (nextTimestamp - timestamp) / 1000 : 0.01;
                    const noteFraction = length / elapsedTime;
                    // console.log({ midiData, timestamp, nextTimestamp, elapsedTime, noteFraction });
                    if (command == 144 && velocity != 0) {
                        return [midiNote, noteFraction];
                    } else if (command == 128 || velocity == 0) {
                        return [-1, noteFraction];
                    } else {
                        console.log(`Something went terribly wrong! \nCommand value is neither 144 nor 128: ${command}`);
                        return;
                    }
                });
            // console.log({ noteItems, filteredNoteItems, midiArray });
            break;
        case "poly":
            let onNotes = [];
            let noteTimeline = {};
            noteItems.forEach(noteItem => {
                const { midiData, timestamp } = noteItem;
                const [command, midiNote, velocity] = midiData;

                if (!(timestamp in noteTimeline)) {
                    noteTimeline[timestamp] = [];
                }
                if (command == 144 && velocity != 0) {
                    onNotes.push(midiNote);
                } else if (command == 128 || velocity == 0) {
                    onNotes = onNotes.filter(onNote => onNote != midiNote);
                } else {
                    console.log(`Something went terribly wrong for this noteItem: \n${noteItem}`);
                }
                noteTimeline[timestamp].push(...onNotes);
            });
            console.log({ noteTimeline });
            const noteRecords = Object.entries(noteTimeline);
            console.log({ noteRecords });
            midiArray = noteRecords
                .map((noteRecord, index) => {
                    const [timestampString, midiNotes] = noteRecord;
                    const timestamp = parseFloat(timestampString);
                    const nextTimestamp = noteRecords[index + 1] ? parseFloat(noteRecords[index + 1][0]) : (timestamp + length * 100);
                    const elapsedTime = (nextTimestamp - timestamp) / 1000 > 0 ? (nextTimestamp - timestamp) / 1000 : 0.01;
                    const noteFraction = length / elapsedTime;
                    console.log({ midiNotes, timestamp, nextTimestamp, elapsedTime, noteFraction });
                    return [midiNotes, noteFraction];
                });
            break;
        default:
            console.log(`Invalid phonic style: ${phonic}\nPhonic style can either be "mono" or "poly"!`);
    }
    // console.log({ midiArray });
    return midiArray;
}

// smooth scroll on piano keys
function smoothScrollLeft(scrollbar, pixels, direction) {
    let scrolled = 0;
    const step = pixels / 4;
    let scrollTimer = setInterval(() => {
        switch (direction) {
            case "left":
                scrollbar.scrollLeft -= step;
                break;
            case "right":
                scrollbar.scrollLeft += step;
                break;
            default:
                console.log(`Invalid direction: ${direction}\nDirection can only be "left" or "right"!`);
        }
        scrolled += step;
        if (scrolled >= pixels) {
            window.clearInterval(scrollTimer);
        }
    }, 15);
}

// trigger mouse event function
function triggerMouseEvent(node, eventType) {
    let mouseEvent = document.createEvent("MouseEvents");
    mouseEvent.initEvent(eventType, true, true);
    node.dispatchEvent(mouseEvent);
    // console.log(`Mouse event "${eventType}" has been triggered on node "${node.id}"!`);
}

let pianoKeyDown = false;

// keydown listener
document.addEventListener("keydown", (event) => {
    // console.log(event);
    if (event.key in emulatedKeys && !(event.ctrlKey)) {
        const midiNote = emulatedKeys[event.key];
        if (!activeVoices[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKeyDown = true;
            triggerMouseEvent(pianoKey, "mousedown");
            console.log({ midiNote, keyId, pianoKey });
        }
    } else if (event.key.startsWith('Arrow')) {
        const direction = event.key.replace('Arrow', "")[0].toLowerCase() + event.key.replace('Arrow', "").slice(1);
        if (direction == 'up' || direction == 'down') {
            let shift;
            switch (direction) {
                case 'up':
                    shift = 1;
                    break;
                case 'down':
                    shift = -1;
                    break;
                default:
                    console.log(`Something went terribly wrong with the direction: ${direction}!`);
            }
            const shiftedOctave = accessibleOctave + shift;
            if (shiftedOctave > -2 && shiftedOctave < 8) {
                const accessibles = document.querySelectorAll(".accessible");
                // console.log({ accessibles });
                const keys = Object.keys(emulatedKeys);
                for (let accessible of accessibles) {
                    accessible.classList.remove("accessible");
                }
                for (let key of keys) {
                    emulatedKeys[key] += 12 * shift;
                    const keyId = `midiNote-${emulatedKeys[key]}`;
                    const pianoKey = document.querySelector(`#${keyId}`);
                    pianoKey.classList.add("accessible");
                }
                accessibleOctave = shiftedOctave;
                console.log({ accessibleOctave });
            }
        } else if (direction == 'left' || direction == 'right') {
            let shiftUnit;
            if (event.ctrlKey) {
                shiftUnit = 7;
            } else {
                shiftUnit = 1;
            }
            const shiftKeyId = `shiftKey-${direction}-${shiftUnit}`;
            console.log({ direction, shiftKeyId });
            const shiftKey = document.querySelector(`#${shiftKeyId}`);
            triggerMouseEvent(shiftKey, "click");
        } else {
            console.log(`Something went terribly wrong with the direction: ${direction}!`);
        }
    }
});

document.addEventListener("keydown", (event) => {
    socket.emit('keydown-from-others', event.key, event.ctrlKey)
    // console.log(event);
    if (event.key in emulatedKeys && !(event.ctrlKey)) {
        const midiNote = emulatedKeys[event.key];
        if (!activeVoices[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKeyDown = true;
            triggerMouseEvent(pianoKey, "mousedown");
            console.log({ midiNote, keyId, pianoKey });
        }
    } else if (event.key.startsWith('Arrow')) {
        const direction = event.key.replace('Arrow', "")[0].toLowerCase() + event.key.replace('Arrow', "").slice(1);
        if (direction == 'up' || direction == 'down') {
            let shift;
            switch (direction) {
                case 'up':
                    shift = 1;
                    break;
                case 'down':
                    shift = -1;
                    break;
                default:
                    console.log(`Something went terribly wrong with the direction: ${direction}!`);
            }
            const shiftedOctave = accessibleOctave + shift;
            if (shiftedOctave > -2 && shiftedOctave < 8) {
                const accessibles = document.querySelectorAll(".accessible");
                // console.log({ accessibles });
                const keys = Object.keys(emulatedKeys);
                for (let accessible of accessibles) {
                    accessible.classList.remove("accessible");
                }
                for (let key of keys) {
                    emulatedKeys[key] += 12 * shift;
                    const keyId = `midiNote-${emulatedKeys[key]}`;
                    const pianoKey = document.querySelector(`#${keyId}`);
                    pianoKey.classList.add("accessible");
                }
                accessibleOctave = shiftedOctave;
                console.log({ accessibleOctave });
            }
        } else if (direction == 'left' || direction == 'right') {
            let shiftUnit;
            if (event.ctrlKey) {
                shiftUnit = 7;
            } else {
                shiftUnit = 1;
            }
            const shiftKeyId = `shiftKey-${direction}-${shiftUnit}`;
            console.log({ direction, shiftKeyId });
            const shiftKey = document.querySelector(`#${shiftKeyId}`);
            triggerMouseEvent(shiftKey, "click");
        } else {
            console.log(`Something went terribly wrong with the direction: ${direction}!`);
        }
    }
});

socket.on('keydown-from-others', function (key,ctrlKey) {
    // console.log(event);
    if (key in emulatedKeys && !(ctrlKey)) {
        const midiNote = emulatedKeys[key];
        if (!activeVoices[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKeyDown = true;
            triggerMouseEvent(pianoKey, "mousedown");
            console.log({ midiNote, keyId, pianoKey });
        }
    } else if (key.startsWith('Arrow')) {
        const direction = key.replace('Arrow', "")[0].toLowerCase() + key.replace('Arrow', "").slice(1);
        if (direction == 'up' || direction == 'down') {
            let shift;
            switch (direction) {
                case 'up':
                    shift = 1;
                    break;
                case 'down':
                    shift = -1;
                    break;
                default:
                    console.log(`Something went terribly wrong with the direction: ${direction}!`);
            }
            const shiftedOctave = accessibleOctave + shift;
            if (shiftedOctave > -2 && shiftedOctave < 8) {
                const accessibles = document.querySelectorAll(".accessible");
                // console.log({ accessibles });
                const keys = Object.keys(emulatedKeys);
                for (let accessible of accessibles) {
                    accessible.classList.remove("accessible");
                }
                for (let key of keys) {
                    emulatedKeys[key] += 12 * shift;
                    const keyId = `midiNote-${emulatedKeys[key]}`;
                    const pianoKey = document.querySelector(`#${keyId}`);
                    pianoKey.classList.add("accessible");
                }
                accessibleOctave = shiftedOctave;
                console.log({ accessibleOctave });
            }
        } else if (direction == 'left' || direction == 'right') {
            let shiftUnit;
            if (ctrlKey) {
                shiftUnit = 7;
            } else {
                shiftUnit = 1;
            }
            const shiftKeyId = `shiftKey-${direction}-${shiftUnit}`;
            console.log({ direction, shiftKeyId });
            const shiftKey = document.querySelector(`#${shiftKeyId}`);
            triggerMouseEvent(shiftKey, "click");
        } else {
            console.log(`Something went terribly wrong with the direction: ${direction}!`);
        }
    }
});

// keyup listener
document.addEventListener("keyup", (event) => {
    socket.emit('keyup-from-others', event.key)
    // console.log(event);
    if (event.key in emulatedKeys) {
        const midiNote = emulatedKeys[event.key];
        if (activeVoices[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKeyDown = false;
            triggerMouseEvent(pianoKey, "mouseup");
            console.log({ midiNote, keyId, pianoKey });
        }
    }
});
socket.on("keyup-from-others", function(key) {
    // console.log(event);
    if (key in emulatedKeys) {
        const midiNote = emulatedKeys[key];
        if (activeVoices[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKeyDown = false;
            triggerMouseEvent(pianoKey, "mouseup");
            console.log({ midiNote, keyId, pianoKey });
        }
    }
});




// DOM selection
// bootstrap slider
// https://www.w3schools.com/howto/howto_js_rangeslider.asp
const volumeSlider = document.querySelector("#volumeSlider");
const outputVolume = document.querySelector("#outputVolume");


const playTetris = document.querySelector("#playTetris");
const typeButtons = document.querySelectorAll(".typeButton");

const trackListScroll = document.querySelector(".trackListScroll");
const trackList = document.querySelector("#trackList");

const midiListScroll = document.querySelector(".midiListScroll");
const dataList = document.querySelector('#midiList')

const clearButton = document.querySelector("#clearButton");
const playButton = document.querySelector("#playButton");
const monopoly = document.querySelector("#monopoly");
const addToTrackButton = document.querySelector("#addToTrackButton");
// const stopButton = document.querySelector("#stopButton");

const shiftKeys = document.querySelectorAll(".shiftKey");
const shiftKeyLeft7 = document.querySelector("#shiftKeyLeft7");
const shiftKeyLeft1 = document.querySelector("#shiftKeyLeft1");
const shiftKeyRight1 = document.querySelector("#shiftKeyRight1");
const shiftKeyRight7 = document.querySelector("#shiftKeyRight7");

const pianoKeyScroll = document.querySelector(".pianoKeyScroll");
const pianoKeys = document.querySelectorAll(".pianoKey");


// add event listener
playTetris.addEventListener("click", (event) => {
    const gainValue = volumeToGain(volume);
    playTetrisMidi(length, eps, gainValue);
});

clearButton.addEventListener("click", clearMidiList);
playButton.addEventListener("click", (event) => {
    // playButton.classList.add("active");
    const gainValue = volumeToGain(volume);
    playMidiList(length, eps, gainValue, type, timeConstant);
});
// stopButton.addEventListener("click", stopSound);

outputVolume.innerHTML = volumeSlider.value; // Display the default slider value

let savedGainValues = {};
let muted = false;

// update the current slider value
volumeSlider.oninput = function () {
    volume = parseInt(this.value, 10);
    outputVolume.innerHTML = volume;
    // console.log({ volume });
    if (volume == 0) {
        const midiNotes = Object.keys(activeVoices);
        console.log({ midiNotes });
        for (let midiNote of midiNotes) {
            // savedGainValues[midiNote] = {};
            // let synthesizers = activeVoices[midiNote].synthesizers;
            // const notes = Object.keys(synthesizers);
            let amplifier = activeVoices[midiNote].amplifier;
            savedGainValues[midiNote] = amplifier.gain.value;
            activeVoices[midiNote].amplifier.gain.value = 0;
            console.log(`activeVoices[${midiNote}].amplifier.gain.value = ${activeVoices[midiNote].amplifier.gain.value}`);

            // const notes = Object.keys(oscillators);
            // console.log({ notes });
            // for (let note of notes) {
            //     savedGainValues[midiNote][note] = synthesizers[note].amplifier.gain.value;
            //     // synthesizers[note].amplifier.gain.setTargetAtTime(0, context.currentTime, 0);
            //     // synthesizers[note].amplifier.gain.value -= synthesizers[note].amplifier.gain.value;
            //     // synthesizers[note].amplifier.gain.value = 0;
            //     // synthesizers[note].amplifier.gain.linearRampToValueAtTime(0, timeConstant);
            //     synthesizers[note].amplifier.disconnect();
            //     console.log(`synthesizers[${note}].amplifier.gain.value = ${synthesizers[note].amplifier.gain.value}`);
            // }
        }
        muted = true;
        console.log({ muted, savedGainValues });
    } else if (context) {
        if (muted) {
            const midiNotes = Object.keys(activeVoices);
            for (let midiNote of midiNotes) {
                // let synthesizers = activeVoices[midiNote].synthesizers;
                // const notes = Object.keys(synthesizers);
                // const notes = Object.keys(synthesizers);
                // console.log({ notes });
                let amplifier = activeVoices[midiNote].amplifier;
                amplifier.gain.value = savedGainValues[midiNote] ;
                console.log(`activeVoices[${midiNote}].amplifier.gain.value = ${activeVoices[midiNote].amplifier.gain.value}`);
                
                // for (let note of notes) {
                //     console.log({ savedGainValues });
                //     // synthesizers[note].amplifier.gain.value = savedGainValues[midiNote][note];
                //     synthesizers[note].amplifier.connect(context.destination);
                //     // synthesizers[note].amplifier.gain.linearRampToValueAtTime(savedGainValues[midiNote][note], timeConstant);
                //     console.log(`synthesizers[${note}].amplifier.gain.value = ${synthesizers[note].amplifier.gain.value}`);
                // }
            }
            savedGainValues = {};
            muted = false;
            console.log({ muted, savedGainValues });
        }
        let gain = (volume / previousVolume) ** 2;
        // let gain = volume / previousVolume;
        console.log({ volume, previousVolume, gain });

        previousVolume = volume;
        const midiNotes = Object.keys(activeVoices);
        for (let midiNote of midiNotes) {
            // let synthesizers = activeVoices[midiNote].synthesizers;
            // const notes = Object.keys(synthesizers);
            // console.log({ notes });
            let amplifier = activeVoices[midiNote].amplifier;
            let gainValue = (amplifier.gain.value * gain).toFixed(2);
            amplifier.gain.linearRampToValueAtTime(gainValue, timeConstant);
            console.log(`activeVoices[${midiNote}].amplifier.gain.value = ${activeVoices[midiNote].amplifier.gain.value}`);


            // for (let note of notes) {
            //     let gainValue = synthesizers[note].amplifier.gain.value * gain;
            //     console.log(`synthesizers[${note}].amplifier.gain.value * gain = ${synthesizers[note].amplifier.gain.value} * ${gain}`);
            //     synthesizers[note].amplifier.gain.linearRampToValueAtTime(gainValue, timeConstant);
            //     // synthesizers[note].amplifier.gain.value *= gain;
            //     console.log(`synthesizers[${note}].amplifier.gain.value = ${synthesizers[note].amplifier.gain.value}`);
            // }
        }
    }
}

for (let typeButton of typeButtons) {
    typeButton.addEventListener("click", (event) => {
        // console.log(event);
        // console.log("event.currentTarget = ", event.currentTarget);
        // console.log("event.currentTarget.value = ", event.currentTarget.value);
        // console.log("event.currentTarget.checked = ", event.currentTarget.checked);
        if (event.currentTarget.checked) {
            type = event.currentTarget.value;
            console.log({ type });
            const midiNotes = Object.keys(activeVoices);
            for (let midiNote of midiNotes) {
                console.log(`activeVoices[${midiNote}].oscillators = ${activeVoices[midiNote].oscillators}`);
                let oscillators = activeVoices[midiNote].oscillators;
                const notes = Object.keys(oscillators);
                console.log({ notes });
                for (let note of notes) {
                    oscillators[note].type = type;
                    console.log(`oscillators[${note}].type = ${oscillators[note].type}`);
                }
            }
        }
    });
}

for (let pianoKey of pianoKeys) {
    pianoKey.addEventListener("mousedown", keyPressed, false);
    pianoKey.addEventListener("mouseover", keyPressed, false);
    pianoKey.addEventListener("mouseup", keyReleased, false);
    pianoKey.addEventListener("mouseleave", keyReleased, false);
}

function keyPressed(event) {
    if ((event.buttons || pianoKeyDown) && !event.currentTarget.classList.contains('active')) {
        const keyId = event.currentTarget.id;
        const midiNote = parseInt(keyId.split("-")[1], 10);
        console.log({ keyId, midiNote });
        event.currentTarget.classList.add("active");
        noteOn(midiNote);
        addNoteToMidiList(144, midiNote, 127);
    }
}

function keyReleased(event) {
    if (event.currentTarget.classList.contains('active')) {
        const keyId = event.currentTarget.id;
        const midiNote = parseInt(keyId.split("-")[1], 10);
        console.log({ keyId, midiNote });
        event.currentTarget.classList.remove("active");
        noteOff(midiNote);
        addNoteToMidiList(128, midiNote, 0);
    }
}

for (let shiftKey of shiftKeys) {
    shiftKey.addEventListener("click", (event) => {
        const shiftKeyId = event.currentTarget.id;
        const [name, direction, shift] = shiftKeyId.split("-");
        const shiftUnit = parseInt(shift, 10);
        smoothScrollLeft(pianoKeyScroll, 48 * shiftUnit, direction);
    });
}

addToTrackButton.addEventListener("click", (event) => {
    let phonic;
    if (monopoly.checked) {
        phonic = "poly";
    } else {
        phonic = "mono"
    }
    const midiArray = convertToMidiArray(phonic, length);
    addMidiToTrackList(midiArray, phonic);
});

// convert volume to gain value
function volumeToGain(volume) {
    const gainValue = (volume / 100) ** 2;
    // const gainValue = volume / 100;
    return gainValue;
}


// add midi array to track list
function addMidiToTrackList(midiArray, phonic) {
    const trackList = document.querySelector("#trackList");
    const trackItem = document.createElement('li');
    const trackId = [...trackList.children].length;
    const midiTrackItem = { phonic: phonic, midiArray: midiArray };
    console.log({ trackId, phonic, midiArray });
    trackItem.id = `trackItem-${trackId}`;
    trackItem.innerHTML = `
        <button id="deleteTrackButton-${trackId}" data-id="${trackId}" class="themeButton trackControlButton deleteTrackButton deleteTrackButton-${trackId}">
            <i class="fas fa-times"></i>
        </button>
        <input type="text" id="trackName-${trackId}" name="trackName" data-id="${trackId}" class="trackName trackName-${trackId}" value="Track_${trackId}" required>
        <div class="scrollbar scrollbar-primary midiArrayScroll">
            <div class="force-overflow">
                <span id="midiArrayTrack-${trackId}" data-id="${trackId}" class="midiArrayTrack midiArrayTrack-${trackId}">
                    ${JSON.stringify(midiTrackItem)}
                </span>
            </div>
        </div>
        <button id="playTrackButton-${trackId}" data-id="${trackId}" class="themeButton trackControlButton playTrackButton playTrackButton-${trackId}">
            <i class="fas fa-play"></i>
            <i class="fas fa-pause"></i>
        </button>
        <button id="exportTrackButton-${trackId}" data-id="${trackId}" class="themeButton trackControlButton exportTrackButton exportTrackButton-${trackId}">
            <i class="fas fa-file-export"></i>
        </button>
    `;
    trackList.appendChild(trackItem);
    console.log({ trackItem });
    trackListScroll.scrollTop = trackListScroll.scrollHeight;

    const midiArrayTrack = document.querySelector(`#midiArrayTrack-${trackId}`);
    const trackName = document.querySelector(`trackName-${trackId}`);
    const deleteTrackButton = document.querySelector(`#deleteTrackButton-${trackId}`);
    const playTrackButton = document.querySelector(`#playTrackButton-${trackId}`);
    const exportTrackButton = document.querySelector(`#exportTrackButton-${trackId}`);

    deleteTrackButton.addEventListener("click", (event) => {
        trackItem.remove();
    });

    playTrackButton.addEventListener("click", (event) => {
        const midiArrayData = JSON.parse(midiArrayTrack.innerHTML);
        console.log({ midiArrayData });
        const { phonic, midiArray } = midiArrayData;
        playMidiArray(midiArray, phonic, length, eps, volumeToGain(volume), type, timeConstant);
    });

    exportTrackButton.addEventListener("click", async (event) => {
        const midiArrayData = JSON.parse(midiArrayTrack.innerHTML);
        console.log({ midiArrayData });
        const { phonic, midiArray } = midiArrayData;

        let format;
        switch (phonic) {
            case "mono":
                format = 0;
                break;
            case "poly":
                format = 1;
                break;
            default:
                console.log(`Invalid phonic style: ${phonic}\nPhonic style can either be "mono" or "poly"!`);
        }

        const body = {
            name: trackName,
            format: format,
            midiArray: midiArray
        }

        const res = await fetch("/music/writeMidiFile", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(body)
        });
        // const { dataUri, result } = await res.json();
        const { midiFile, result } = await res.json();
        const trackAudio = document.createElement('audio');
        console.log({ result });
        trackAudio.setAttribute("controls", "");
        trackAudio.setAttribute("src", midiFile);
        // trackAudio.setAttribute("src", dataUri);
        trackItem.appendChild(trackAudio);

    });

}

// find key location on keyboard
function findKeyLocation(midiNote) {
    const keyLocation = midiNote * 28;
    return keyLocation;
}

// keyboard emulation
const emulatedKeys = {
    "z": 60,
    "s": 61,
    "x": 62,
    "d": 63,
    "c": 64,
    "v": 65,
    "g": 66,
    "b": 67,
    "h": 68,
    "n": 69,
    "j": 70,
    "m": 71,
    ",": 72,
    "l": 73,
    ".": 74,
    ";": 75,
    "/": 76,
    "q": 72,
    "2": 73,
    "w": 74,
    "3": 75,
    "e": 76,
    "r": 77,
    "5": 78,
    "t": 79,
    "6": 80,
    "y": 81,
    "7": 82,
    "u": 83,
    "i": 84,
    "9": 85,
    "o": 86,
    "0": 87,
    "p": 88,
    "[": 89,
    "=": 90,
    "]": 91
}

// This is a tetris theme transposed from https://musescore.com/user/16693/scores/38133
const tetris = [
    [76, 4],
    [71, 8],
    [72, 8],
    [74, 4],
    [72, 8],
    [71, 8],
    [69, 4],
    [69, 8],
    [72, 8],
    [76, 4],
    [74, 8],
    [72, 8],
    [71, 4],
    [71, 8],
    [72, 8],
    [74, 4],
    [76, 4],
    [72, 4],
    [69, 4],
    [69, 4],
    [0, 4],
    [74, 3],
    [77, 8],
    [81, 4],
    [79, 8],
    [77, 8],
    [76, 3],
    [72, 8],
    [76, 4],
    [74, 8],
    [72, 8],
    [71, 4],
    [71, 8],
    [72, 8],
    [74, 4],
    [76, 4],
    [72, 4],
    [69, 4],
    [69, 4],
    [0, 4]
]


// midiData
// [command, midiNote, velocity]
  // command - command value
    // 144 - “note on” event
    // 128 - “note off” event
  // midiNote - note value
    // range: 0–127 (lowest to highest)
      // For example: 88-key piano
        // lowest note: 21
        // highest note: 108
        // “middle C”: 60
  // velocity - velocity value
    // range: 0–127 (softest to loudest)
    // softest possible “note on” velocity = 1
    // A velocity of 0 is sometimes used in conjunction with a command value of 144 (which typically represents “note on”) to indicate a “note off” message, so it’s helpful to check if the given velocity is 0 as an alternate way of interpreting a “note off” message

// Tone.js
// https://tonejs.github.io/?source=post_page---------------------------

// Playing with MIDI in JavaScript
// https://medium.com/swinginc/playing-with-midi-in-javascript-b6999f2913c3

// Audio API + MIDI API example
// https://codepen.io/kulak-at/pen/rdxXwM

// javascript oscillator type
// https://developer.mozilla.org/en-US/docs/Web/API/OscillatorNode/type

// web audio font
// https://surikov.github.io/webaudiofont/examples/midiplayer.html?source=post_page---------------------------#
// https://surikov.github.io/webaudiofont/

// WebAudio API - Monophonic Synthesis
// http://blog.chrislowis.co.uk/2013/06/05/playing-notes-web-audio-api.html

// WebAudio API - Polyphonic Synthesis
// http://blog.chrislowis.co.uk/2013/06/10/playing-multiple-notes-web-audio-api.html

// test codes
