const logoutButton = document.querySelector("#logout");

logoutButton.addEventListener("click", async (event) => {
    event.preventDefault();

    localStorage.clear();

    const res = await fetch("/logout");
    const json = await res.json();

    console.log("window.location.pathname = ", window.location.pathname);
    if (window.location.pathname !== '/login' && window.location.pathname !== '/login.html') {
        window.location.href = "/login";
    }
})