import * as express from "express";
import { playerService } from "../app";
// import { playerRouter } from "../app";
// import { bandRouter } from "../app";
// import { bandAdsRouter } from "../app";

export class ProtectedRouter {

    router() {
        const router = express.Router();

        // // [CODE REVIEW] CheckProfileMiddleware cannot protect the html
        // router.get("/main", checkProfileMiddleware, (req: express.Request, res: express.Response) => res.sendfile("/protected/main.html"));
        // router.get("/profile", checkProfileMiddleware, (req: express.Request, res: express.Response) => res.sendfile("/protected/profile.html"));
        // router.get("/profileSetup", checkProfileMiddleware, (req: express.Request, res: express.Response) => res.sendfile("/protected/profileSetup.html"));
        router.get("/main", (req: express.Request, res: express.Response) => res.sendfile("/protected/main.html"));
        router.get("/profile", (req: express.Request, res: express.Response) => res.sendfile("/protected/profile.html"));
        router.get("/bandRoom", (req: express.Request, res: express.Response) => res.sendfile("/protected/bandRoom.html"));
        router.get("/bands", (req: express.Request, res: express.Response) => res.sendfile("/protected/band.html"));
        // // router.get("/match", checkProfileMiddleware, (req: express.Request, res:express.Response) => res.sendfile("/protected/matchSetup/matchInfo.html"));
        // // router.get("/record", checkProfileMiddleware, (req: express.Request, res:express.Response) => res.sendfile("/protected/matchRecord.html"));
        // // router.get("/chatRoom",checkProfileMiddleware,(req: express.Request, res:express.Response) => res.sendfile("/protected/chatRoom.html"));
        // router.use("/player", playerRouter.router());
        // router.use("/bands", bandRouter.router());
        // router.use("/bandsAds", bandAdsRouter.router());
        // // router.use("/algo", algoRouter.router());
        // // router.use("/match", matchInfoRouter.router());
        // // router.use("/matching", matchRouter.router());
        // // router.use("/chatRoom", chatRoomRouter.router());
        // // router.use("/profile", this.checkProfileMiddleware, profileRouter.router());
        
        return router;
    }

    checkProfileMiddleware = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        console.log("checkProfileMiddleware is called");
        const player = await playerService.getPlayer(req.user.id);
        console.log("player = ", player);
        if (!player) {
            // player is not created yet
            console.log("player is not created yet!");
            res.sendfile("/profileSetup");
        } else {
            // no problem
            console.log("no problem!");
            next();
        }
    }

}