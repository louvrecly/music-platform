import * as Knex from "knex";
import { stdContents, prioritizedSheets } from "../database-init/readExcel";


// Data Definition (DD) - Create Tables
export async function up(knex: Knex): Promise<any> {
    
    for (let i = 0; i < prioritizedSheets.length; i++) {
        
        const sheet: string = prioritizedSheets[i];
        console.log("sheet = ", sheet);

        const hasTable = await knex.schema.hasTable(sheet);
        // console.log("hasTable = ", hasTable);

        if (!hasTable) {
            
            await knex.schema.createTable(sheet, (table) => {
                const columns = Object.keys(stdContents[sheet][0]);
                console.log("columns = ", columns);
                table.increments();
                for (let column of columns) {

                    console.log("column = ", column);
                    const schemas = stdContents['schema'];
                    const schema = schemas.find((record: any) => record.sheet == sheet && record.column == column);
                    
                    if (schema) {

                        const columnType = schema.type;
                        console.log("columnType = ", columnType);
                        switch(columnType) {
                            case 'string': 
                                table.string(column);
                                break;
                            case 'text': 
                                table.text(column);
                                break;
                            case 'integer': 
                                table.integer(column);
                                break;
                            case 'bigInteger': 
                                table.bigInteger(column);
                                break;
                            case 'float': 
                                table.float(column);
                                break;
                            case 'decimal': 
                                table.decimal(column);
                                break;
                            case 'boolean': 
                                table.boolean(column);
                                break;
                            case 'date': 
                                table.date(column);
                                break;
                            case 'dateTime': 
                                table.dateTime(column);
                                break;
                            case 'timestamp': 
                                table.timestamp(column);
                                break;
                            default: 
                                console.log(`Type '${columnType}' is not valid!`);
                        }
                        const ref = schema.references;
                        // console.log("ref = ", ref);
                        let foreign = null;
                        if (ref) {
                            foreign = ref.split('.')[0] + 'id';
                            table.foreign(column).references(foreign);
                        }
                        console.log(` [ column: ${column} | type: ${columnType} | references: ${foreign} ]`);

                    } else {
                        console.log(`"${sheet}" is not found on schema sheet`);
                    }

                }
                table.timestamps();
            });
            console.log(`Table "${sheet}" has been created!`);
        
        } else {
            console.log(`Table "${sheet}" already exists!`);
        }
        
    }

}


// Data Definition (DD) - Drop Tables
export async function down(knex: Knex): Promise<any> {
    
    for (let i = prioritizedSheets.length - 1; i >= 0; i--) {

        const sheet = prioritizedSheets[i];
        // console.log("sheet = ", sheet);

        const hasTable = await knex.schema.hasTable(sheet);
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await knex.schema.dropTable(sheet);
            console.log(`Table "${sheet}" has been dropped!`);
        } else {
            console.log(`Table "${sheet}" does not exist!`);
        }

    }

}