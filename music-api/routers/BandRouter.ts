import * as express from "express";
import * as multer from "multer";
import { BandService } from "../services/BandService";
import { Band } from "../services/models";

export class BandRouter {

    constructor(protected bandService: BandService, private upload: multer.Instance) { }

    public router() {

        const router = express.Router();

        router.get("/", this.loadBands);
        router.get("/ownBand", this.ownBand);
        router.get("/bandMembers/:id", this.loadBandMembers);
        router.get("/loadJoinedBands", this.loadJoinedBands);
        router.get("/:id", this.getBand);
        router.post("/createBand", this.addBand);
        router.put("/editBand", this.editBand);
        router.post("/previewLogoPic", this.upload.single('uploadImg'), this.previewBandImg);
        router.delete("/kickMember/:bandId&:playerId", this.kickMember);
        // router.delete("/band/:bandId/player/:playerId", this.kickMember);
        router.delete("/quitBand/:bandId", this.quitBand);

        return router;
    }

    protected loadBands = async (req: express.Request, res: express.Response) => {
        try {
            const bands = await this.bandService.loadBands();
            res.json(bands);
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected getBand = async (req: express.Request, res: express.Response) => {
        try {
            const [band] = await this.bandService.getBandById(req.params.id as number);
            res.json({ isSuccess: true, data: band });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected ownBand = async (req: express.Request, res: express.Response) => {
        try {
            const band = await this.bandService.ownBand(req.user.id as number);
            res.json(band);
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected loadBandMembers = async (req: express.Request, res: express.Response) => {
        try {
            const members = await this.bandService.loadBandMembers(req.params.id);
            res.json(members);
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected kickMember = async (req: express.Request, res: express.Response) => {
        try {
            const bandId = await this.bandService.deleteMember(req.params.bandId, req.params.playerId);
            res.json(bandId);
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected quitBand = async (req: express.Request, res: express.Response) => {
        try {
            const bandId = await this.bandService.quitBand(req.params.bandId, req.user.id);
            res.json(bandId);
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected loadJoinedBands = async (req: express.Request, res: express.Response) => {
        try {
            const bands = await this.bandService.loadJoinedBands(req.user.id as number);
            res.json(bands);
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected addBand = async (req: express.Request, res: express.Response) => {
        try {

            const newBand: Band = {
                name: req.body.bandName,
                band_img: req.body.band_img,
                founder_id: req.user.id
            }
            console.log("req.user.id = ", req.user.id);
            console.log({ newBand });
            const createdBand = await this.bandService.addBand(newBand);
            res.json({ band: createdBand });
        } catch (e) {
            console.log(e.toString());
            res.json({ band: null, result: e.toString() });
        }
    }

    protected editBand= async (req: express.Request, res: express.Response) => {
        try {
            const editedBand = {
                bandId: req.body.bandId,
                bandName: req.body.bandName,
                band_img: req.body.band_img,
                founder_id: req.user.id
            };
            console.log("editedBand = ", editedBand);
            const band = await this.bandService.editBand(editedBand.bandId, editedBand.bandName,editedBand.band_img,editedBand.founder_id);
            console.log("editedBand = ", band);
            res.json({ edit: band, result: "Band's info edited!" });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected previewBandImg = (req: express.Request, res: express.Response) => {
        try {
            const photoSrc = req.file.location.split('s://s3.ap-southeast-1.amazonaws.com').join(':/')
            res.json({ isSuccess: true, img: photoSrc });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }
}