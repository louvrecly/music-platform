import { knex } from "../app";
import { prioritizedSheets } from "../database-init/readExcel";


// Data Manipulation (DM) - Delete Data
async function truncateTables() {

    for (let i = prioritizedSheets.length - 1; i >= 0; i--) {

        await knex.transaction(async (trx) => {

            const sheet = prioritizedSheets[i];
            // console.log("sheet = ", sheet);

            const hasTable = await trx.schema.hasTable(sheet);
            // console.log("hasTable = ", hasTable);

            if (hasTable) {

                await trx(sheet).truncate();
                console.log(`Table "${sheet}" has been truncated!`);

            } else {
                console.log(`Table "${sheet}" is not found!`);
            }

        });

    }

}


truncateTables();