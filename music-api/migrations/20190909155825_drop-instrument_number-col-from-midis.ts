import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("midis");
    if(hasTable){
        return knex.schema.alterTable("midis",(table)=>{
            table.dropColumn('instrument_number');
        });  
    }else{
        return Promise.resolve();
    }

}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("midis");
    if(hasTable){
        return knex.schema.alterTable("midis",(table)=>{
            table.integer('instrument_number').unsigned();
        });
    }else{
        return Promise.resolve();
    }
    
}