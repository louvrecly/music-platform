import * as express from "express";
import { MidiService } from "../services/MidiService";
import { MidiFile } from "../services/models";
// import { MidiFile, Track } from "../services/models";


export class MidiRouter {

    constructor(protected midiService: MidiService) { }

    public router() {
        const router = express.Router();
        router.get('/player', this.listPlayerTracks)
        router.post('/', this.post);
        return router;
    }

    protected post = async (req: express.Request, res: express.Response) => {
        try {
            const { midiName, exportTracks, band_id } = req.body;
            // console.log({ track });
            const file_path = await this.midiService.writeMidiFile(midiName, exportTracks);
            // const { band_id } = exportTracks[0];
            const name = midiName;
            const newMidiFile: MidiFile = {
                name,
                src: file_path as string,
                band_id,
            };
            await this.midiService.addMidiFile(newMidiFile);
            res.json({ isSuccess: true, data: file_path });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected listPlayerTracks = async (req: express.Request, res: express.Response) => {
        try {
            const midis = await this.midiService.getMidiByPlayerId(req.user.id)
            res.json({ isSuccess: true, data: midis });
        } catch (e){
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString()})
        }
    }

}