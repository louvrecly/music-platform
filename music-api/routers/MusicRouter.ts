import * as express from "express";
import { instrumentRouter } from "../app";
import { trackRouter } from "../app";
import { midiRouter } from "../app";
// import { InstrumentRouter } from "./InstrumentRouter";
// import { TrackRouter } from "./TrackRouter";
// import { MidiRouter } from "./MidiRouter";


export class MusicRouter {

    constructor(
        // private instrumentRouter: InstrumentRouter,
        // private trackRouter: TrackRouter,
        // private midiRouter: MidiRouter,
    ) { }

    public router() {

        const router = express.Router();

        router.use("/instrument", instrumentRouter.router());
        router.use("/track", trackRouter.router());
        router.use("/midi", midiRouter.router());

        return router;
    }

}