window.onload = () => {
    destroyContext();
    pianoKeyScroll.scrollLeft = findKeyLocation(60);
};

let midi, data;

// start talking to MIDI controller
if (navigator.requestMIDIAccess) {
    navigator.requestMIDIAccess({
        sysex: false
    }).then(onMIDISuccess, onMIDIFailure);
} else {
    console.warn("No MIDI support in your browser")
}

function onMIDISuccess(midiData) {
    // this is all our MIDI data
    midi = midiData;
    var inputs = midi.inputs;
    var outputs = midi.outputs;
    // var allInputs = midi.inputs.values();
    var allInputs = inputs.values();
    // loop over all available inputs and listen for any MIDI input
    for (var input = allInputs.next(); input && !input.done; input = allInputs.next()) {
        // when a MIDI value is received call the onMIDIMessage function
        input.value.onmidimessage = gotMIDImessage;
    }
}

function gotMIDImessage(messageData) {

    const command = messageData.data[0];
    const midiNote = messageData.data[1];
    const velocity = messageData.data.length > 2 ? messageData.data[2] : 0;

    addNoteToMidiList(command, midiNote, velocity);

    switch (command) {
        case 144: // noteOn
            if (velocity > 0) {
                // noteOn(midiNote, velocity);
                noteOn(midiNote);
            } else {
                noteOff(midiNote);
            }
            break;
        case 128: // noteOff
            noteOff(midiNote);
            break;
        default:
            console.log(`Something went terribly wrong! \nCommand value is neither 144 nor 128: ${command}`);
    }
}

// on failure
function onMIDIFailure() {
    console.warn("Not recognising MIDI controller")
}

// initialize context and oscillator type
let context;
// let oscillator;
// let contexts = [];
let oscillators = {};
// let oscillators = [];
let type = "sine";

// initialize start indicator
let isStarted = false;

// specify length and eps
const length = 2;
const eps = 0.01;

// initialize context
function getOrCreateContext() {
// function getOrCreateContext() {
    // if (!context) {
    //     context = new AudioContext();
    //     oscillator = context.createOscillator();
    //     oscillator.connect(context.destination);
    //     oscillator.type = type;
    // }
    // return context;
    if (!context) {
        context = new AudioContext();
        // const context = new AudioContext();
    }
    // getOrCreateOscillator(context, midiNote);
    // const oscillator = context.createOscillator();
    // oscillator.connect(context.destination);
    // oscillator.type = type;
    // oscillators[midiNote] = oscillator;

    return context;
}

// create oscillator
function getOrCreateOscillator(context, midiNote = -1) {
    const freq = midiNoteToFrequency(midiNote);
    if (!(midiNote >= 0 && midiNote < 128)) {
        midiNote = "o";
    }
    if (!oscillators[midiNote]) {
        const oscillator = context.createOscillator();
        const amplifier = context.createGain();
        // oscillator.connect(context.destination);
        amplifier.gain.value = 1;
        amplifier.connect(context.destination);
        oscillator.connect(amplifier);
        oscillator.type = type;
        oscillator.frequency.value = freq;
        oscillators[midiNote] = oscillator;
    }
    console.log({ oscillators });
    return oscillators;
}

// destroy context
function destroyContext() {
    context = null;
    // oscillator = null;
    oscillators = {};
    return context;
}

// destroy oscillator
function destroyOscillator(context, midiNote = -1) {
    if (!(midiNote >= 0 && midiNote < 128)) {
        midiNote = "o";
    }
    if (oscillators[midiNote]) {
        delete oscillators[midiNote];
    }
    console.log({ oscillators });
    return oscillators;
}

// translate midi note to frequency
function midiNoteToFrequency(midiNote) {
    let frequency;
    if (midiNote >= 0 && midiNote <128) {
        frequency = (2 ** ((midiNote - 69) / 12)) * 440;
    } else {
        frequency = 0;
    }
    return frequency;
}

// play sound with Web Audio API using frequency
function playSound(frequency, time) {
    oscillator.frequency.setTargetAtTime(frequency, time, 0.001);
}

// play sound with Web Audio API using midiNote
function playMidiNote(midiNote, time) {
    // const freq = (midiNote >= 0 && midiNote < 128) ? midiNoteToFrequency(midiNote) : 0;
    const freq = midiNoteToFrequency(midiNote);
    midiNote = (midiNote >= 0 && midiNote < 128) ? midiNote : "o";
    // const freq = midiNote >= 0 ? midiNoteToFrequency(midiNote) : 0;
    // const freq = midiNoteToFrequency(midiNote);
    console.log(`oscillators[${midiNote}] = ${oscillators[midiNote]}`);
    oscillators[midiNote].frequency.setTargetAtTime(freq, time, 0.001);
    
    // oscillators[midiNote].start(time);
    // oscillators[midiNote].stop(time);
}

// specify noteOn function
function noteOn(midiNote) {
    // getOrCreateContext();
    context = getOrCreateContext();
    // getOrCreateOscillator(context, midiNote);
    
    // // const freq = midiNoteToFrequency(midiNote);
    // const time = context.currentTime;
    // // console.log({ midiNote, freq, time });
    // console.log({ midiNote, time });
    // // playSound(freq, time);
    // playMidiNote(midiNote, time);
    // // if (!isStarted) {
    if (!oscillators[midiNote]) {
        getOrCreateOscillator(context, midiNote);
        
        const time = context.currentTime;
        console.log({ midiNote, time });
        playMidiNote(midiNote, time);
        
        if (!(midiNote >= 0 && midiNote < 128)) {
            midiNote = "o";
        }
        // oscillator.start(0);
        oscillators[midiNote].start(0);
        console.log(`oscillators[${midiNote}] is started!`);
        // isStarted = true;
    } else {
        context.resume();
    }
}

// specify noteOff function
function noteOff(midiNote) {
    if (oscillators[midiNote]) {
        oscillators[midiNote].stop(0);
        console.log(`oscillators[${midiNote}] is stopped!`);
    }
    // // stopSound();
    destroyOscillator(context, midiNote);
    // // destroyContext();
    // isStarted = false;
}

// specify stop sound function
function stopSound() {
    if (context) {
        context.suspend();
    }
}

let currentKeyDown = null;

// add note to the midi list
function addNoteToMidiList(command, midiNote, velocity) {
    const timestamp = performance.now();
    const newItem = document.createElement('li');
    const noteData = [command, midiNote, velocity];
    newItem.appendChild(document.createTextNode([...noteData, timestamp]));
    console.log(newItem);
    // console.log("performance.now() = ", performance.now());
    dataList.appendChild(newItem);
    midiListScroll.scrollTop = midiListScroll.scrollHeight;
}

// smooth scroll on piano keys
function smoothScrollLeft(scrollbar, pixels, direction) {
    let scrolled = 0;
    const step = pixels / 4;
    let scrollTimer = setInterval(() => {
        switch (direction) {
            case "left":
                scrollbar.scrollLeft -= step;
                break;
            case "right":
                scrollbar.scrollLeft += step;
                break;
            default:
                console.log(`Invalid direction: ${direction}\nDirection can only be "left" or "right"!`);
        }
        scrolled += step;
        if (scrolled >= pixels) {
            window.clearInterval(scrollTimer);
        }
    }, 15);
}

// // keydown listener
// document.addEventListener("keydown", (event) => {
//     console.log(event);
//     if (key in emulatedKeys && !(event.ctrlKey)) {
//         // console.log({currentKeyDown});
//         // console.log(`keydown = ${event.key}`);
//         if (currentKeyDown != event.key) {
//             if (currentKeyDown) {
//                 const currentMidiNote = emulatedKeys[currentKeyDown];
//                 const currentKeyId = `midiNote-${currentMidiNote}`;
//                 const currentPianoKey = document.querySelector(`#${currentKeyId}`);
//                 currentPianoKey.classList.remove("active");
//             }
//             currentKeyDown = event.key;
//             const midiNote = emulatedKeys[currentKeyDown];
//             const keyId = `midiNote-${midiNote}`;
//             const pianoKey = document.querySelector(`#${keyId}`);
//             pianoKey.classList.add("active");
//             console.log({ midiNote, currentKeyDown, keyId, pianoKey });
//             noteOn(midiNote);

//             addNoteToMidiList(144, midiNote, 127);
//         }
//     } else if (event.key == 'ArrowLeft' || event.key == 'ArrowRight') {
//         const direction = event.key.replace('Arrow', "")[0].toLowerCase() + event.key.replace('Arrow', "").slice(1);
//         console.log({ direction });
//         if (event.ctrlKey) {
//             smoothScrollLeft(pianoKeyScroll, 336, direction);
//         } else {
//             smoothScrollLeft(pianoKeyScroll, 48, direction);
//         }
//     }
// });

// // keyup listener
// document.addEventListener("keyup", (event) => {
//     console.log(event);
//     if (event.key in emulatedKeys) {
//         // console.log({currentKeyDown});
//         // console.log(`keyup = ${event.key}`);
//         if (currentKeyDown == event.key) {
//             const midiNote = emulatedKeys[event.key];
//             const keyId = `midiNote-${midiNote}`;
//             const pianoKey = document.querySelector(`#${keyId}`);
//             pianoKey.classList.remove("active");
//             console.log({ midiNote, currentKeyDown, keyId, pianoKey });
//             noteOff(midiNote);
//             currentKeyDown = null;

//             addNoteToMidiList(128, midiNote, 0);
//         }
//     }
// });

// keydown listener
document.addEventListener("keydown", (event) => {
    // console.log(event);
    if (event.key in emulatedKeys && !(event.ctrlKey)) {
        socket.emit('keydown-from-others', event.key,event.ctrlKey)
        // console.log({currentKeyDown});
        // console.log(`keydown = ${event.key}`);
        // if (currentKeyDown) {
        //     const currentMidiNote = emulatedKeys[currentKeyDown];
        //     const currentKeyId = `midiNote-${currentMidiNote}`;
        //     const currentPianoKey = document.querySelector(`#${currentKeyId}`);
        //     currentPianoKey.classList.remove("active");
        // }

        currentKeyDown = event.key;
        const midiNote = emulatedKeys[currentKeyDown];
        if (!oscillators[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKey.classList.add("active");
            console.log({ midiNote, currentKeyDown, keyId, pianoKey });
            noteOn(midiNote);
    
            addNoteToMidiList(144, midiNote, 127);
            console.log({ oscillators });
        }

        // if (currentKeyDown != event.key) {
        //     currentKeyDown = event.key;
        //     const midiNote = emulatedKeys[currentKeyDown];
        //     const keyId = `midiNote-${midiNote}`;
        //     const pianoKey = document.querySelector(`#${keyId}`);
        //     pianoKey.classList.add("active");
        //     console.log({ midiNote, currentKeyDown, keyId, pianoKey });
        //     noteOn(midiNote);
    
        //     addNoteToMidiList(144, midiNote, 127);
        //     console.log({ oscillators });
        // }

    } else if (event.key.startsWith('Arrow')) {
    // } else if (event.key == 'ArrowLeft' || event.key == 'ArrowRight') {
        const direction = event.key.replace('Arrow', "")[0].toLowerCase() + event.key.replace('Arrow', "").slice(1);
        console.log({ direction });
        if (event.ctrlKey) {
            smoothScrollLeft(pianoKeyScroll, 336, direction);
        } else {
            smoothScrollLeft(pianoKeyScroll, 48, direction);
        }
    }
});

// keyup listener
socket.on('keydown-from-others', function (key,ctrlKey) {
    // console.log(event);
    if (key in emulatedKeys && !(ctrlKey)) {
        socket.emit('keydown-from-others', key)
        // console.log({currentKeyDown});
        // console.log(`keydown = ${key}`);
        // if (currentKeyDown) {
        //     const currentMidiNote = emulatedKeys[currentKeyDown];
        //     const currentKeyId = `midiNote-${currentMidiNote}`;
        //     const currentPianoKey = document.querySelector(`#${currentKeyId}`);
        //     currentPianoKey.classList.remove("active");
        // }

        currentKeyDown = key;
        const midiNote = emulatedKeys[currentKeyDown];
        if (!oscillators[midiNote]) {
            const keyId = `midiNote-${midiNote}`;
            const pianoKey = document.querySelector(`#${keyId}`);
            pianoKey.classList.add("active");
            console.log({ midiNote, currentKeyDown, keyId, pianoKey });
            noteOn(midiNote);
    
            addNoteToMidiList(144, midiNote, 127);
            console.log({ oscillators });
        }

        // if (currentKeyDown != key) {
        //     currentKeyDown = key;
        //     const midiNote = emulatedKeys[currentKeyDown];
        //     const keyId = `midiNote-${midiNote}`;
        //     const pianoKey = document.querySelector(`#${keyId}`);
        //     pianoKey.classList.add("active");
        //     console.log({ midiNote, currentKeyDown, keyId, pianoKey });
        //     noteOn(midiNote);
    
        //     addNoteToMidiList(144, midiNote, 127);
        //     console.log({ oscillators });
        // }

    } else if (key.startsWith('Arrow')) {
    // } else if (key == 'ArrowLeft' || key == 'ArrowRight') {
        const direction = key.replace('Arrow', "")[0].toLowerCase() + key.replace('Arrow', "").slice(1);
        console.log({ direction });
        if (ctrlKey) {
            smoothScrollLeft(pianoKeyScroll, 336, direction);
        } else {
            smoothScrollLeft(pianoKeyScroll, 48, direction);
        }
    }
});

// play midi array
function playMidiArray(midiArray, length, eps) {
    console.log({ midiArray });
    if (midiArray.length > 0) {
        // destroyContext();
        getOrCreateContext();
        getOrCreateOscillator(context);

        let midiNoteSet = new Set();
        midiArray.forEach(note => midiNoteSet.add(note[0]));
        console.log({ midiNoteSet });

        midiNoteSet.forEach(noteNum => {
            getOrCreateOscillator(context, noteNum);
            if (!(noteNum >= 0 && noteNum < 128)) {
                noteNum = "o";
            }
            console.log(`oscillators[${noteNum}] = ${oscillators[noteNum]}`)
            // oscillators[noteNum].start(0);
        });

        // // oscillator.start(0);
        oscillators["o"].start(0);

        
        // let time = 0;
        // midiArray.forEach(note => {
        //     // const freq = (note[0] >= 0 && note[0] < 128) ? midiNoteToFrequency(note[0]) : 0;
        //     // console.log({ time, freq });
        //     // playSound(0, time - eps);
        //     // playSound(freq, time);
        //     const midiNote = note[0];
        //     const noteFraction = note[1];
        //     setTimeout(() => {
        //         // console.log({ midiNote, time })
        //         noteOn(midiNote);
        //         // elapsedTime += (eps + (length / noteFraction)) * 1000;
        //         time += Math.round((length / noteFraction) * 100) / 100;
        //         setTimeout(() => {
        //             noteOff(midiNote);
        //         }, (time) * 1000);
        //     }, (time + eps) * 1000);
        // });
        
        
        

        
        
        
        let time = context.currentTime + eps;
        midiArray.forEach(note => {

            const midiNote = note[0];
            const noteFraction = note[1];
            playMidiNote(-1, time - eps);

            // oscillators["o"].stop(time);
            
            playMidiNote(midiNote, time);
            console.log({ time, midiNote });
            time += Math.round((length / noteFraction) * 100) / 100;
            // oscillators["o"].start(time);
            // oscillators[midiNote].stop(time - eps);
            // oscillators[midiNote].start(time);
        });
        destroyOscillator(context);
        destroyContext();
    }
};

// play tetris midi
function playTetrisMidi() {
    playMidiArray(tetris, length, eps);
};

// play midi list
function playMidiList() {
    const midiArray = convertToMidiArray(length);
    playMidiArray(midiArray, length, eps);
}

// clear midi list
function clearMidiList() {
    dataList.innerHTML = "";
}

// convert midi list to midi array
function convertToMidiArray(length) {
    let listItems = [...dataList.children];
    // console.log({listItems});
    const noteItems = listItems.map(listItem => {
        return listItem.textContent.split(",").map((val, index) => {
            if (index < 3) {
                return parseInt(val, 10);
            } else {
                return parseFloat(val);
            }
        });
    });
    console.log({ noteItems });
    const midiArray = noteItems
        .map((noteItem, index) => {
            const command = noteItem[0];
            const midiNote = noteItem[1];
            const velocity = noteItem.length > 2 ? noteItem[2] : 0;
            const timestamp = noteItem[noteItem.length - 1];
            const nextTimeStamp = noteItems[index + 1] ? noteItems[index + 1][noteItems[index + 1].length - 1] : (timestamp + length * 100);
            const elapsedTime = (nextTimeStamp - timestamp) / 1000;
            // const elapsedTime = timestamp - previousTimeStamp;
            const noteFraction = length / elapsedTime;
            console.log({ timestamp, nextTimeStamp });
            previousTimeStamp = timestamp;
            if (command == 144 && velocity != 0) {
                return [midiNote, noteFraction];
            } else if (command == 128 || velocity == 0) {
                return [-1, noteFraction];
            } else {
                console.log(`Something went terribly wrong! \nCommand value is neither 144 nor 128: ${command}`);
                return;
            }
        });
    // console.log({ midiArray });
    return midiArray;
}




// DOM selection
const midiListScroll = document.querySelector(".midiListScroll");
const dataList = document.querySelector('#midiList')

const playButton = document.querySelector("#playButton");
const clearButton = document.querySelector("#clearButton");
// const stopButton = document.querySelector("#stopButton");

const playTetris = document.querySelector("#playTetris");
const typeButtons = document.querySelectorAll(".typeButton");

const pianoKeyScroll = document.querySelector(".pianoKeyScroll");
const pianoKeys = document.querySelectorAll(".pianoKey");

// add event listener
playButton.addEventListener("click", playMidiList);
clearButton.addEventListener("click", clearMidiList);
// stopButton.addEventListener("click", stopSound);

playTetris.addEventListener("click", playTetrisMidi);

for (let typeButton of typeButtons) {
    typeButton.addEventListener("change", (event) => {
        // console.log("event.currentTarget = ", event.currentTarget);
        // console.log("event.currentTarget.value = ", event.currentTarget.value);
        // console.log("event.currentTarget.checked = ", event.currentTarget.checked);
        if (event.currentTarget.checked) {
            type = event.currentTarget.value;
            if (oscillator) {
                oscillator.type = type;
            }
            console.log({ type });
        }
    });
}

for (let pianoKey of pianoKeys) {
    pianoKey.addEventListener("mousedown", (event) => {
        const keyId = event.currentTarget.id;
        const midiNote = parseInt(keyId.split("-")[1], 10);
        console.log({ keyId, midiNote });
        event.currentTarget.classList.add("active");
        noteOn(midiNote);

        addNoteToMidiList(144, midiNote, 127);
    });
    pianoKey.addEventListener("mouseup", (event) => {
        const keyId = event.currentTarget.id;
        const midiNote = parseInt(keyId.split("-")[1], 10);
        console.log({ keyId, midiNote });
        event.currentTarget.classList.remove("active");
        noteOff(midiNote);

        addNoteToMidiList(128, midiNote, 0);
    });
}

function findKeyLocation(midiNote) {
    const keyLocation = midiNote * 28;
    return keyLocation;
}

// keyboard emulation
const emulatedKeys = {
    "z": 60,
    "s": 61,
    "x": 62,
    "d": 63,
    "c": 64,
    "v": 65,
    "g": 66,
    "b": 67,
    "h": 68,
    "n": 69,
    "j": 70,
    "m": 71,
    ",": 72,
    "l": 73,
    ".": 74,
    ";": 75,
    "/": 76,
    "q": 72,
    "2": 73,
    "w": 74,
    "3": 75,
    "e": 76,
    "r": 77,
    "5": 78,
    "t": 79,
    "6": 80,
    "y": 81,
    "7": 82,
    "u": 83,
    "i": 84,
    "9": 85,
    "o": 86,
    "0": 87,
    "p": 88,
    "[": 89,
    "=": 90,
    "]": 91
}

// This is a tetris theme transposed from https://musescore.com/user/16693/scores/38133
const tetris = [
    [76, 4],
    [71, 8],
    [72, 8],
    [74, 4],
    [72, 8],
    [71, 8],
    [69, 4],
    [69, 8],
    [72, 8],
    [76, 4],
    [74, 8],
    [72, 8],
    [71, 4],
    [71, 8],
    [72, 8],
    [74, 4],
    [76, 4],
    [72, 4],
    [69, 4],
    [69, 4],
    [0, 4],
    [74, 3],
    [77, 8],
    [81, 4],
    [79, 8],
    [77, 8],
    [76, 3],
    [72, 8],
    [76, 4],
    [74, 8],
    [72, 8],
    [71, 4],
    [71, 8],
    [72, 8],
    [74, 4],
    [76, 4],
    [72, 4],
    [69, 4],
    [69, 4],
    [0, 4]
]


// midiData
// [command, midiNote, velocity]
  // command - command value
    // 144 - “note on” event
    // 128 - “note off” event
  // midiNote - note value
    // range: 0–127 (lowest to highest)
      // For example: 88-key piano
        // lowest note: 21
        // highest note: 108
        // “middle C”: 60
  // velocity - velocity value
    // range: 0–127 (softest to loudest)
    // softest possible “note on” velocity = 1
    // A velocity of 0 is sometimes used in conjunction with a command value of 144 (which typically represents “note on”) to indicate a “note off” message, so it’s helpful to check if the given velocity is 0 as an alternate way of interpreting a “note off” message

// Tone.js
// https://tonejs.github.io/?source=post_page---------------------------

// Playing with MIDI in JavaScript
// https://medium.com/swinginc/playing-with-midi-in-javascript-b6999f2913c3

// Audio API + MIDI API example
// https://codepen.io/kulak-at/pen/rdxXwM

// javascript oscillator type
// https://developer.mozilla.org/en-US/docs/Web/API/OscillatorNode/type

// web audio font
// https://surikov.github.io/webaudiofont/examples/midiplayer.html?source=post_page---------------------------#
// https://surikov.github.io/webaudiofont/

// WebAudio API - Monophonic Synthesis
// http://blog.chrislowis.co.uk/2013/06/05/playing-notes-web-audio-api.html

// WebAudio API - Polyphonic Synthesis
// http://blog.chrislowis.co.uk/2013/06/10/playing-multiple-notes-web-audio-api.html

// test codes
