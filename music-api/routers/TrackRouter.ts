import * as express from "express";
import { Track } from "../services/models";
import { TrackService } from "../services/TrackService";


export class TrackRouter {

    constructor(protected trackService: TrackService) { }

    public router() {

        const router = express.Router();

        router.get('/player', this.listPlayerTracks)
        router.get('/list/:band_id', this.list);
        router.get('/:id', this.get);
        router.post('/', this.post);
        router.put('/:id', this.put);
        router.delete('/:id', this.delete);

        return router;
    }

    protected list = async (req: express.Request, res: express.Response) => {
        try {
            const band_id = parseInt(req.params.band_id, 10);
            let tracks = await this.trackService.getTracks(band_id);
            res.json({ isSuccess: true, data: tracks });
        } catch (e) {
            console.log(e.toString());
            res.status(400).json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected get = async (req: express.Request, res: express.Response) => {
        try {
            const id = parseInt(req.params.id, 10);
            let [track] = await this.trackService.getTrackById(id);
            res.json({ isSuccess: true, data: track });
        } catch (e) {
            console.log(e.toString());
            res.status(400).json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected post = async (req: express.Request, res: express.Response) => {
        try {
            let { newTrack } = req.body;
            const midi_array = JSON.stringify(newTrack.midi_array);
            newTrack.midi_array = midi_array;
            const [newTrackId] = await this.trackService.addTrack(newTrack as Track);
            res.json({ isSuccess: true, data: newTrackId });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected put = async (req: express.Request, res: express.Response) => {
        try {
            const id = parseInt(req.params.id, 10);
            let { editedTrack } = req.body;
            const midi_array = JSON.stringify(editedTrack.midi_array);
            editedTrack.midi_array = midi_array;
            const [editedTrackId] = await this.trackService.editTrack(id, editedTrack as Track);
            res.json({ isSuccess: true, data: editedTrackId });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected delete = async (req: express.Request, res: express.Response) => {
        try {
            const id = parseInt(req.params.id, 10);
            const [deletedTrackId] = await this.trackService.deleteTrack(id);
            res.json({ isSuccess: true, data: deletedTrackId });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }
    
    protected listPlayerTracks = async (req: express.Request, res: express.Response) => {
        try {
            const tracks = await this.trackService.getTrackByPlayerId(req.user.id)
            res.json({ isSuccess: true, data: tracks });
        } catch (e){
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString()})
        }
    }
}