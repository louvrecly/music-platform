import * as express from "express";
// import { userService } from "../app";
import { UserService } from "../services/UserService";
// import { UserService, User } from "../services/UserService";

export class UserRouter {
    
    constructor(private userService: UserService) {}

    public router() {
        const router = express.Router();

        router.get('/', this.list);
        router.get('/:id', this.get);
        router.get('/currentUser', this.currentUser);
        // router.get("/loginStatus", this.getLoginStatus);
        // router.post("/register", this.register);
        // router.get("/details", this.getUser);
        
        return router;
    }

    list = async (req: express.Request, res: express.Response) => {
        try {
            const users = await this.userService.getUsers();
            res.json({ isSuccess: true, data: users });
        } catch (e) {
            res.status(500).json({ isSuccess: false, msg: e.toString() });
        }
    }

    get = async (req: express.Request, res: express.Response) => {
        try {
            if ('username' in req.query) {
                const username: string = req.query.username;
                const [user] = await this.userService.getUserByUsername(username);
                res.json({ isSuccess: true, data: user });
            } else {
                const [user] = await this.userService.getUser(req.params.id as number);
                res.json({ isSuccess: true, data: user });
            }
        } catch (e) {
            res.status(500).json({ isSuccess: false, msg: e.toString() });
        }
    }

    currentUser = (req: express.Request, res: express.Response) => {
        res.json(req.user);
    }

    // protected getUser = (req: express.Request, res: express.Response) => {
    //     res.json(req.user);
    // }
    
    // protected getLoginStatus = (req: express.Request, res: express.Response) => {
    //     console.log("req.user = ", req.user);
    //     const loginStatus = req.user != undefined;
    //     res.json(loginStatus);
    // }

    // protected register = async (req: express.Request, res: express.Response) => {
    //     console.log("req.body.username = ", req.body.username);
    //     console.log("req.body.password = ", req.body.password);
    //     const user = await this.userService.findUserByUsername(req.body.username);
    //     if (user) {
    //         console.log("Username already taken!");
    //         res.json({approval: false, message: "Username already taken!"});
    //     } else {
    //         const newUser: User = {
    //             username: req.body.username, 
    //             password: req.body.password
    //         };
    //         await this.userService.addUser(newUser);
    //         console.log("account registered");
    //         res.json({approval: true, message: "Account registered! Please log in!"});
    //     }
    // }

    // protected logout = (req: express.Request, res: express.Response) => {
    //     if (req.session) {
    //         req.session.authenticated = false;
    //         console.log("req.session.authenticated = ", req.session.authenticated);
    //         res.json({loggedIn: false});
    //     } else {
    //         res.json({loggedIn: false});
    //     }
    // }

}