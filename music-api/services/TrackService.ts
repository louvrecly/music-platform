import * as Knex from "knex";
import { Track } from "./models";


export class TrackService {

    constructor(private knex: Knex) { }

    public async getTracks(band_id: number) {
        return await this.knex.select('*').from('tracks').where('band_id', band_id).orderBy('id', 'asc');
    }

    public async getTrackById(id: number) {
        return await this.knex.select('*').from('tracks').where('id', id).limit(1);
    }

    public async getTrackByPlayerId(playerId: number) {
        return await this.knex.select('band_members.band_id as band_id','bands.name as band_name','tracks.id as track_id','tracks.name','tracks.is_mono','tracks.midi_array','tracks.instrument_id','tracks.is_percussion')
        .from('band_members').where('member_id', playerId)
        .innerJoin('tracks', 'tracks.band_id', '=', 'band_members.band_id')
        .innerJoin('bands', 'band_members.band_id', '=', 'bands.id' )

    }


    public async addTrack(newTrack: Track) {
        return await this.knex.insert(newTrack).into('tracks').returning('id');
    }

    public async editTrack(id: number, editedTrack: Track) {
        const [track] = await this.knex.select('*').from('tracks').where('id', id).limit(1);
        // console.log({ track, editedTrack });
        if (track) {
            return await this.knex('tracks').update(editedTrack).where('id', id).returning('id');
        } else {
            console.log(`Invalid track id: ${id}`);
            throw new Error(`Invalid track id: ${id}`);
        }
    }

    public async deleteTrack(id: number) {
        const [track] = await this.knex.select('*').from('tracks').where('id', id).limit(1);
        // console.log({ track });
        if (track) {
            return await this.knex('tracks').where('id', id).del().returning('id');
        } else {
            console.log(`Invalid track id: ${id}`);
            throw new Error(`Invalid track id: ${id}`);
        }
    }

}