import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("midis");
    if(hasTable){
        return knex.schema.alterTable("midis",(table)=>{
            table.dropColumn('track_id');
            table.integer('band_id').unsigned();
            table.foreign('band_id').references('bands.id');
            table.integer('instrument_number').unsigned();
        });  
    }else{
        return Promise.resolve();
    }

}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("midis");
    if(hasTable){
        return knex.schema.alterTable("midis",(table)=>{
            table.integer('track_id').unsigned();
            table.foreign('track_id').references('tracks.id');
            table.dropColumn('band_id');
            table.dropColumn('instrument_number');
        });
    }else{
        return Promise.resolve();
    }
    
}