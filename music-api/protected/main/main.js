var socket = io.connect();

window.onload = async () => {
    player = await loadPlayer();
    console.log({ player });
    myPlayerId = player.id;
    console.log({ myPlayerId });
    myBand = await getMyBand()
    console.log({ myBand });
    getMyBandMember(myBand.id)
    loadAdResponse(myBand.id)
    loadJoinBands(myPlayerId)
    loadBandRoom(myPlayerId)
    // GetMyUserId();
}

let player;

async function loadPlayer(overwrite = false) {

    console.log("overwrite === true: ", overwrite === true);
    if (overwrite === true) {

        // do not check if player already exists in local storage 
        const res = await fetch("/protected/player/info", { method: "GET" });
        const playerData = await res.json();
        console.log("playerData = ", playerData);
        localStorage.setItem("player", JSON.stringify(playerData));

    } else {

        // check if player already exists in local storage first 
        // console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
        // console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);

        if (localStorage.getItem("player") == null) {
            const res = await fetch("/protected/player/info", { method: "GET" });
            const playerData = await res.json();
            // console.log("playerData = ", playerData);
            localStorage.setItem("player", JSON.stringify(playerData));
        }

    }

    // console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
    // console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

    return JSON.parse(localStorage.getItem("player"));

}
let myPlayerId;

async function getMyBand(){
    const res =  await fetch("/protected/bands/ownBand", { method: "GET" })
  
    const band = await res.json();

    localStorage.setItem("myBand", JSON.stringify(band));
    return JSON.parse(localStorage.getItem("myBand"));
  }

  async function getMyBandMember(bandId){
    const res = await fetch(`/protected/bands/bandMembers/${bandId}`);
    const members = await res.json();

    let html = '';
    for (let member of members){
        let bandId = member.band
        let name = member.name
        let memberId = member.member

      html += `
                <div class="row memberInformation" id="data-${bandId}">
                <div class="tidy memberName">${name}</div>
                <div class="tidy"><div class="resButton" id="${memberId}" onclick='kick("${memberId}")'>Kick</div></div>
                 </div>
                `
            const showAd = document.querySelector('#memberTable');
            showAd.innerHTML = html;
            ;
    }
  }

  async function loadAdResponse(bandId){
    const res =  await fetch(`/protected/bandsAds/loadAdResponse/${bandId}`, { method: "GET" })
  
    const responses = await res.json();

    let html = '';
    for (let response of responses){
      let name = response.name
      let msg = response.message
      let status = response.status
      let playerId = response.id
      
      if (status == "requesting"){
        html += `
        <div class="row resInformation">
        <div class="tidy applicantName">${name}</div>
        <div class="tidy msg">${msg}</div>
        <div class="tidy showButton">
        <div class="resButton" onclick='approve("${playerId}")'>approve</div>
        <div class="resButton" onclick='decline("${playerId}")'>decline</div>       
         </div>

        `
    const showResponse = document.querySelector('#resTable');
    showResponse.innerHTML = html;
    ;
      }
    }
}

async function loadJoinBands(id){
    const res =  await fetch(`/protected/bands/loadJoinedBands/${id}`, { method: "GET" })
  
    const bands = await res.json();

    let html = '';
    for (let band of bands){
      let name = band.name
      let bandId = band.band

        html += `
        <div class="row bandInformation">
        <div class="tidy bandName">${name}</div>
        <div class="resButton tidy" onclick='quit("${bandId}")'>Quit</div>     
         </div>
        `
    const showJoinedBands = document.querySelector('#bandsTable');
    showJoinedBands .innerHTML = html;
    ;
      
    }
}

async function loadBandRoom(id){
    const res =  await fetch(`/protected/bands/loadJoinedBands/${id}`, { method: "GET" })
  
    const bands = await res.json();
    getOnlineNumbers(myBand.id)
  
    let html = `
                <div class="row bandRoom">
                <div class="tidy bandName">${myBand.name}</div>
                <div class="tidy online" id="room${myBand.id}"></div>
                <div class="resButton tidy" onclick='joinRoom("${myBand.id}")'>Enter</div>
                </div>
        `
         for (let band of bands){
         let name = band.name
         let bandId = band.band

        html += `
        <div class="row bandRoom">
        <div class="tidy bandName">${name}</div>
        <div class="tidy online" id="room${bandId}"></div>
        <div class="resButton tidy" onclick='joinRoom("${bandId}")'>Enter</div>     
         </div>
        `
    const bandRoom = document.querySelector('#bandRoomsTable');
        bandRoom.innerHTML = html;
    ;
    getOnlineNumbers(bandId)
    }
}

function getOnlineNumbers(room){
    socket.emit('getOnline', room);
}

socket.on('created', function (room,id, numClients) {
    console.log(id)
    getOnlineNumbers(room)
    // getOnlineNumbers(room)
    // console.log(numClients)
    // const roomNumber = document.querySelector(`#room${room}`)
    // roomNumber.innerHTML = numClients + ' '+ 'member(s)'
});


socket.on('joined', function (room,id ,numClients) {
    console.log(id)
    getOnlineNumbers(room)
    // getOnlineNumbers(room)
    // console.log(numClients)
    // const roomNumber = document.querySelector(`#room${room}`)
    // roomNumber.innerHTML = numClients + ' '+ 'member(s)'
});


socket.on('onlineClients', function (room,numClients) {
    // getOnlineNumbers(room)
    console.log(numClients)
    const roomNumber = document.querySelector(`#room${room}`)
    roomNumber.innerHTML = numClients + ' '+ 'member(s)'
});

async function kick(member, bandId){
    
        await fetch("/protected/bandsAds/createBandAds", { method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({
            name: createAdsForm.adName.value,
            description: createAdsForm.adDesc.value,
            message: createAdsForm.adMsg.value,
            band_id: myBand.id,
            instruments:instruments
            
        })
    });
}