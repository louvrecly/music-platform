import * as express from "express";
import * as multer from "multer";
// import * as path from "path";
import { PlayerService } from "../services/PlayerService";
import { Player } from "../services/models";


export class PlayerRouter {

    // constructor(protected playerService: PlayerService) { }
    constructor(protected playerService: PlayerService, private upload: multer.Instance) { }

    public router() {

        const router = express.Router();

        // router.get("/sports", this.loadSports);
        // router.get("/players",this.loadPlayers);
        // router.get("/playersSocial",this.loadPlayersSocial);
        router.get("/playerProfiles", this.loadPlayerProfiles);
        router.get("/profile", this.viewProfile);
        router.get("/profile/:id", this.getPublicProfile);
        router.post("/profile", this.addPlayer);
        router.get("/info", this.getPlayer);
        router.put("/edit", this.editPlayer);
        router.post("/uploadProfilePic", this.upload.single('profile'), this.updateProfileImg);
        // // router.post("/uploadProfilePic", upload.single('profile'), this.updateProfileImg);
        // router.put("/updatePlayerInfo", this.updatePlayerInfo);
        // // router.put("/updateTimeLocation", this.updatePlayerTimeLocation);
        // // router.put("/updateProficiency", this.updatePlayerProficiencyAfterMatch);
        // // router.post("/joinSport", this.playerAddSport);

        return router;
    }

    protected loadPlayerProfiles = async (req: express.Request, res: express.Response) => {
        try {
            const playerProfiles = await this.playerService.loadPlayerProfiles();
            res.json(playerProfiles);
        } catch (e) {
            console.log(e.toString());
            res.json({ result: e.toString() });
        }
    }

    protected viewProfile = (req: express.Request, res: express.Response) => {
        res.redirect("/protected/profile");
    }

    protected getPublicProfile = async (req: express.Request, res: express.Response) => {
        try {
            const publicProfile = await this.playerService.getPublicProfile(req.params.id);
            res.json(publicProfile);
        } catch (e) {
            console.log(e.toString());
            res.json({ result: e.toString() });
        }
    }

    protected getPlayer = async (req: express.Request, res: express.Response) => {
        try {
            // console.log("req.user = ", req.user);
            const [player] = await this.playerService.getPlayer(req.user.id as number);
            if (player) {
                res.json({ isSuccess: true, data: player });
            } else {
                res.json({ isSuccess: false, msg: "player not found" });
            }
        } catch (e) {
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected addPlayer = async (req: express.Request, res: express.Response) => {
        try {
            const gender = req.body.player.gender;
            let profile_img;
            if (gender == 'M') {
                profile_img = "/empty-profile-M_edited.jpg";
            } else if (gender == 'F') {
                profile_img = "/empty-profile-F_edited.jpg";
            } else {
                console.log({ gender });
                profile_img = "";
            }
            const newPlayer: Player = {
                id: req.user.id,
                ...req.body.player,
                profile_img: profile_img,
                user_id: req.user.id
            }
            const instrumentIds = req.body.instruments.map((id: string) => parseInt(id, 10));
            console.log({ instrumentIds });
            console.log("req.user.id = ", req.user.id);
            console.log({ newPlayer });
            const newPlayerId = await this.playerService.addPlayer(newPlayer, instrumentIds);
            console.log({ newPlayerId });
            res.json({ id: newPlayerId, result: "Player's profile created!" });
        } catch (e) {
            console.log(e.toString());
            res.json({ result: e.toString() });
        }
    }

    protected editPlayer = async (req: express.Request, res: express.Response) => {
        try {
            const editedPlayer = { ...req.body.editedPlayer, id: req.user.id };
            // console.log({ editedPlayer });
            const [editedPlayerId] = await this.playerService.editPlayer(editedPlayer);
            res.json({ isSuccess: true, data: editedPlayerId });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected updateProfileImg = (req: express.Request, res: express.Response) => {
        try {
            this.playerService.updateProfileImg(req.user.id, req.file.location.split('s://s3.ap-southeast-1.amazonaws.com').join(':/'));
            // this.playerService.updateProfileImg(req.user.id, req.file.location);
            // // this.playerService.updateProfileImg(req.user.id, path.join("../protected/uploads/", req.file.filename));
            res.json({ isSuccess: true, data: req.file.location });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    // protected updatePlayerInfo = async (req: express.Request, res: express.Response) => {
    //     try {
    //         const editedPlayerName = req.body.editedPlayerName;
    //         console.log({ editedPlayerName });
    //         const editedPlayerId = await this.playerService.updatePlayerInfo(req.user.id, editedPlayerName);
    //         res.json({ id: editedPlayerId, result: "Player's info edited!" });
    //     } catch (e) {
    //         console.log(e.toString());
    //         res.json({ result: e.toString() });
    //     }
    // }

}