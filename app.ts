import * as express from "express";
import * as expressSession from "express-session";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as socketIO from "socket.io";
import * as passport from "passport";
import * as path from "path";
import * as moment from "moment";
import * as Knex from "knex";
import * as os from "os";
import { loginFlow, loginGuard } from "./passport";
import { UserRouter } from "./routers/UserRouter";
import { UserService } from "./services/UserService";
import { MusicRouter } from "./routers/MusicRouter";
import { MusicService } from "./services/MusicService";
import { ProtectedRouter } from "./routers/ProtectedRouter";
import { PlayerRouter } from "./routers/PlayerRouter";
import { PlayerService } from "./services/PlayerService";
import { BandRouter } from "./routers/BandRouter";
import { BandService} from "./services/BandService";
import { BandAdsRouter } from "./routers/BandAdsRouter";
import { BandAdsService} from "./services/BandAdsService";
// import { MatchInfoRouter } from "./routers/MatchInfoRouter";
// import { MatchInfoService} from "./services/MatchInfoService";
// import { MatchRouter } from "./routers/MatchRouter";
// import { MatchService} from "./services/MatchService";
// import { LeaderBoardService} from "./services/LeaderBoardService";
// import { LeaderBoardRouter} from "./routers/LeaderBoardRouter";
// import { AlgoService} from "./services/AlgoService"
// import { AlgoRouter} from "./routers/AlgoRouter"
// import { ChatRoomService} from "./services/ChatRoomService";
// import { ChatRoomRouter} from "./routers/ChatRoomRouter";
import "./passport";


const app = express();
const server = new http.Server(app);
const io = socketIO(server);
const PORT = 8000;
const knexConfig = require("./knexfile");
export const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
export const userService = new UserService(knex);
export const playerService = new PlayerService(knex);
export const musicService = new MusicService(knex);
export const bandService = new BandService(knex);
export const bandAdsService = new BandAdsService(knex);
// export const matchInfoService = new MatchInfoService();
// export const matchService = new MatchService();
// export const leaderBoardService = new LeaderBoardService();
// export const leaderBoardRouter = new LeaderBoardRouter();
// export const algoService = new AlgoService();
// export const chatRoomService = new ChatRoomService();
// export const chatRoomRouter = new ChatRoomRouter();


export const protectedRouter = new ProtectedRouter();
export const userRouter = new UserRouter(userService);
export const playerRouter = new PlayerRouter(playerService);
export const musicRouter = new MusicRouter(musicService);
export const bandRouter = new BandRouter(bandService);
export const bandAdsRouter = new BandAdsRouter(bandAdsService);
// export const matchInfoRouter = new MatchInfoRouter();
// export const matchRouter = new MatchRouter();
// export const algoRouter = new AlgoRouter();

// specify sessionMiddleware
const sessionMiddleware = expressSession({
    secret: "everyone knows", 
    resave: true, 
    saveUninitialized: true, 
    cookie: {secure: false}
});

// backend request log on node
app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log("[" + moment().format("YYYY-MM-DD HH:mm:ss") + "] " + req.path);
    next();
});

// use sessionMiddleware for express
app.use(sessionMiddleware);

// use bodyParser for express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// use sessionMiddleware for socketIO
io.use((socket, next) => {
    sessionMiddleware(socket.request, socket.request.res, next);
});

// initialize socketIO connection
// io.on("connection", function (socket) {
//     console.log(socket);
//     // store socket.id as socketId inside session
//     // [CODE REVIEW] become problematic if a user opens more than 1 tab
//     // socket.request.session.socketId = socket.id;
//     socket.request.session.save();
//     // destroy socketId when disconnected
//     socket.on("disconnect", () => {
//         socket.request.session.socketId = null;
//         socket.request.session.save();
//     })
//     socket.on ('i-wanna-join-room',(room)=>{
//         console.log('hi2')
//         socket.join(room);
//     })
//     socket.on ('msg',(room,msg)=>{
//         console.log('hi4')
//         io.to(room).emit('got-msg',msg);
//     })
//     socket.on('update-contact',()=>{
//         console.log('bye3')
//         io.emit('refresh-contact')
//     })
// });
io.sockets.on('connection', function(socket) {

    // convenience function to log server messages on the client
    // function log() {
    //   var array = ['Message from server:'];
    //   array.push.apply(array, arguments);
    //   socket.emit('log', array);
    // }
    socket.on('keydown-from-others',function(key,ctrlKey){
      console.log('someone is playing keydown')
      socket.broadcast.emit('keydown-from-others',key,ctrlKey)
    })

    socket.on('keyup-from-others',function(key){
      console.log('someone is playing keyup')
      socket.broadcast.emit('keyup-from-others',key)
    })

    socket.on('getOnline',function(room){
        var clientsInRoom = io.sockets.adapter.rooms[room];
        var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
        socket.emit('onlineClients',room,numClients)
        console.log('Room ' + room + ' now has ' + numClients + ' client(s)');
      })

    socket.on('message', function(message) {
      console.log('Client said: ', message);
      // for a real app, would be room-only (not broadcast)
      socket.broadcast.emit('message', message, socket.id);
    });
  
    socket.on('create or join', function(room) {
      console.log('Received request to create or join room ' + room);
  
      var clientsInRoom = io.sockets.adapter.rooms[room];
      var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
      socket.emit('onlineClients',room,numClients)
    
      console.log('Room ' + room + ' now has ' + numClients + ' client(s)');
      
      if (numClients === 0) {
        socket.join(room);
        console.log('Client ID ' + socket.id + ' created room ' + room);
        socket.emit('created', room, socket.id);
  
      } else if (numClients > 0){
        console.log('Client ID ' + socket.id + ' joined room ' + room);
        io.sockets.in(room).emit('join', room,socket.id);
        socket.join(room);
        socket.emit('joined', room, socket.id);
        io.sockets.in(room).emit('ready');
      } 
      // else { // max two clients
      //   socket.emit('full', room);
      // }
    });
  
    socket.on('ipaddr', function() {
      var ifaces = os.networkInterfaces();
      for (var dev in ifaces) {
        ifaces[dev].forEach(function(details) {
          if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
            socket.emit('ipaddr', details.address);
          }
        });
      }
    });
  
  });

// passport - application middleware
// initialize passport
app.use(passport.initialize());
// notify passport of the usage of session
app.use(passport.session());

// grant access to public folder
app.use(express.static(__dirname + "/public"));
// grant access to resources folder
app.use(express.static(__dirname + "/resources"));
// protected access to protected folder with loginGuard
// app.use("/protected", loginGuard, checkProfileMiddleware, express.static(__dirname + "/protected"));
app.use("/set-profile", loginGuard, express.static(__dirname + "/set-profile"));
app.use("/protected", loginGuard, protectedRouter.checkProfileMiddleware, express.static(__dirname + "/protected"));

// Routers
app.use("/user", userRouter.router());
app.use("/music", musicRouter.router());
app.use("/player", playerRouter.router());
app.use("/bands", bandRouter.router());
app.use("/bandAds", bandAdsRouter.router());
app.use("/protected", protectedRouter.router());
// app.use("/leaderBoard", leaderBoardRouter.router());


// Route Handlers
// login routes
app.get("/login", (req: express.Request, res: express.Response) => {
    res.sendFile(path.join(__dirname, "/public/login.html"));
});
app.post("/login", (...rest) => {
    // passport.authenticate("local", loginFlow(...rest))(...rest);
    const authMiddleware = passport.authenticate("local", loginFlow(...rest));
    authMiddleware(...rest);
});
// app.post("/login", passport.authenticate("local", {failureRedirect: "/logout"}), (req: express.Request, res: express.Response) => {
//     res.redirect("/protected/main");
// });
// main route
// app.get("/protected/main", (req: express.Request, res: express.Response) => {
//     res.sendFile(path.join(__dirname, "/protected/main.html"));
// });

// logout route
app.get("/logout", (req: express.Request, res: express.Response, next: express.NextFunction) => {
    req.logout();
    res.json({loggedOut: true});
});
app.get("/profileSetup", (req: express.Request, res: express.Response) => res.redirect("/set-profile/profileSetup.html"));

// http server listens to the PORT
server.listen(PORT, () => console.log(`Listening to http://localhost:${PORT}...`));