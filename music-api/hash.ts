import * as bcrypt from "bcrypt";

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword: string) {
    const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
    return hash;
}

export async function checkPassword(plainPassword: string, hashPassword: string) {
    const match = await bcrypt.compare(plainPassword, hashPassword);
    return match;
}


// // hash seed passwords
// const run = async () => {
//     console.log("111 = ", await hashPassword("111"));
//     console.log("222 = ", await hashPassword("222"));
//     console.log("333 = ", await hashPassword("333"));
//     console.log("444 = ", await hashPassword("444"));
//     console.log("555 = ", await hashPassword("555"));
//     console.log("666 = ", await hashPassword("666"));
//     console.log("777 = ", await hashPassword("777"));
//     console.log("888 = ", await hashPassword("888"));
//     console.log("999 = ", await hashPassword("999"));
//     console.log("101010 = ", await hashPassword("101010"));
//     console.log("111111 = ", await hashPassword("111111"));
// };
// run();