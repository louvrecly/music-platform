import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable('band_ads');
    if(hasTable){
        return knex.schema.alterTable('band_ads',(table)=>{
            table.string("instruments")
        });  
    }else{
        return Promise.resolve();
    }

}


export async function down(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("bands_ads");
    if(hasTable){
        return knex.schema.alterTable('band_ads',(table)=>{
            table.dropColumn("instruments")
        });
    }else{
        return Promise.resolve();
    }
}


