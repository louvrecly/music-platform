import * as Knex from "knex";
// import { knex } from "../app";
// import { stdContents, prioritizedSheets, Schema } from "../database-init/readExcel";
import { bands, band_ads, ads_response, users, players, band_members, instruments, midi_instruments } from "../database-init/seedRef";


// Data Manipulation (DM) - Insert Data
export async function seed(knex: Knex): Promise<any> {

    // for (let i = 0; i < prioritizedSheets.length; i++) {

    //     const sheet = prioritizedSheets[i];
    //     // console.log("sheet = ", sheet);

    //     await knex.transaction(async (trx) => {

    //         const hasTable = await trx.schema.hasTable(sheet);
    //         // console.log("hasTable = ", hasTable);

    //         if (hasTable) {

    //             await trx(sheet).del();
    //             console.log(`Table "${sheet}" is truncated!`);

    //             let records = stdContents[sheet];
    //             console.log({ records });
    //             if (records.length > 0) {
    //                 records = await checkReferences(records, sheet);

    //                 const batchSize = 30;
    //                 // await trx.insert(records).into(sheet).returning('id');
    //                 await trx.batchInsert(sheet, records, batchSize).returning('id');
    //                 console.log(`Records have been inserted into "${sheet}" table!`);
    //             }

    //         } else {
    //             console.log(`Table "${sheet}" is not found!`);
    //         }

    //     });

        
    // }

    await knex.transaction(async (trx) => {

        let hasTable: boolean;
        
        // users
        hasTable = await trx.schema.hasTable('users');
        if (hasTable) {
            // truncate users...
            await trx('users').del();
            
            // insert data into users...
            await trx.insert(users).into("users").returning('id');
        }

    });



    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // players
        hasTable = await trx.schema.hasTable('players');
        if (hasTable) {
            // truncate players...
            await trx('players').del();
            
            // insert data into players...
            await trx.insert(players).into("players").returning('id');
        }

    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // bands
        hasTable = await trx.schema.hasTable('bands');
        if (hasTable) {
            // truncate bands...
            await trx('bands').del();
            
            // insert data into bands...
            await trx.insert(bands).into("bands").returning('id');
        }

    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // band_members
        hasTable = await trx.schema.hasTable('band_members');
        if (hasTable) {
            // truncate band_members...
            await trx('band_members').del();
            
            // insert data into band_members...
            await trx.insert(band_members).into("band_members").returning('id');
        }

    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // band_members
        hasTable = await trx.schema.hasTable('band_members');
        if (hasTable) {
            // truncate band_members...
            await trx('band_members').del();
            
            // insert data into band_members...
            await trx.insert(band_members).into("band_members").returning('id');
        }

    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // band_ads
        hasTable = await trx.schema.hasTable('band_ads');
        if (hasTable) {
            // truncate band_ads...
            await trx('band_ads').del();
            
            // insert data into band_ads...
            await trx.insert(band_ads).into("band_ads").returning('id');
        }

    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;
        
        // ads_response
        hasTable = await trx.schema.hasTable('ads_response');
        if (hasTable) {
            // truncate ads_response...
            await trx('ads_response').del();
            
            // insert data into ads_response...
            await trx.insert(ads_response).into("ads_response").returning('id');
        }

    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // instruments
        hasTable = await trx.schema.hasTable('instruments');
        if (hasTable) {
            // truncate instruments...
            await trx('instruments').del();
            
            // insert data into instruments...
            await trx.insert(instruments).into("instruments").returning('id');
        }
        
    });

    await knex.transaction(async (trx) => {

        let hasTable: boolean;

        // midi_instruments
        hasTable = await trx.schema.hasTable('midi_instruments');
        if (hasTable) {
            // truncate midi_instruments...
            await trx('midi_instruments').del();
            
            // insert data into midi_instruments...
            await trx.insert(midi_instruments).into("midi_instruments").returning('id');
        }
        
    });

};


// async function checkReferences(records: Object[], sheet: string) {

//     const schemas = stdContents['schema'];
//     const columns = schemas.filter((row: Schema) => row.sheet == sheet);

//     let dependencies = {};
//     columns.forEach((row: Schema) => {
//         if (row.references) {
//             const dependency: string[] = row.references.split('.');
//             dependencies[row.column] = dependency;
//         }
//     });
//     // console.log('dependencies = ', dependencies);

//     if (Object.keys(dependencies).length != 0) {
//         let dependencyRef = {};
//         for (let key in dependencies) {
//             const refIds = await knex.select('id', dependencies[key][1]).from(dependencies[key][0]);
//             // console.log("refIds = ", refIds);
//             const refIndex = refIds.reduce(function (acc, refId) {
//                 acc[refId[dependencies[key][1]]] = refId.id;
//                 return acc;
//             }, {});
//             // console.log("refIndex = ", refIndex);
//             dependencyRef[key] = refIndex;
//         }
//         // console.log('dependencyRef = ', dependencyRef);

//         records.map(row => {
//             for (let key in dependencyRef) {
//                 const references = dependencyRef[key];
//                 // console.log("references = ", references);
//                 const ref = row[key];
//                 // console.log("ref = ", ref);
//                 if (ref) {
//                     if (ref in references) {
//                         const index = references[ref];
//                         console.log("index = ", index);
//                         row[key] = index;
//                     } else {
//                         throw new Error(`Value '${ref}' is not found in the field '${dependencies[key][1]}' of the table "${dependencies[key][0]}"!`);
//                     }
//                 } else {
//                     console.log(`Value of ${key} is ${ref}!`);
//                 }
//             }
//         })
//         // console.log("records = ", records);
//     }
//     return records;

// }