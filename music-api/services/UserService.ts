import * as Knex from "knex";
import { User } from "./models";
// import { hashPassword } from "../hash";


export class UserService {

    // protected users: User[];

    constructor(private knex: Knex) {}

    getUsers() {
        return this.knex.select('*').from('users').orderBy('created_at', 'asc');
    }

    getUser(id: number) {
        return this.knex.select('*').from('users').where('id', id).limit(1);
    }

    getUserByUsername(username: string) {
        return this.knex.select('*').from('users').where('username', username).limit(1);
    }

    addUser(user: User) {
        return this.knex.insert(user).into('users').returning('id');
    }

    // public async getUsers() {
    //     this.users = await this.knex.transaction(async (trx) => {
    //         return await trx.select('*').from('users');
    //     });
    //     return this.users;
    // }

    // public async findUser(username: string, password: string) {
    //     const user = await this.knex.transaction(async (trx) => {
    //         return (await trx.select('*').from('users').where('username', username).andWhere('password', password))[0];
    //     });
    //     console.log("user = ", user);
    //     return user;
    // }

    // public async findUserByUserId(id: number) {
    //     const user = await this.knex.transaction(async (trx) => {
    //         return (await trx.select('*').from('users').where('id', id))[0];
    //     });
    //     console.log("user = ", user);
    //     return user;
    // }

    // public async findUserByUsername(username: string) {
    //     const user = await this.knex.transaction(async (trx) => {
    //         return (await trx.select('*').from('users').where('username', username))[0];
    //     });
    //     console.log("user = ", user);
    //     return user;
    // }
    
    // public async addUser(newUser: User) {
    //     const user = await this.findUserByUsername(newUser.username);
    //     if (user) {
    //         throw new Error("username already taken");
    //     } else {
    //         const hash = await hashPassword(newUser.password);
    //         newUser.password = hash;
    //         console.log("newUser = ", newUser);
    //         const newUserId = await this.knex.transaction(async (trx) => {
    //             return await trx.insert(newUser).into('users').returning('id');
    //         });
    //         return newUserId;
    //     }
    // }
    
}

// // Test Codes
// const userService = new UserService();
// // userService.addUser("user1", "111");
// userService.addUser("user4", "444");
// userService.findUserByUsername("user1");
// userService.findUserByUsername("user5");