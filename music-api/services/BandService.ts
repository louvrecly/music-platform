import * as Knex from "knex";
import { Band } from "./models";



export class BandService {

    constructor(private knex: Knex) { }

    public async loadBands() {
        const bands: Band[] = await this.knex.transaction(async (trx) => {
            return await trx.select('id', 'name', 'band_img', 'founder_id').from('bands');
        });
        return bands;
    }

    public async getBandById(id: number) {
        return await this.knex.select('*').from('bands').where('id', id).limit(1);
    }

    public async loadJoinedBands(id: number) {
        const bands: Band[] = await this.knex.transaction(async (trx) => {
            return (await trx.select('bands.id as id', 'bands.name as name', 'bands.band_img as band_img', 'bands.founder_id as founder_id').from('bands')
                .innerJoin('band_members', 'bands.id', '=', 'band_members.band_id')
                .where('band_members.member_id', id)
                .whereNot('bands.founder_id', id))
        });
        console.log("members= ", bands);
        return bands;
    }

    public async loadBandMembers(id: number) {
        const members: number[] = await this.knex.transaction(async (trx) => {
            return (await trx.select('players.id as id', 'players.name as name', 'players.gender as gender').from('bands')
                .innerJoin('band_members', 'bands.id', '=', 'band_members.band_id')
                .innerJoin('players', 'band_members.member_id', '=', 'players.id')
                .where('bands.id', id));
        });
        console.log("members= ", members);
        return members;
    }

    public async getBand(id: number) {
        const band = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('bands').where('id', id))[0];
        });
        console.log("band = ", band);
        return band;
    }

    public async ownBand(id: number) {
        const [band] = await this.knex.select('*').from('bands').where('founder_id', id).limit(1);
        if (!band) {
            throw new Error("You don't have a Band, please create one first")
        }
        else {
            console.log("my own band = ", band);
            return band;
        }
    }

    public async editBand(bandId: number, bandName:string, logo:string,founder_id:number ) {
        const [band] = await this.knex.select('*').from('bands').where('id', bandId).limit(1);
        if (!band) {
            throw new Error("You don't have a Band, please create one first")
        }
        else {
            const editBand = await this.knex('bands').update({name: bandName, band_img: logo})
            .where('founder_id', founder_id)
            .andWhere('id', bandId)
            .returning(['id', 'name', 'band_img','founder_id']);
            return editBand[0];
        }
    }


    public async findBandByfounder_id(id: number) {
        const band = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('bands').where('founder_id', id))[0];
        });
        return band
    }

    public async findBandByBandName(bandName: string) {
        const band = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('bands').where('name', bandName))[0];
        });
        console.log("band = ", band);
        return band;
    }

    public async addBand(createBand: Band) {
        const hasName = await this.findBandByBandName(createBand.name);
        const hasBand = await this.findBandByfounder_id(createBand.founder_id)
        if (hasName) {
            throw new Error("Band name already taken");
        } else if (hasBand){
            throw new Error("You have owned a band");
        }
        else {
            console.log({ createBand });
            const newBand = await this.knex.insert([createBand]).into('bands').returning(['id', 'name', 'band_img','founder_id']);
            console.log({ newBand });
            return newBand;
        }
    }

    public async updateBandName(id: number, name: string) {
        let band: Band = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('bands').where('id', id))[0];
        });
        console.log("id = ", id);
        console.log("band = ", band);
        if (band) {
            console.log('name = ', name);
            band.name = name;
            const editedBandId = await this.knex.transaction(async (trx) => {
                return await trx('bands').update(band).where('id', id).returning('id');
            });
            return editedBandId;
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async deleteMember(bandId: number, memberId: number) {
        let band: Band = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('bands').where('id', bandId))[0];
        });
        console.log("memberId = ", memberId);
        console.log("bandId = ", bandId);
        if (band) {
            const editedBandId = await this.knex.transaction(async (trx) => {
                return await trx('band_members').where('band_id', bandId).andWhere('member_id', memberId).del().returning('id');
            });
            return editedBandId;
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async quitBand(bandId: number, id: number) {
        let band: Band = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('bands').where('id', bandId))[0];
        });
        console.log("myId = ", id);
        console.log("bandId = ", band.id);
        if (band) {
            const editedBandId = await this.knex.transaction(async (trx) => {
                return await trx('band_members').where('band_id', bandId).andWhere('member_id', id).del().returning('id');
            });
            return editedBandId;
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async updateBandImg(id: number, band_img: string) {
        const [band] = await this.knex.select('*').from('bands').where('id', id).limit(1);
        // console.log({ id, player, profile_img });
        if (band) {
            band.band_img = band_img;
            const editedBandId = await this.knex('players').update(band).where('id', id).returning('id');
            return editedBandId;
        } else {
            // console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }


}