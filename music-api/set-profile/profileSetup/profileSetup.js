window.onload = async () => {
    // await loadSports();
    await loadInstruments();
}

// var map; //Will contain map object.
// var marker = false; ////Has the user plotted their location marker? 

// //Function called to initialize / create the map.
// //This is called when the page has loaded.
// function initMap() {
//     //The center location of our map.
//     var centerOfMap = new google.maps.LatLng(22.287945, 114.18135);

//     //Map options.
//     var options = {
//         center: centerOfMap, //Set center.
//         zoom: 12 //The zoom value.
//     };

//     //Create the map object.
//     map = new google.maps.Map(document.getElementById('map'), options);

//     //Listen for any clicks on the map.
//     google.maps.event.addListener(map, 'click', function (event) {
//         //Get the location that the user clicked.
//         var clickedLocation = event.latLng;
//         //If the marker hasn't been added.
//         if (marker === false) {
//             console.log("hello")
//             //Create the marker.
//             marker = new google.maps.Marker({
//                 position: clickedLocation,
//                 map: map,
//                 draggable: true //make it draggable
//             });
//             //Listen for drag events!
//             google.maps.event.addListener(marker, 'dragend', function (event) {
//                 markerLocation();
//             });
//         } else {
//             //Marker has already been added, so just change its location.
//             console.log("hi")
//             marker.setPosition(clickedLocation);
//             console.log(clickedLocation)
//         }
//         //Get the marker's location.
//         markerLocation();
//     });
// }

// //This function will get the marker's current location and then add the lat/long
// //values to our textfields so that we can save the location.
// function markerLocation() {
//     //Get location.
//     var currentLocation = marker.getPosition();
//     //Add lat and lng values to a field that we can save.
//     document.getElementById('lat').value = currentLocation.lat(); //latitude
//     document.getElementById('lng').value = currentLocation.lng(); //longitude
// }


// //Load the map when the page has finished loading.
// google.maps.event.addDomListener(window, 'load', initMap());

// function codeAddress() {
//     geocoder = new google.maps.Geocoder();
//     var address = document.getElementById('address').value;
//     geocoder.geocode({ 'address': address }, function (results, status) {
//         if (status == 'OK') {
//             map.setCenter(results[0].geometry.location);
//             if (marker === false) {
//                 marker = new google.maps.Marker({
//                     map: map,
//                     position: results[0].geometry.location,
//                     draggable: true
//                 });
//             }
//             else {
//                 marker.setPosition(results[0].geometry.location);
//             }
//             console.log(results[0].geometry.location)
//             var currentLocation = marker.getPosition();
//             document.getElementById('lat').value = currentLocation.lat(); //latitude
//             document.getElementById('lng').value = currentLocation.lng(); //longitude
//         } else {
//             alert('Geocode was not successful for the following reason: ' + status);
//         }
//     });
// }

// function geoFindMe() {
//     var output = document.getElementById("out");

//     if (!navigator.geolocation) {
//         output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
//         return;
//     }

//     function success(position) {
//         var latitude = position.coords.latitude;
//         var longitude = position.coords.longitude;

//         document.getElementById('lat').value = position.coords.latitude; //latitude
//         document.getElementById('lng').value = position.coords.longitude; //longitude
//         pos = new google.maps.LatLng({ lat: latitude, lng: longitude });
//         map.setCenter(pos);
//         if (marker === false) {
//             marker = new google.maps.Marker({
//                 position: pos,
//                 map: map,
//                 draggable: true
//             });
//         }
//         else {
//             marker.setPosition(pos);
//         }
//     }

//     function error() {
//         output.innerHTML = "Unable to retrieve your location";
//     };

//     navigator.geolocation.getCurrentPosition(success, error);
// }

// // load sport options
// async function loadSports() {
//     const res = await fetch("/player/sports", { method: "GET" });
//     const sports = await res.json();
//     console.log("sports = ", sports);

//     const sportOptions = document.querySelector("#sports-options");
//     let html = "";

//     for (let sport of sports) {
//         const sportId = sport.replace(/\s/g, "-");
//         const sportName = sport.split(" ").map(word => word[0].toUpperCase() + word.slice(1)).join(" ");
//         console.log("sportName = ", sportName);

//         html += `
//       <div class="custom-control custom-switch sports-toggle">
//         <input name="sports-option" type="checkbox"
//           class="custom-control-input sports-option" id="${sportId}" value="${sportName}">
//         <label for="${sportId}" class="form-label custom-control-label sports-option-label">
//           <div class="sports-name">
//             <p>${sportName}</p>
//           </div>
//         </label>
//       </div>
//       `;
//     }
//     sportOptions.innerHTML = html;
// }

// load instrument options
async function loadInstruments() {
    const res = await fetch("/music/instruments", { method: "GET" });
    const instruments = await res.json();
    console.log("instruments = ", instruments);

    const instrumentOptions = document.querySelector("#instruments-options");
    let html = "";

    for (let instrument of instruments) {
        // const instrumentDom = instrument.replace(/\s/g, "-");
        const instrumentOptionName = instrument.name;
        const instrumentOptionId = instrument.id;
        const instrumentDom = instrumentOptionName.split(" ").join("-");
        const instrumentName = instrumentOptionName.split(" ").map(word => word[0].toUpperCase() + word.slice(1)).join(" ");
        // console.log("instrumentName = ", instrumentName);

        html += `
            <div class="custom-control custom-switch instruments-toggle">
                <input name="instruments-option" type="checkbox" class="custom-control-input instruments-option" id="${instrumentDom}" data-id="${instrumentOptionId}" value="${instrumentName}">
                <label for="${instrumentDom}" class="form-label custom-control-label instruments-option-label">
                <div class="instruments-name">
                    <p>${instrumentName}</p>
                </div>
                </label>
            </div>
        `;
    }
    instrumentOptions.innerHTML = html;
}

// profile form submission
const profileForm = document.querySelector("#profileForm");
profileForm.addEventListener("submit", async (event) => {
    event.preventDefault();

    const name = profileForm.playerName.value;
    const gender = profileForm.gender.value;
    const dateOfBirth = profileForm.dateOfBirth.value;
    
    // let age = profileForm.age.value;
    // if (profileForm.age.value.endsWith("+")) {
    //     age = 65;
    // } else {
    //     age = parseInt(profileForm.age.value);
    // }
    // console.log("age = ", age);

    // const phoneNumber = parseInt(profileForm.phone.value);
    // console.log("phoneNumber = ", phoneNumber);

    // let availability = [
    //     [null, null],
    //     [null, null],
    //     [null, null],
    //     [null, null],
    //     [null, null],
    //     [null, null],
    //     [null, null]
    // ];
    // const timeSlots = document.querySelectorAll(".time-slot");
    // console.log("timeSlots = ", timeSlots);
    // for (let timeSlot of timeSlots) {
    //     const timeSlotId = timeSlot.id;
    //     // console.log("timeSlotId = ", timeSlotId);
    //     const timeSlotIdValues = timeSlot.id.substring(timeSlot.id.length - 2);
    //     // console.log("timeSlotIdValues = ", timeSlotIdValues);
    //     availability[parseInt(timeSlotIdValues[0], 10)][parseInt(timeSlotIdValues[1], 10)] = timeSlot.checked ? 1 : 0;
    //     // console.log("timeSlot.checked = ", timeSlot.checked);
    // }
    // console.log("availability = ", availability);

    // console.log(profileForm.lat.value);
    // console.log(profileForm.lng.value);
    // const x = parseFloat(profileForm.lat.value);
    // const y = parseFloat(profileForm.lng.value);
    // console.log("x = ", x);
    // console.log("y = ", y);

    let instrumentIds = [];
    const instrumentCheckBoxes = document.querySelectorAll(".instruments-option");
    for (let instrumentCheckBox of instrumentCheckBoxes) {
        if (instrumentCheckBox.checked) {
            console.log("instrumentCheckBox.id = ", instrumentCheckBox.id);
            // const instrument = instrumentCheckBox.id.split("-").join(" ");
            // console.log("instrument = ", instrument);
            const instrumentId = instrumentCheckBox.dataset.id;
            console.log({instrumentId});
            instrumentIds.push(instrumentId);
        }
    }
    console.log({instrumentIds});

    const playerInfo = {
        name: name, 
        gender: gender, 
        date_of_birth: dateOfBirth
    };
    
    const body = {
        player: playerInfo, 
        instruments: instrumentIds
    };

    await fetch("/player/profile", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(body)
    });

    location.href = "/protected/profile";
})