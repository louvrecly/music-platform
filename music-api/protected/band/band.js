window.onload = async () => {
    myBand = await getMyBand();
}
  

document.querySelector('#createBand').addEventListener('click', function () {
    const modal = addModal();
    var el = document.createElement("div");
    el.setAttribute(`style`, `position: fixed;height: 45%;width: 40%;top:50%;left: 50%;transform: translate(-50%, -50%);text-align:center;border: 1px solid rgba(112,111,111,.36)!important;
    background-color: rgba(31,31,31, 0.9);padding:.9375em;`);
    el.setAttribute("id", "clickedToCreate")
    el.innerHTML = `<h3>Create your own Band</h3>
    <form id="creatBandForm" action="protected/bands/createBand" method="POST">
        <div class="form-group">
            <label for="bandName">Band Name</label></br>
            <input type="text" id="newName" placeholder="Enter your band name"></br>
            <label for="bandLogo">Upload your Band Logo</label></br>
            <input type="file"  id="bandLogo" accept="image/*"></br>
            <button type="submit" class="btn btn-primary">Create</button>
            <button class="btn btn-primary closeButton">Close</button>
        </div>
    </form>`;
    document.body.appendChild(el);

    const createForm = document.querySelector("#creatBandForm");
    createForm.addEventListener("submit", async (event) => {
        event.preventDefault();
        await fetch("/protected/bands/createBand", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({
                name: createForm.newName.value
            })
        });
        getMyBand()
        el.innerHTML = 
        `<div>Band created successfully!</div>`
        el.style = `position: fixed;height: 10%;width: 40%;top:50%;left: 50%;transform: translate(-50%, -50%);border: 1px solid rgba(112,111,111,.36)!important;
        background-color: rgba(31,31,31, 0.9);padding:.9375em;z-index:1`
        
    })
    const closeButton = document.querySelector('.closeButton')
    modal.appendChild(el);

    removeModal(modal,el,closeButton)
});

document.querySelector('#createAds').addEventListener('click', function () {
   const modal = addModal();
    var el = document.createElement("div");
    el.setAttribute(`style`, `position: fixed;height: 60%;width: 40%;top:50%;left: 50%;transform: translate(-50%, -50%);border: 1px solid rgba(112,111,111,.36)!important;
    background-color: rgba(31,31,31, 0.9);padding:.9375em;z-index:1`);
    el.setAttribute("id", "clickedToRecruit")
    el.innerHTML =
        `
        <h3>Place a wanted ad</h3>
    <form id="createAdsForm" action="BandAds/createAds" method="POST">
        <div class="form-group">
            <div class="row">
            <div class="col">
            <label for="adName">Ad Headline</label></br>
            <input type="text" id="adName" placeholder="Give your ad a title">
            </div>
            <div class="col">
            <label for="adDesc">Description</label></br>
            <input type="text" id="adDesc" placeholder="Describe your band">
            </div>
            </div>
            </br>
            <div class="row">
            <div class="col">
            <label for="adMsg">Message</label>
            <input type="text" id="adMsg" placeholder="Message to musicians"></br>
            </div>
            </div>
            </br>
            <div class="row">
            <div class="col">
            <label for="instrument">instrument required<label>
            </div>
            </div>
            <div class="row">
            <div class="col-3">
            <input type="checkbox" id="instrument1" value="keyboard">
            <label class="form-check-label" for="instrument1">Keyboard</label></br>
            </div>
            <div class="col-3">
            <input type="checkbox" id="instrument2" value="guitar">
            <label class="form-check-label" for="instrument2">Guitar</label></br>
            </div>
            <div class="col-3">
            <input type="checkbox" id="instrument3" value="bass">
            <label class="form-check-label" for="instrument3">Bass</label></br>
            </div>
            <div class="col-3">
            <input type="checkbox" id="instrument4" value="drums">
            <label class="form-check-label" for="instrument4">Drums</label></br>
            </div>
            </div>
            </br>
            <button type="submit" class="btn btn-primary">Create</button>
            <button class="btn btn-primary closeButton">Close</button>
        </div>
    </form>`
        ;
    document.body.appendChild(el);

    const createAdForm = document.querySelector("#createAdsForm");
    createAdForm.addEventListener("submit", async (event) => {
    
        event.preventDefault();
        let instruments = []
        if (createAdForm.instrument1.checked){
            instruments.push(createAdForm.instrument1.value)
        }
        if (createAdForm.instrument2.checked){
            instruments.push(createAdForm.instrument2.value)
        }if (createAdForm.instrument3.checked){
            instruments.push(createAdForm.instrument3.value)
        }if (createAdForm.instrument4.checked){
            instruments.push(createAdForm.instrument4.value)
        }
        await fetch("/protected/bandsAds/createBandAds", { method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({
            name: createAdsForm.adName.value,
            description: createAdsForm.adDesc.value,
            message: createAdsForm.adMsg.value,
            band_id: myBand.id,
            instruments:instruments
            
        })
    });
        el.innerHTML = `<div>Band's Ads created successfully!</div>`
        el.style = `position: fixed;height: 10%;width: 40%;top:50%;left: 50%;transform: translate(-50%, -50%);border: 1px solid rgba(112,111,111,.36)!important;
        background-color: rgba(31,31,31, 0.9);padding:.9375em;z-index:1`
    })
    const closeButton = document.querySelector('.closeButton')
    modal.appendChild(el);

    removeModal(modal,el,closeButton)
});

async function getBandAds(){
    const res =  await fetch("/protected/bandsAds/loadBandAds", { method: "GET" })
  
    const ads = await res.json();

    let html = '';
    for (let ad of ads){
      let adId=ad.id
      let bandName = ad.name
      let headline=ad.ads_name
      let description = ad.description
      let message = ad.message
      let instruments= ad.instruments
      if (instruments !== null){
            instruments = instruments.replace(/["}{}]/g,"")
      }
      

      html += `
                <div class="row adInformation" id="data-${adId}">
                <div class="tidy bandName">${bandName}</div>
                <div class="tidy adName">${headline}</div>
                <div class="tidy">${description}</div>
                <div class="tidy">${message}</div>
                <div class="tidy">${instruments}</div>
                <div class="tidy"><div class="resButton" id="${adId}" onclick='response("${adId}","${headline}","${bandName}","${description}","${message}","${instruments}")'>View Ad</div></div>
                 </div>
                `
            const showAd = document.querySelector('#adTable');
            showAd.innerHTML = html;
            ;
    }
  }
getBandAds();

// async function searchBandAds(){
//     const res =  await fetch("/protected/bandsAds/searchBandAds", { method: "POST",
//     headers: {
//         "Content-Type": "application/json; charset=utf-8"
//     },
//     body: JSON.stringify({
//         name: createAdsForm.adName.value,
//         description: createAdsForm.adDesc.value,
//         message: createAdsForm.adMsg.value,
//         band_id: myBand.id,
//         instruments:instruments
        
//     })
// })
  
//     const ads = await res.json();

//     let html = '';
//     for (let ad of ads){
    
//       let adId=ad.id
//       let bandName = ad.name
//       let description = ad.description
//       let message = ad.message
//       let instruments= ad.instruments
//       let headline=ad.ads_name

//       html += `
//                 <div class="row adInformation" id="data-${adId}">
//                 <div class="tidy bandName">${bandName}</div>
//                 <div class="tidy adName">${headline}</div>
//                 <div class="tidy">${description}</div>
//                 <div class="tidy">${message}</div>
//                 <div class="tidy">${instruments}</div>
//                 <div class="tidy"><div class="resButton" id="${adId}" onclick='response(`${adId}`)'>View Ad</div></div>
//                  </div>
//                 `
//             const showAd = document.querySelector('#adTable');
//             showAd.innerHTML = html;
//             ;
//     }
//   }

            
async function getMyBand(){
    const res =  await fetch("/protected/bands/ownBand", { method: "GET" })
  
    const band = await res.json();

    localStorage.setItem("myBand", JSON.stringify(band));
    return JSON.parse(localStorage.getItem("myBand"));
  }

function addModal(){
    var modal = document.createElement("div");
    modal.setAttribute(`style`, `
    position: fixed; /* Stay in place */
    z-index: 2; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }`)
    document.body.appendChild(modal);
    return modal
}

function removeModal(modal, element, button){

    window.addEventListener("click", function(event) {
    if (event.target == modal){
        modal.removeChild(element);
        modal.style.display = "none";
    }
});
button.addEventListener("click", function(){
    modal.removeChild(element);
    modal.style.display = "none";
    })
}

async function response(adId, headline,bandName,description,message,instruments){
    if (instruments !== null){
        instruments = instruments.replace(/["}{}]/g,"")
  }
    const modal = addModal();
    var el = document.createElement("div");
    el.setAttribute(`style`, `position: fixed;height: 60%;width: 40%;top:50%;left: 50%;transform: translate(-50%, -50%);border: 1px solid rgba(112,111,111,.36)!important;
    background-color: rgba(31,31,31, 0.9);padding:.9375em;z-index:1`);
    el.setAttribute("id", "clickedResponse")
    el.innerHTML =
        `
        <h2>Wanted ad</h2>
        </br>
    <form id="responseAdForm" action="bandAds/responseBandAds" method="POST">
        <div class="form-group">
            <div class="row">
            <div class="col">
            <h5>${headline}</h5>
            </div>
            </div>
            </br>
            <div class="row">
            <div class="col-5">
            Band
            </div>
            <div class="col-4" style="color:#ff790d">
           ${bandName}
            </div>
            </div>
            <div class="row">
            <div class="col-5">
            Description
            </div>
            <div class="col-4">
           ${description}
            </div>
            </div>
            <div class="row">
            <div class="col-5">
            Message
            </div>
            <div class="col-4">
           ${message}
            </div>
            </div>
            <div class="row">
            <div class="col-5">
            Instruments
            </div>
            <div class="col-4">
           ${instruments}
            </div>
            </div>
            <div class="row">
            <div class="col-5">
            Message to band founder
            </div>
            <div class="col-4">
            <input type="text" id="msgToBand">
            </div>
            </div>
            </br>
            <button type="submit" class="btn btn-primary">Response</button>
            <button class="btn btn-primary closeButton">Close</button>
        </div>
    </form>`;
    modal.appendChild(el);
    const responseAdForm = document.querySelector("#responseAdForm");
    responseAdForm.addEventListener('submit', async (event) =>{
    event.preventDefault();
    const res =  await fetch("/protected/bandsAds/responseBandAds", { method: "POST",
    headers: {
        "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({
        message: responseAdForm.msgToBand.value,
        adId: adId
    })
});
    el.innerHTML = `
        <div>You have sent a request to the band!</div>`
        el.style = `position: fixed;height: 10%;width: 40%;top:50%;left: 50%;transform: translate(-50%, -50%);border: 1px solid rgba(112,111,111,.36)!important;
        background-color: rgba(31,31,31, 0.9);padding:.9375em;z-index:1`
            })
    const closeButton = document.querySelector('.closeButton')
    removeModal(modal,el, closeButton)
}

const searchAdsForm = document.querySelector("#searchAdsForm");
searchAdsForm.addEventListener('submit', async (event) =>{
    event.preventDefault();
    const res =  await fetch("/protected/bandsAds/searchBandAds", { method: "POST",
    headers: {
        "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({
        name: searchAdsForm.bandNameSearch.value,
        headline: searchAdsForm.headlineSearch.value,
        instruments: searchAdsForm.instrumentSearch.value
    })
});
  
    const searchResult = await res.json();

    let html = '';
    for (let ad of searchResult){
    
      let adId=ad.id
      let bandName = ad.name
      let description = ad.description
      let message = ad.message
      let instruments= ad.instruments
      if (instruments !== null){
            instruments = instruments.replace(/["}{}]/g,"")
      }
      let headline=ad.ads_name

      html += `
                <div class="row adInformation" id="data-${adId}">
                <div class="tidy bandName">${bandName}</div>
                <div class="tidy adName">${headline}</div>
                <div class="tidy">${description}</div>
                <div class="tidy">${message}</div>
                <div class="tidy">${instruments}</div>
                <div class="tidy"><div class="resButton" id="${adId}" onclick='response("${adId}","${headline}","${bandName}","${description}","${message}","${instruments}")'>View Ad</div></div>
                 </div>
                `
            const showAd = document.querySelector('#adTable');
            showAd.innerHTML = html;
            ;
    }
    
}) 