'use strict';

var myId;
var joinedClients = 0;
var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var localStream;
var remoteStream;
var pc = [];
var turnReady;
var hasConnection = [];
var connection = 0;
var newId;
var connectId;
// var position;

var pcConfig = {
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  }]
};

// Set up audio and video regardless of what devices are present.
var sdpConstraints = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

/////////////////////////////////////////////

var room = 'foo';
// Could prompt for room name:
// room = prompt('Enter room name:');

var socket = io.connect();

if (room !== '') {
  socket.emit('create or join', room);
  console.log('Attempted to create or  join room', room);
}

socket.on('created', function(room,Id) {
  myId = Id;
  console.log('Created room ' + room + ' ' + myId);
  console.log('This peer is the initiator of room ' + room + '!');
  isInitiator = true;
});

socket.on('join', function (room,Id){
  joinedClients += 1;
  newId = Id;
  console.log(`Now has' ${joinedClients} 'client`)
  console.log(`${newId} made a request to join room ${room}`);
  isChannelReady = true;
});

socket.on('joined', function(room , Id) {
  myId = Id;
  console.log('joined: ' + room + ' ' + myId);
  isChannelReady = true;
});

// socket.on('log', function(array) {
//   console.log.apply(console, array);
// });

////////////////////////////////////////////////

function sendMessage(message) {
  console.log('Client sending message: ', message);
  socket.emit('message', message);
}

// This client receives a message
socket.on('message', function(message, id) {
  console.log('Client received message:', message);
  if (message.getUserMedia === 'got user media') {
    maybeStart(id);
  } else if (message.id && hasConnection.indexOf(message.id) === -1){
    if (message.session.type === 'offer') {
      if (!isInitiator) {
      maybeStart(id);
    }
    pc[message.id].setRemoteDescription(new RTCSessionDescription(message.session));
    
    doAnswer(id); 
    }else if (message.session.type === 'answer' && isStarted) {
      if (message.target === myId){
    pc[message.id].setRemoteDescription(new RTCSessionDescription(message.session));
    }else{
      console.log('not for me')
    }
    }
   else if (message.session.type === 'candidate' && isStarted) {
     if (message.target === myId){
    var candidate = new RTCIceCandidate({
      sdpMLineIndex: message.session.label,
      candidate: message.session.candidate
    });
    pc[message.id].addIceCandidate(candidate);
    hasConnection.push(message.id)
    console.log(message.id + 'is connected')
    }
  }else{
    console.log('not for me')
  }
  }  
  else if (message.id && hasConnection.indexOf(message.id) !== -1){
    console.log('connection on, no action required')
  }
  else if (message === 'bye' && isStarted) {
    handleRemoteHangup(id);
    }
  }
);

////////////////////////////////////////////////////

var localVideo = document.querySelector('#localVideo');
// var remoteVideo = document.querySelector('#remoteVideo');
// var remoteVideo2 = document.querySelector('#remoteVideo2');

navigator.mediaDevices.getUserMedia({
  audio: true,
  video: true
})
.then(gotStream)
.catch(function(e) {
  alert('getUserMedia() error: ' + e.name);
});

function gotStream(stream) {
  console.log('Adding local stream.');
  localStream = stream;
  localVideo.srcObject = stream;
  let message = {
    getUserMedia:'got user media',
    id:myId
  }
  sendMessage(message);
  if (isInitiator) {
    maybeStart('null');
  }
}

var constraints = {
  video: true
};

console.log('Getting user media with constraints', constraints);

if (location.hostname !== 'localhost') {
  requestTurn(
    'https://computeengineondemand.appspot.com/turn?username=41784574&key=4080218913'
  );
}

function maybeStart(socketId) {
  console.log('>>>>>>> maybeStart() ', isStarted, localStream, isChannelReady);
  if ( typeof localStream !== 'undefined' && isChannelReady) {
    console.log('>>>>>> creating peer connection');
    createPeerConnection(socketId);
    pc[socketId].addStream(localStream);
    isStarted = true;
    console.log('isInitiator', isInitiator);
    if (joinedClients >= 1 ) {
      doCall(socketId);
    }
  }
}

window.onbeforeunload = function() {
  sendMessage('bye');
};

/////////////////////////////////////////////////////////

function createPeerConnection(socketId) {
  try {
    pc[socketId] = new RTCPeerConnection(null);
    pc[socketId].onicecandidate = function(event) {
      handleIceCandidate(event,socketId);
    }
    pc[socketId].onaddstream = function(event){
    handleRemoteStreamAdded(event, socketId);
    }
    pc[socketId].onremovestream = handleRemoteStreamRemoved;
    console.log('Created RTCPeerConnection');
  } catch (e) {
    console.log('Failed to create PeerConnection, exception: ' + e.message);
    alert('Cannot create RTCPeerConnection object.');
    return;
  }
}

function handleIceCandidate(event,socketId) {
  console.log('icecandidate event: ', event);
  if (event.candidate) {
    sendMessage(
      {id:myId,
        target:socketId,
        session:
        {
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
        }
      }
    );
  } else {
    console.log('End of candidates.');
  }
}

function handleCreateOfferError(event) {
  console.log('createOffer() error: ', event);
}

function doCall(socketId) {
  console.log('Sending offer to peer');
  // pc[socketId].createOffer(setLocalAndSendMessage, handleCreateOfferError);
  pc[socketId].createOffer().then(function(offer) {
    let offerMsg = {
      session:offer,
      id:myId,
      target:socketId
    }
    console.log('setLocalAndSendMessage sending message', offerMsg);
    sendMessage(offerMsg);
    return pc[socketId].setLocalDescription(offer);
  })
  .catch(function(event) {
    console.log('createOffer() error: ', event);
  });
  console.log('call you'+ ' '+ socketId)
}

function doAnswer(socketId) {
  console.log('Sending answer to peer.');
  pc[socketId].createAnswer().then(function(answer) {
    let offerMsg = {
      session:answer,
      id:myId,
      target:socketId
    }
    console.log('setLocalAndSendMessage sending message', offerMsg);
    sendMessage(offerMsg);
    return pc[socketId].setLocalDescription(answer);
  })
  .catch(function(event) {
    console.log('createAnswer() error: ', event);
  });
  console.log('answer you' + ' ' + socketId)
}

// function setLocalAndSendMessage(sessionDescription) {
//   pc[newId].setLocalDescription(sessionDescription);
//     let offerMsg = {
//     session:sessionDescription,
//     id:myId,
//     target:targetId
//   }
//   console.log('setLocalAndSendMessage sending message', offerMsg);
//   sendMessage(offerMsg);
// }

// function onCreateSessionDescriptionError(error) {
//   trace('Failed to create session description: ' + error.toString());
// }

function requestTurn(turnURL) {
  var turnExists = false;
  for (var i in pcConfig.iceServers) {
    if (pcConfig.iceServers[i].urls.substr(0, 5) === 'turn:') {
      turnExists = true;
      turnReady = true;
      break;
    }
  }
  if (!turnExists) {
    console.log('Getting TURN server from ', turnURL);
    // No TURN server. Get one from computeengineondemand.appspot.com:
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var turnServer = JSON.parse(xhr.responseText);
        console.log('Got TURN server: ', turnServer);
        pcConfig.iceServers.push({
          'urls': 'turn:' + turnServer.username + '@' + turnServer.turn,
          'credential': turnServer.password
        });
        turnReady = true;
      }
    };
    xhr.open('GET', turnURL, true);
    xhr.send();
  }
}

  function handleRemoteStreamAdded(event, socketId) {
    // if  (connection ===0){
    // remoteStream1 = event.stream;
    // remoteVideo1.srcObject = remoteStream1;
    // console.log('Remote stream 1 added.');
    // }else
    //   {
    //   remoteStream2 = event.stream;
    //   remoteVideo2.srcObject = remoteStream2;
    //   console.log('Remote stream 2 added.');
    //  }
    //  connection += 1
    remoteStream = event.stream;
    var videos = document.querySelectorAll('videos')
    var video  = document.createElement('video')
    var div    = document.createElement('div')
  
  video.setAttribute('id', socketId);
  video.srcObject   = remoteStream;
  video.autoplay    = true; 
  video.muted       = false;
  video.controls    = true;
  video.playsinline = true;
  
  div.appendChild(video);      
  document.querySelector('.videos').appendChild(div);  
  console.log('Remote stream added.');
  }

function handleRemoteStreamRemoved(event) {
  console.log('Remote stream removed. Event: ', event);
}

function hangup() {
  console.log('Hanging up.');
  stop();
  sendMessage('bye');
}

function handleRemoteHangup(id) {
  console.log('Session terminated.');
  stop(id);
  isInitiator = false;
}

function stop(id) {
  isStarted = false;
  pc[id].close();
  let video = document.querySelector(`#${id}`);
  let parentDiv = video.parentElement;
  video.parentElement.parentElement.removeChild(parentDiv);
  delete pc[id];
  hasConnection.splice(hasConnection.indexOf(id),1)
  console.log('remote stream removed')
  console.log(hasConnection.length + ' connection(s) remaining')
}
