import * as XLSX from "xlsx";
import * as path from "path";


const excelFile = "music_erd_seed.xlsx";
const workbook = XLSX.readFile(path.join(__dirname, excelFile));

export interface Schema {
    sheet: string, 
    column: string, 
    type: string, 
    references: string
}

function readExcel(workbook: XLSX.WorkBook) {
    let wbContents = {};
    for (let i = 0; i < workbook.SheetNames.length; i++) {
        const worksheetName = workbook.SheetNames[i];
        // console.log('worksheetNames = ', worksheetName);
        const sheetContent = XLSX.utils.sheet_to_json(workbook.Sheets[worksheetName]);
        // console.log("sheetContent = \n", sheetContent);
        wbContents[worksheetName] = sheetContent;
    }
    // console.log("wbContents = \n", wbContents);
    return wbContents;
}

function sortDependencies(schema: Object[]) {
    
    let sortingScores = {};
    schema.forEach(row => {sortingScores[row['sheet']] = 0});
    // console.log("sortingScores = \n", sortingScores);

    schema.map(row => {
        checkDependency(row, schema, sortingScores);
    });

    console.log("sortingScores = \n", sortingScores);

    const sheetPriorities = keyValueSwap(sortingScores);
    console.log("sheetPriorities = \n", sheetPriorities);
    
    const priorityKeys = Object.keys(sheetPriorities).map(key => parseInt(key, 10));
    // console.log("priorityKeys = \n", priorityKeys);
    const topPriority = Math.max(...priorityKeys);
    // console.log("topPriority = ", topPriority);

    let prioritizedSheets = [];
    for (let i = topPriority; i >= 0; i--) {
        prioritizedSheets.push(...sheetPriorities[i]);
    }
    console.log("prioritizedSheets = \n", prioritizedSheets);    
    return prioritizedSheets;
    
}

function keyValueSwap(obj: Object) {
    let swapped = {};
    for (let key in obj) {
        if (obj[key] in swapped) {
            swapped[obj[key]].push(key);
        } else {
            swapped[obj[key]] = [key];
        }
    }
    return swapped;
}

function checkDependency(row: Object, schema: Object[], sortingScores: Object) {
    // console.log("row['sheet'] = ", row['sheet']);
    if (row['references']) {
        const dependency: string = row['references'].split('.')[0];
        // console.log("dependency = ", dependency);
        sortingScores[dependency] += 1;
        const dependentRows = schema.filter(row => row['sheet'] == dependency);
        dependentRows.map(row => {
            checkDependency(row, schema, sortingScores);
        });
        return dependency;
    } else {
        return null;
    }
}

function prioritizeSheets(rawContents: Object) {
    const schemaRecords = rawContents['schema'];
    const prioritizedSheets = sortDependencies(schemaRecords);
    return prioritizedSheets;
}

export function standardizeNaming(name: string) {
    const renamed = name[0].toLowerCase() + name.slice(1).split(' ').join('_');
    return renamed;
}

function standardizeSchemas(rawContents: Object) {
    const schemaRecords: Schema[] = rawContents['schema'];
    // console.log("schemaRecords = \n", schemaRecords);
    schemaRecords.map(row => {
        const sheet = standardizeNaming(row.sheet);
        const column = standardizeNaming(row.column);
        row.sheet = sheet;
        row.column = column;
        if (row.references) {
            const references = row.references.split('.').map(standardizeNaming).join('.');
            row.references = references;
        }
    });
    return schemaRecords;
}

export function standardizeRecordKeys(dataRecords: Object[]) {
    let records: Object[] = [];
    dataRecords.map(row => {
        let standardizedRecord = {};
        for (let key in row) {
            const renamedKey = standardizeNaming(key);
            standardizedRecord[renamedKey] = row[key];
        }
        records.push(standardizedRecord);
    });
    return records;
}

function standardizeContents(rawContents: Object) {
    let stdContents = {};
    for (let sheet in rawContents) {
        if (sheet == 'schema') {
            const schemas = standardizeSchemas(rawContents);
            stdContents[sheet] = schemas;
        } else {
            const records = standardizeRecordKeys(rawContents[sheet]);
            stdContents[sheet] = records;
        }
    }
    return stdContents;
}


export const rawContents = readExcel(workbook);
// console.log("rawContents = \n", rawContents);
export const stdContents = standardizeContents(rawContents);
// console.log("stdContents = \n", stdContents);
export const prioritizedSheets = prioritizeSheets(stdContents);
// console.log("prioritizedSheets = \n", prioritizedSheets);
// export const schemas = standardizeSchemas(stdContents);
// // console.log("schemas = \n", schemas);