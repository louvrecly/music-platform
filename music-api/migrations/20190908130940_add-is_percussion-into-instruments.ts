import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("instruments");
    if(hasTable){
        return knex.schema.alterTable("instruments",(table)=>{
            table.boolean('is_percussion');
        });  
    }else{
        return Promise.resolve();
    }

}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("instruments");
    if(hasTable){
        return knex.schema.alterTable("instruments",(table)=>{
            table.dropColumn('is_percussion');
        });
    }else{
        return Promise.resolve();
    }
    
}