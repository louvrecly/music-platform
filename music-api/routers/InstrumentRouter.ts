import * as express from "express";
import { Instrument } from "../services/models";
import { InstrumentService } from "../services/InstrumentService";


export class InstrumentRouter {

    constructor(protected instrumentService: InstrumentService) { }

    public router() {

        const router = express.Router();

        router.get("/", this.list);
        router.post("/", this.post);
        router.put("/", this.put);

        return router;
    }

    protected list = async (req: express.Request, res: express.Response) => {
        try {
            const instruments = await this.instrumentService.getInstruments();
            res.json({ isSuccess: true, data: instruments });
        } catch (e) {
            console.log(e.toString());
            res.status(400).json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected post = async (req: express.Request, res: express.Response) => {
        try {
            const { newInstrument } = req.body;
            // console.log({ newInstrument });
            const [newInstrumentId] = await this.instrumentService.addInstrument(newInstrument as Instrument);
            res.json({ isSuccess: true, data: newInstrumentId });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

    protected put = async (req: express.Request, res: express.Response) => {
        try {
            const { editedInstrument } = req.body;
            // console.log({ editedInstrument });
            const [editedInstrumentId] = await this.instrumentService.editInstrument(editedInstrument as Instrument);
            res.json({ isSuccess: true, data: editedInstrumentId });
        } catch (e) {
            console.log(e.toString());
            res.json({ isSuccess: false, msg: e.toString() });
        }
    }

}