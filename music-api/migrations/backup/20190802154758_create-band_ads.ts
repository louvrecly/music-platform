import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("band_ads");
    if(!hasTable){
        return knex.schema.createTable("band_ads",(table)=>{
            table.increments();
            table.string("name")
            table.string("description");
            table.string("instruments");
            table.boolean("valid")
            table.integer("band_id").unsigned();
            table.foreign('band_id').references('bands.id');
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("band_ads");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await knex.schema.dropTable("band_ads");
            console.log(`Table band_ads" has been dropped!`);
        } else {
            console.log(`Table "band_ads does not exist!`);
        }
}

