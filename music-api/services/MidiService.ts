import * as Knex from "knex";
import * as aws from "aws-sdk";
import { NoteEvent, MidiFile, TrackExport } from "./models";
// import { NoteEvent, MidiArray, MidiFile, Track } from "./models";
// let MidiWriter = require("midi-writer-js");
// import * as fs from "fs";
import { Midi } from '@tonejs/midi';
import { InstrumentJSON } from "@tonejs/midi/dist/Instrument";
import { TrackJSON } from "@tonejs/midi/dist/Track";


export class MidiService {

    constructor(private knex: Knex) { }

    public upload = async (data: Buffer, name: string) => {
        const s3 = new aws.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            region: 'ap-southeast-1'
        });
        let file_path: string;
        // Promisify s3.upload
        const s3UploadPromise = () => {
            return new Promise((resolve, reject) => {
                s3.upload({
                    Body: data,
                    Bucket: 'cdn.letsjam.ga',
                    Key: `${name}-${Date.now()}.mid`
                }).on('httpUploadProgress', (evt: any) => {
                    //上傳進度
                    console.log(evt);
                }).send((err: Error, res: any) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
            });
        }
        // call promisified s3.upload
        return s3UploadPromise()
            .then((res: any) => {
                // console.log("file_path = ", res.Location.split('s://s3.ap-southeast-1.amazonaws.com').join(':/'));
                // //上傳完畢或是碰到錯誤
                file_path = res.Location.split('s://s3.ap-southeast-1.amazonaws.com').join(':/');
                return file_path;
            })
            .catch((e) => {
                console.log(e.toString());
            });
    }

    // public writeMidiFile = async (name: string, is_mono: boolean, midi_array: MidiArray) => {
    //     let track = new MidiWriter.Track();
    //     const noteEvents: NoteEvent[] = midi_array.map(midiItem => {
    //         const [pitch, duration] = midiItem;
    //         const noteEvent: NoteEvent = { pitch, duration };
    //         // console.log({ noteEvent });
    //         return new MidiWriter.NoteEvent(noteEvent);
    //     });
    //     // console.log({ noteEvents });
    //     let sequential: boolean = is_mono;
    //     track.addEvent(noteEvents, () => {
    //         return { sequential: sequential };
    //     });
    //     // console.log({ track });
    //     let writer = new MidiWriter.Writer([track]);
    //     // console.log("writer.dataUri() = ", writer.dataUri());
    //     // // writer.saveMIDI(path.join(__dirname, name));
    //     const buffer = new (Buffer as any).from(writer.buildFile());
    //     const file_path = await this.upload(buffer, name);
    //     // console.log({ file_path });
    //     return file_path;
    // }

    // public writeMidiFile = async (name: string, is_mono: boolean, midi_array: MidiArray, instrumentOption: number, is_percussion: boolean) => {
    public writeMidiFile = async (midiName: string, exportTracks: Array<TrackExport>) => {
        
        let midiFile = new Midi();
        
        for (let exportTrack of exportTracks) {
            
            const i = exportTracks.indexOf(exportTrack);

            // console.log({ exportTrack });
            const { track, instrument_number } = exportTrack;
            const { name, is_mono, midi_array, is_percussion } = track;

            // // hard code testing
            // const { name } = exportTrack;
            // const is_mono = true;
            // const is_percussion = true;
            // const midi_array: MidiArray = [[60, 4], [64, 4], [67, 2]];

            const newTrack = midiFile.addTrack();
            const instrumentJSON: InstrumentJSON = {
                number: instrument_number,
                family: "",
                name: ""
            };
    
            const channel: number = is_percussion ? 10 : i;
            const trackJSON: TrackJSON = {
                name,
                notes: [],
                channel,
                instrument: instrumentJSON,
                controlChanges: {}
            };
            newTrack.fromJSON(trackJSON);

            if (is_percussion) {
                // percussion track
                // midi_array.map((midiItem: [number[], number], i, arr) => {
                midi_array.map((midiItem) => {
                    const [midiNotes, time] = midiItem;
                    // const nextTime = arr[i + 1] ? arr[i + 1][1] : 0.5;
                    // const duration = nextTime - time;
                    const duration = 0.5;
                    let noteEvent: NoteEvent;
                    for (let midi of midiNotes as Array<number>) {
                        noteEvent = { midi, time, duration };
                        console.log({ noteEvent });
                        newTrack.addNote(noteEvent);
                    }
                });
            } else {
                // non-percussion track
                let time = 0;
                if (is_mono) {
                    // mono
                    midi_array.map((midiItem: [number, number]) => {
                        let [midi, fraction] = midiItem;
                        midi = midi < 0 ? 0 : midi;
                        const duration = 2 / fraction;
                        const noteEvent: NoteEvent = { midi, time, duration };
                        console.log({ noteEvent });
                        newTrack.addNote(noteEvent);
                        time += duration;
                    });
                } else {
                    // poly
                    let midi: number;
                    let onNotes = {};
                    midi_array.map((midiItem, i, arr) => {
                        const [notes, fraction] = midiItem;
                        const period = 2 / fraction;
                        const onNoteKeys = Object.keys(onNotes);
                        for (let onNoteKey of onNoteKeys) {
                            midi = parseInt(onNoteKey, 10);
                            midi = midi < 0 ? 0 : midi;
                            if ((notes as Array<number>).indexOf(midi) < 0 || i === arr.length - 1) {
                                const { time, duration } = onNotes[midi];
                                const noteEvent: NoteEvent = {
                                    midi,
                                    time,
                                    duration
                                };
                                newTrack.addNote(noteEvent);
                                console.log({ noteEvent });
                                delete onNotes[midi];
                            }
                        }
                        for (let note of notes as Array<number>) {
                            if (note in onNotes) {
                                onNotes[note].duration += period;
                            } else {
                                onNotes[note] = {
                                    time,
                                    duration: period
                                };
                            }
                        }
                        time += period;
                    });
                }
            }

        }

        // const midiName = "test_midi";
        
        // console.log("midi_array", is_percussion, midiFile)
        const buffer = new Buffer(midiFile.toArray());
        
        // fs.writeFileSync(name + ".mid", buffer);
        const file_path = await this.upload(buffer, midiName);
        // console.log({ file_path });
        return file_path;

    }

    public addMidiFile = async (newMidiFile: MidiFile) => {
        return await this.knex.insert(newMidiFile).into('midis').returning('id');
    }

    public async getMidiByPlayerId(playerId: number) {
        return await this.knex.select('band_members.band_id as band_id','bands.name as band_name','midis.id as midis_id','midis.name','midis.src')
            .from('band_members').where('member_id', playerId)
            .innerJoin('midis', 'midis.band_id', '=', 'band_members.band_id')
            .innerJoin('bands', 'band_members.band_id', '=', 'bands.id' )
    }
}
