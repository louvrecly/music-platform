import * as express from "express";
// import * as expressSession from "express-session";
// import * as bodyParser from "body-parser";
import * as http from "http";
import * as socketIO from "socket.io";
import * as os from "os";

const app = express();
const server = new http.Server(app);
const io = socketIO(server);
const PORT = 8000;
app.use(express.static(__dirname));

io.sockets.on('connection', function(socket) {

    // convenience function to log server messages on the client
    // function log() {
    //   var array = ['Message from server:'];
    //   array.push.apply(array, arguments);
    //   socket.emit('log', array);
    // }
    socket.on('keydown-from-others',function(key,ctrlKey){
      console.log('someone is playing keydown')
      socket.broadcast.emit('keydown-from-others',key,ctrlKey)
    })

    socket.on('keyup-from-others',function(key,ctrlKey){
      console.log('someone is playing keyup')
      socket.broadcast.emit('keyup-from-others',key)
    })

    socket.on('message', function(message) {
      console.log('Client said: ', message);
      // for a real app, would be room-only (not broadcast)
      socket.broadcast.emit('message', message, socket.id);
    });
  
    socket.on('create or join', function(room) {
      console.log('Received request to create or join room ' + room);
  
      var clientsInRoom = io.sockets.adapter.rooms[room];
      var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
  
      console.log('Room ' + room + ' now has ' + numClients + ' client(s)');
  
      if (numClients === 0) {
        socket.join(room);
        console.log('Client ID ' + socket.id + ' created room ' + room);
        socket.emit('created', room, socket.id);
  
      } else if (numClients > 0){
        console.log('Client ID ' + socket.id + ' joined room ' + room);
        io.sockets.in(room).emit('join', room,socket.id);
        socket.join(room);
        socket.emit('joined', room, socket.id);
        io.sockets.in(room).emit('ready');
      } 
      // else { // max two clients
      //   socket.emit('full', room);
      // }
    });
  
    socket.on('ipaddr', function() {
      var ifaces = os.networkInterfaces();
      for (var dev in ifaces) {
        ifaces[dev].forEach(function(details) {
          if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
            socket.emit('ipaddr', details.address);
          }
        });
      }
    });
  
  });



// http server listens to the PORT
server.listen(PORT, () => console.log(`Listening to http://localhost:${PORT}...`));