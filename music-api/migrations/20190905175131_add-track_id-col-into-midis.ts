import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("midis");
    if(hasTable){
        return knex.schema.alterTable("midis",(table)=>{
            table.dropColumn('type');
            table.string('src');
            table.integer('track_id').unsigned();
            table.foreign('track_id').references('tracks.id');
        });  
    }else{
        return Promise.resolve();
    }

}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("midis");
    if(hasTable){
        return knex.schema.alterTable("midis",(table)=>{
            table.string('type');
            table.dropColumn('src');
            table.dropColumn('track_id');
        });
    }else{
        return Promise.resolve();
    }
    
}