import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("ads_response");
    if(!hasTable){
        return knex.schema.createTable("ads_response",(table)=>{
            table.increments();
            table.string("message")
            table.string("status");
            table.integer("band_id").unsigned();
            table.foreign('band_id').references('bands.id');
            table.integer("band_ads_id").unsigned();
            table.foreign('band_ads_id').references('band_ads.id');
            table.integer("player_id").unsigned();
            table.foreign('player_id').references('players.id');
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("ads_response");
        // console.log("hasTable = ", hasTable);
        if (hasTable) {
            await knex.schema.dropTable("ads_response");
            console.log(`Table ads_response" has been dropped!`);
        } else {
            console.log(`Table "ads_response does not exist!`);
        }
}

