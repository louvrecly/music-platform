export interface User {
    id?: number,
    username: string,
    password: string,
    created_at?: Date,
    updated_at?: Date
}

export interface PublicProfile {
    id: number,
    name: string,
    date_of_birth: Date,
    gender: string,
    profile_img: string
}

export interface Player {
    id: number,
    name: string,
    date_of_birth: Date,
    gender: string,
    profile_img: string,
    user_id: number,
    created_at?: Date,
    updated_at?: Date
}

export interface Band {
    id?: number,
    name: string,
    band_img: string,
    founder_id: number
}

export type Duration = number;
export type Pitch = number | number[];
export type MidiItem = [Pitch, Duration];
// export type MidiItem = [number, Duration];
export type MidiArray = MidiItem[];
export type MidiData = [number, number, number];
// export type ActiveInstrument = 
//     "keyboard" | 
//     "drumsKit";

export interface NoteEvent {
    midi: number,
    time: number,
    ticks?: number,
    name?: string,
    // pitch: Pitch,
    pitch?: string,
    octave?: number,
    velocity?: number,
    duration: Duration
}

export interface Instrument {
    id?: number,
    name: string,
    is_percussion: boolean,
    created_at?: Date,
    updated_at?: Date
}

export interface TrackDB {
    id?: number
    name: string,
    is_mono: boolean,
    midi_array: string,
    band_id: number,
    instrument_id: number,
    is_percussion: boolean,
    created_at?: Date,
    updated_at?: Date
}

export interface Track {
    id?: number
    name: string,
    is_mono: boolean,
    midi_array: MidiArray,
    band_id: number,
    instrument_id: number,
    is_percussion: boolean,
    created_at?: Date,
    updated_at?: Date
}

export interface TrackExport {
    track: Track,
    instrument_number: number
}

export interface MidiFile {
    id?: number,
    name: string,
    src: string,
    band_id: number,
    created_at?: Date,
    updated_at?: Date
}

export interface Room {
    id: number,
    members: Array<RoomMember>
}

export interface RoomMember {
    id: number,
    name: string
}

export interface Message {
    // messageId: string,
    speakerId: number,
    speakerName: string,
    content: string,
    timeStamp: string
}