import * as express from "express";
// import * as multer from "multer";
// import * as path from "path";
// import * as moment from "moment";
import { BandAdsService, BandAds } from "../services/BandAdsService";

export class BandAdsRouter {

    constructor(protected bandAdsService: BandAdsService) {}

    public router() {

        const router = express.Router();

        router.get("/loadBandAds", this.loadBandAds)
        router.get("/loadAdResponse/:id", this.getResponse)
        router.post("/createBandAds", this.addBandAd);
        router.post("/searchBandAds", this.searchBandAds);
        router.post("/responseBandAds", this.responseAd);
        router.post("/replyAds/:status", this.replyAds);
        router.delete("/deleteAds/:id", this.deleteAds)

        return router;
    }

    protected addBandAd = async (req: express.Request, res: express.Response) => {
        try {
            const newBandAd: BandAds = {
                name: req.body.headline,
                description: req.body.description, 
                message: req.body.message,
                instruments:req.body.instruments,
                valid: true,
                band_id: req.body.band_id
            }
            
            console.log("req.user.id = ", req.user.id);
            console.log({newBandAd});
            
            const createdAd = await this.bandAdsService.createAd(newBandAd);
            res.json({ad: createdAd, result: "Band's Ad created!"});
        } catch (err) {
            console.log(err.message); 
            res.json({ad: null, result:err.message});
        }
    }

    public loadBandAds = async (req: express.Request, res: express.Response) => {
        try {
            const bandAds = await this.bandAdsService.loadAds();
            res.json(bandAds);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }
    public deleteAds = async (req: express.Request, res: express.Response) => {
        try {
            const deleteAdsId = await this.bandAdsService.deleteAds(req.params.id);
            res.json(deleteAdsId);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    public searchBandAds = async (req: express.Request, res:express.Response) => {
        try {
                const bandName = req.body.name
                const headline = req.body.headline
                const instruments = req.body.instruments
            
                const bandAds = await this.bandAdsService.searchAds(bandName, headline,instruments);
                res.json(bandAds);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    public responseAd = async (req:express.Request, res:express.Response) =>{
        try {
            const playerId = req.user.id
            const message = req.body.message
            const adId = req.body.adId
        
            const response = await this.bandAdsService.responseAd(adId, playerId,message);
            console.log({id:response, isSuccess:true})
    } catch(err) {
        console.log(err.message);
        res.json({isSuccess:false,result: err.message});
        }
    }

    public getResponse = async (req:express.Request, res:express.Response) =>{
        try {
            const response = await this.bandAdsService.getResponse(req.params.id);
            res.json(response);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    public replyAds = async (req:express.Request, res:express.Response) =>{
        try {
            if (req.params.status === 'accept'){
            const response = await this.bandAdsService.acceptAd(req.body.adId,req.body.bandId,req.body.playerId);
        console.log(req.body.playerId+'HIHIHIdsssddssdsdssddsdssdHIHIHIÎ')
            res.json(response);
            }
            else {
                const response = await this.bandAdsService.declineAd(req.body.adId);
                res.json(response);
            }
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }
}
