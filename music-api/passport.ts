import * as passport from "passport";
import * as passportJWT from "passport-jwt";
import jwt from "./jwt";
import { userService } from "./app";


const JWTStrategy = passportJWT.Strategy;
const { ExtractJwt } = passportJWT;

passport.use(new JWTStrategy({
    secretOrKey: jwt.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, async (payload, done) => {
    console.log("passport payload", payload)
    const [user] = await userService.getUserByUsername(payload.username);
    if (user) {
        console.log("passport", { user });
        return done(null, user);
    } else {
        return done(new Error(`non-existent username: ${payload.username}`));
    }
}));


// import * as passport from "passport";
// import * as passportLocal from "passport-local";
// import * as express from "express";
// import { checkPassword } from "./hash";
// import { userService } from "./app";
// import { User } from "./services/UserService";

// const LocalStrategy = passportLocal.Strategy;

// // adopt local strategy
// passport.use(new LocalStrategy(
//     async function (username, password, done) {
//         // const users = await userService.getUsers();
//         // const user = users.find(user => user.username === username);
//         const user = await userService.findUserByUsername(username);
//         console.log("user = ", user);
//         if (!user) {
//             // done(error, user, info)
//             done(null, false, {message: "invalid_username"});
//         } else {
//             const authenticated = await checkPassword(password, user.password);
//             console.log("authenticated = ", authenticated);
//             if (!authenticated) {
//                 // done(error, user, info)
//                 done(null, false, {message: "invalid_password"});
//             } else {
//                 // done(error, user, info)
//                 done(null, user);
//             }
//         }
//     }
// ));

// // serialize user - store minimal info (id) passport for identification
// passport.serializeUser(function (user: User, done) {
//     done(null, user.id);
// });

// // deserialize user - use id stored in passport to identify user
// passport.deserializeUser(async function (id: number, done) {
//     // const users = await userService.getUsers();
//     // const user = users.find(user => user.id === id);
//     const user = await userService.findUserByUserId(id);
//     if (user) {
//         done(null, user);
//     } else {
//         done(new Error("User not found"));
//     }
// });

// // specify login flow
// export function loginFlow(req: express.Request, res: express.Response, next: express.NextFunction) {
//     return (err: Error, user: User, info: {message: string}) => {
//         if (err) {
//             // error in login
//             console.log("/login?error=" + err.message);
//             res.json({access: false, message: "Login failed!"});    
//             // res.redirect("/login?error=" + err.message);    
//         } else if (info && info.message) {
//             // info exists - invalid username or password
//             console.log("/login?error=" + info.message);
//             res.json({access: false, message: "Invalid Username or Password!"});    
//             // res.redirect("/login?error=" + info.message);
//         } else {
//             // login success
//             req.logIn(user, (err) => {
//                 if (err) {
//                     // error
//                     console.log("/login?error=" + err.message);
//                     res.json({access: false, message: "Login failed!"});    
//                     // res.redirect("/login?error=" + err.message);
//                 } else {
//                     // no error
//                     console.log("/login success");
//                     res.json({access: true, message: "welcome!"});    
//                     // res.redirect("/protected/main");
//                 }
//             });
//         }
//     };
// }

// // specify route guard
// export function loginGuard(req: express.Request, res: express.Response, next: express.NextFunction) {
//     console.log("loginGuard is called");
//     if (req.user) {
//         next();
//     } else {
//         res.redirect("/login");
//     }
// }