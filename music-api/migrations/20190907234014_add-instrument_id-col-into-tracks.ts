import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("tracks");
    if(hasTable){
        return knex.schema.alterTable("tracks",(table)=>{
            table.integer('instrument_id').unsigned();
            table.foreign('instrument_id').references('instruments.id');
        });  
    }else{
        return Promise.resolve();
    }

}


export async function down(knex: Knex): Promise<any> {

    const hasTable = await knex.schema.hasTable("tracks");
    if(hasTable){
        return knex.schema.alterTable("tracks",(table)=>{
            table.dropColumn('instrument_id');
        });
    }else{
        return Promise.resolve();
    }
    
}