import * as Knex from "knex";

export enum Status {
    PENDING = 'requesting',
    ACCEPTED = 'accepted',
    DECLINED = 'declined'
}

export interface BandAds {
    id?: number,
    name: string,
    description: string,
    message: string,
    instruments: string,
    valid: boolean,
    band_id: number
}

export interface AdResponse {
    id?: number,
    message: string,
    status: Status,
    band_ads_id: number,
    player_id: number
}

export class BandAdsService {

    constructor(private knex: Knex) { }

    public async loadAds() {
        const ads: BandAds[] = await this.knex.transaction(async (trx) => {
            return await trx.select('band_ads.id as id', 'founder_id', 'description', 'message', 'valid', 'instruments', 'band_id', 'bands.name as bandName', 'band_ads.name as name')
            .from('band_ads')
            .innerJoin('bands', 'bands.id', '=', 'band_ads.band_id');
        });
        console.log({ ads });
        return ads;
    }

    public async searchAds(bandName: string, headline: string, instrument: string) {
        const ads: BandAds[] = await this.knex.transaction(async (trx) => {
            return await trx.select('band_ads.id as id', 'founder_id', 'description', 'message', 'valid', 'instruments', 'band_id', 'bands.name as bandName', 'band_ads.name as name')
            .from('band_ads').innerJoin('bands', 'bands.id', '=', 'band_ads.band_id')
            .where('bands.name', 'like', `%${bandName}%`)
            .andWhere('band_ads.name', 'like', `%${headline}%`).
            andWhere('band_ads.instruments', 'like', `%${instrument}%`)
        });
        console.log({ ads });
        return ads;
    }

    public async createAd(newAd: BandAds) {
        const ad = await this.getMyBandAd(newAd.band_id);
        if (ad) {
            throw new Error("Your band has placed a band ad already");
        } else {
            console.log({ newAd });

            const createdAdId = await this.knex.insert([newAd]).into('band_ads').returning('id');
            const ad = await this.knex.select('band_ads.id as id', 'founder_id', 'description', 'message', 'valid', 'instruments', 'band_id', 'bands.name as bandName', 'band_ads.name as name')
                .from('band_ads')
                .innerJoin('bands', 'bands.id', '=', 'band_ads.band_id')
                .where('band_ads.id', `${createdAdId}`);

            return ad[0]
        }
    }
    public async getMyBandAd(id: number) {
        const ad = await this.knex.transaction(async (trx) => {
            return (await trx.select('*').from('band_ads').where('band_id', id))[0];
        });
        console.log("ad = ", ad);
        return ad;
    }

    public async responseAd(adId: number, playerId: number, msg: string) {
        const hasResponse = await this.knex.select('*').from('ads_response')
            .where('band_ads_id', adId)
            .andWhere('player_id', playerId)
        if (!hasResponse) {
            const newRes: AdResponse = {
                message: msg,
                status: Status.PENDING,
                band_ads_id: adId,
                player_id: playerId
            }
            const newResponseId = await this.knex.transaction(async (trx) => {
                const newResponseId = await trx.insert([newRes]).into('ads_response').returning('id');
                console.log({ newResponseId });
                return newResponseId;
            });
            return newResponseId;
        }
        throw new Error("You have replied to this ad")
    }

    public async deleteAds(adId: number) {
        let bandAds = await this.knex.select('*').from('band_ads').where('id', adId);
        console.log("bandAds", bandAds)
        if (bandAds) {
            const editedAdsId = await this.knex.transaction(async (trx) => {
                return await trx('band_ads').where('id', adId).del().returning('id');
            });
            return editedAdsId[0];
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async getResponse(bandId: number) {

        const response = await this.knex.transaction(async (trx) => {
            return (await trx.select('players.name', 'players.id as playerId', 'ads_response.message', 'ads_response.status', 'ads_response.id as resId').from('band_ads').
                innerJoin('ads_response', 'band_ads.id', '=', 'ads_response.band_ads_id')
                .innerJoin('players', 'player_id', '=', 'players.id')
                .where('band_id', bandId));
        });
        console.log("response = ", response);
        return response;
    }

    public async acceptAd(adID: number, bandId: number, playerId: number) {

        const band = await this.knex.select('*').from('bands').where('id', bandId).limit(1);
        if (band) {
            const newMember = {
                band_id: bandId,
                member_id: playerId
            }
            await this.knex.insert(newMember).into('band_members').returning('id');
            return await this.knex('ads_response').update('status', 'accepted').where('id', adID).returning('id');
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }

    public async declineAd(adID: number) {

        const ad = await this.knex.select('*').from('ads_response').where('id', adID).limit(1);
        if (ad) {
            return await this.knex('ads_response').update('status', 'declined').where('id', adID).returning('id');
        } else {
            console.log("Invalid id");
            throw new Error("Invalid id");
        }
    }
}