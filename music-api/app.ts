import * as express from "express";
import * as expressSession from "express-session";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as socketIO from "socket.io";
import * as passport from "passport";
import * as Knex from "knex";
import * as os from "os";
import * as moment from "moment";
import * as cors from "cors";
// import * as path from "path";
import * as multer from "multer";
import * as multerS3 from "multer-s3";
import * as aws from "aws-sdk";
import { isLoggedIn } from "./guards";
// import { loginFlow, loginGuard } from "./passport";
import { UserService } from "./services/UserService";
import { UserRouter } from "./routers/UserRouter";
// import { MusicService } from "./services/MusicService";
import { MusicRouter } from "./routers/MusicRouter";
import { InstrumentService } from "./services/InstrumentService";
import { InstrumentRouter } from "./routers/InstrumentRouter";
import { AuthRouter } from "./routers/AuthRouter";
// import { ProtectedRouter } from "./routers/ProtectedRouter";
import { PlayerRouter } from "./routers/PlayerRouter";
import { PlayerService } from "./services/PlayerService";
import { BandService } from "./services/BandService";
import { BandRouter } from "./routers/BandRouter";
import { BandAdsService } from "./services/BandAdsService";
import { BandAdsRouter } from "./routers/BandAdsRouter";
// // import { MatchInfoRouter } from "./routers/MatchInfoRouter";
// // import { MatchInfoService} from "./services/MatchInfoService";
// // import { MatchRouter } from "./routers/MatchRouter";
// // import { MatchService} from "./services/MatchService";
// // import { LeaderBoardService} from "./services/LeaderBoardService";
// // import { LeaderBoardRouter} from "./routers/LeaderBoardRouter";
// // import { AlgoService} from "./services/AlgoService"
// // import { AlgoRouter} from "./routers/AlgoRouter"
// // import { ChatRoomService} from "./services/ChatRoomService";
// // import { ChatRoomRouter} from "./routers/ChatRoomRouter";
import "./passport";
import { TrackService } from "./services/TrackService";
import { TrackRouter } from "./routers/TrackRouter";
import { MidiService } from "./services/MidiService";
import { MidiRouter } from "./routers/MidiRouter";
import { MidiData, Room, RoomMember, Message } from "./services/models";

const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY } = process.env;

// specify s3 and upload
const s3 = new aws.S3({
  accessKeyId: AWS_ACCESS_KEY_ID,
  secretAccessKey: AWS_SECRET_ACCESS_KEY,
  region: 'ap-southeast-1'
});

// setup upload function as a multer instance
const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'cdn.letsjam.ga',
    metadata: (req, file, db) => {
      db(null, { fieldname: file.fieldname });
    },
    key: (req, file, cb) => {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
});

// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//       cb(null, path.join(__dirname, "../protected/uploads/"));
//   },
//   filename: (req, file, cb) => {
//       cb(null, `${req.user.id}-${moment().format("YYYYMMDD-HHmmss")}.${file.mimetype.split('/')[1]}`);
//   }
// });
// const upload = multer({ storage });

const app = express();
const server = new http.Server(app);
const io = socketIO(server);
const PORT = 8000;
const knexConfig = require("./knexfile");
export const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

export const userService = new UserService(knex);
export const playerService = new PlayerService(knex);
const bandService = new BandService(knex);
const bandAdsService = new BandAdsService(knex);
// const musicService = new MusicService(knex);
const instrumentService = new InstrumentService(knex);
const trackService = new TrackService(knex);
const midiService = new MidiService(knex);
// const matchInfoService = new MatchInfoService();
// const matchService = new MatchService();
// const leaderBoardService = new LeaderBoardService();
// const leaderBoardRouter = new LeaderBoardRouter();
// const algoService = new AlgoService();
// const chatRoomService = new ChatRoomService();
// const chatRoomRouter = new ChatRoomRouter();


const authRouter = new AuthRouter(userService);
// const protectedRouter = new ProtectedRouter();
const userRouter = new UserRouter(userService);
const playerRouter = new PlayerRouter(playerService, upload);
// const playerRouter = new PlayerRouter(playerService);
const bandRouter = new BandRouter(bandService, upload);
const bandAdsRouter = new BandAdsRouter(bandAdsService);
// const musicRouter = new MusicRouter(musicService);
export const instrumentRouter = new InstrumentRouter(instrumentService);
export const trackRouter = new TrackRouter(trackService);
export const midiRouter = new MidiRouter(midiService);
const musicRouter = new MusicRouter(
  // instrumentRouter,
  // trackRouter,
  // midiRouter
);
// const matchInfoRouter = new MatchInfoRouter();
// const matchRouter = new MatchRouter();
// const algoRouter = new AlgoRouter();

// specify sessionMiddleware
const sessionMiddleware = expressSession({
  secret: "everyone knows this",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false }
});

// allow cors
app.use(cors());

// backend request log on node
app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
  console.log("[" + moment().format("YYYY-MM-DD HH:mm:ss") + "] " + req.method + ": " + req.path + " (" + req.user + ")");
  next();
});

// use sessionMiddleware for express
app.use(sessionMiddleware);

// use bodyParser for express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// use sessionMiddleware for socketIO
io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next);
});

// initialize socketIO connection
// io.on("connection", function (socket) {
//     console.log(socket);
//     // store socket.id as socketId inside session
//     // [CODE REVIEW] become problematic if a user opens more than 1 tab
//     // socket.request.session.socketId = socket.id;
//     socket.request.session.save();
//     // destroy socketId when disconnected
//     socket.on("disconnect", () => {
//         socket.request.session.socketId = null;
//         socket.request.session.save();
//     })
//     socket.on ('i-wanna-join-room',(room)=>{
//         console.log('hi2')
//         socket.join(room);
//     })
//     socket.on ('msg',(room,msg)=>{
//         console.log('hi4')
//         io.to(room).emit('got-msg',msg);
//     })
//     socket.on('update-contact',()=>{
//         console.log('bye3')
//         io.emit('refresh-contact')
//     })
// });

const rooms: Array<Room> = [];

// const joinedRooms: {
//   roomId: number, players: {
//     id: number,
//     name: string
//   }[]
// }[] = [];

io.on('connection', function (socket) {

  // convenience function to log server messages on the client
  // function log() {
  //   var array = ['Message from server:'];
  //   array.push.apply(array, arguments);
  //   socket.emit('log', array);
  // }
  // store socket.id as socketId inside session
  // [CODE REVIEW] become problematic if a user opens more than 1 tab
  socket.request.session.socketId = socket.id;
  socket.request.session.save();
  // destroy socketId when disconnected
  socket.on("disconnect", () => {
    socket.request.session.socketId = null;
    socket.request.session.save();
  });

  // socket.on('keydown-from-others', function (key, ctrlKey) {
  //   console.log('someone is playing keydown')
  //   socket.broadcast.emit('keydown-from-others', key, ctrlKey)
  // });

  // socket.on('keyup-from-others', function (key, ctrlKey) {
  //   console.log('someone is playing keyup')
  //   socket.broadcast.emit('keyup-from-others', key)
  // });

  socket.on('midi-signal', function (band_id: number, player_id: number, midiData: MidiData, is_percussion: boolean) {
    console.log(`midiData: ${midiData} is played by player ${player_id} with ${is_percussion} instrument in room ${band_id}`);
    io.sockets.in(band_id.toString()).emit('midi-signal', player_id, midiData, is_percussion);
  });





  socket.on('getOnline', function (roomId: number) {
    var clientsInRoom = io.sockets.adapter.rooms[roomId];
    var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
    console.log(`Room ${roomId} now has ${numClients} client(s)`);
    socket.emit('onlineClients', roomId, numClients);
  });

  socket.on('message', function (message, ) {
    console.log('Client said: ', message);
    // for a real app, would be room-only (not broadcast)
    socket.broadcast.emit('message', message, socket.id);
  });

  socket.on('create or join', function (room) {
    console.log('Received request to create or join room ' + room);

    var clientsInRoom = io.sockets.adapter.rooms[room];
    var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;

    console.log('Room ' + room + ' now has ' + numClients + ' client(s)');

    if (numClients === 0) {
      socket.join(room);
      console.log('Client ID ' + socket.id + ' created room ' + room);
      socket.emit('created', room, socket.id, numClients);
      socket.broadcast.emit('onlineClients', room, numClients + 1);
    } else if (numClients > 0) {
      console.log('Client ID ' + socket.id + ' joined room ' + room);
      io.sockets.in(room).emit('join', room, socket.id);
      socket.join(room);
      socket.emit('joined', room, socket.id, numClients);
      io.sockets.in(room).emit('ready');
      socket.broadcast.emit('onlineClients', room, numClients + 1);
    }

    // else { // max two clients
    //   socket.emit('full', room);
    // }
  });

  // socket.on('i-wanna-join-room', (room, id, name) => {
  //   const newPlayer = Object.assign({},
  //     {
  //       roomId: room,
  //       players: [{
  //         id: id,
  //         name: name
  //       }]
  //     });
  //   const targetedRoom = joinedRooms.find(targetRoom => targetRoom.roomId === room);
  //   console.log(joinedRooms);
  //   if (targetedRoom) {
  //     targetedRoom.players.push(
  //       {
  //         id: id,
  //         name: name
  //       }
  //     );
  //     socket.join(room);
  //     console.log(targetedRoom);
  //     io.to(room).emit('player-joined', targetedRoom);
  //   } else {
  //     joinedRooms.push(newPlayer);
  //     socket.join(room);
  //     io.to(room).emit('player-joined', newPlayer);
  //     console.log('hi');
  //   }
  // });

  socket.on('join-room-request', (roomId: number, memberId: number, memberName: string) => {
    let room = rooms.find(room => room.id === roomId);
    const member: RoomMember = {
      id: memberId,
      name: memberName
    };
    if (room) {
      room.members.push(member);
    } else {
      room = {
        id: roomId,
        members: [member]
      };
      rooms.push(room);
    }
    console.log(`member ${memberName} [memberId: ${memberId}] joins the room ${roomId}`);
    socket.join(roomId.toString());
    io.to(roomId.toString()).emit('member-joined', room.members);

    var clientsInRoom = io.sockets.adapter.rooms[roomId];
    var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
    console.log(`Room ${roomId} now has ${numClients} client(s)`);
    socket.broadcast.emit('onlineClients', roomId, numClients);
  });

  socket.on('disconnected', (roomId: number, memberId: number) => {
    let room = rooms.find(room => room.id === roomId);
    if (room) {
      room.members = room.members.filter(member => member.id !== memberId);
      console.log(`member ${memberId} is disconnected from room ${roomId}`);
      io.to(roomId.toString()).emit('updatePlayers', memberId);
  
      var clientsInRoom = io.sockets.adapter.rooms[roomId];
      var numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
      console.log(`Room ${roomId} now has ${numClients - 1} client(s)`);
      socket.broadcast.emit('onlineClients', roomId, numClients - 1);
    }
  });

  socket.on('msg', (roomId: number, msg: Message) => {
    console.log(`message: \n${JSON.stringify(msg)}\n is sent to room ${roomId}`);
    io.to(roomId.toString()).emit('got-msg', msg);
  });


  socket.on('ipaddr', function () {
    var ifaces = os.networkInterfaces();
    for (var dev in ifaces) {
      ifaces[dev].forEach(function (details) {
        if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
          socket.emit('ipaddr', details.address);
        }
      });
    }
  });

});

// passport - application middleware
// initialize passport
app.use(passport.initialize());
// notify passport of the usage of session
app.use(passport.session());

// grant access to public folder
app.use(express.static(__dirname + '/public'));
// grant access to resources folder
app.use(express.static(__dirname + '/resources'));
// protected access to protected folder with loginGuard
// app.use('/protected', loginGuard, checkProfileMiddleware, express.static(__dirname + '/protected'));
app.use('/set-profile', isLoggedIn, express.static(__dirname + '/set-profile'));
// app.use('/protected', isLoggedIn, protectedRouter.checkProfileMiddleware, express.static(__dirname + '/protected'));
// app.use('/set-profile', loginGuard, express.static(__dirname + '/set-profile'));
// app.use('/protected', loginGuard, protectedRouter.checkProfileMiddleware, express.static(__dirname + '/protected'));

// Routers
app.use('/auth', authRouter.router());
app.use('/user', userRouter.router());
app.use('/music', isLoggedIn, musicRouter.router());
app.use('/player', isLoggedIn, playerRouter.router());
app.use('/bands', isLoggedIn, bandRouter.router());
app.use('/bandAds', isLoggedIn, bandAdsRouter.router());
// app.use('/protected', isLoggedIn, protectedRouter.router());
// app.use('/music', musicRouter.router());
// app.use('/player', playerRouter.router());
// app.use('/bands', bandRouter.router());
// app.use('/bandAds', bandAdsRouter.router());
// app.use('/protected', protectedRouter.router());
// // app.use('/leaderBoard', leaderBoardRouter.router());



// // Route Handlers
// // login routes
// app.get('/login', (req: express.Request, res: express.Response) => {
//   res.sendFile(path.join(__dirname, '/public/login.html'));
// });
// app.post('/login', (...rest) => {
//   passport.authenticate('local', loginFlow(...rest))(...rest);
//   // const authMiddleware = passport.authenticate('local', loginFlow(...rest));
//   // authMiddleware(...rest);
// });
// // app.post('/login', passport.authenticate('local', {failureRedirect: '/logout'}), (req: express.Request, res: express.Response) => {
// //     res.redirect('/protected/main');
// // });
// // main route
// // app.get('/protected/main', (req: express.Request, res: express.Response) => {
// //     res.sendFile(path.join(__dirname, '/protected/main.html'));
// // });

// // logout route
// app.get('/logout', (req: express.Request, res: express.Response, next: express.NextFunction) => {
//   req.logout();
//   res.json({ loggedOut: true });
// });
app.get('/profileSetup', (req: express.Request, res: express.Response) => res.redirect('/set-profile/profileSetup.html'));

// http server listens to the PORT
server.listen(PORT, () => console.log(`Listening to http://localhost:${PORT}...`));